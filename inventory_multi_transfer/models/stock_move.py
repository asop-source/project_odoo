from odoo import models, fields, api, _

class internal_stock_picking(models.Model):
	_inherit ='stock.picking'
	_order = 'origin'


class internal_stock_move(models.Model):
	_inherit ='stock.move'

	in_reference = fields.Char('Internal Barcode',compute='_in_barcode',  store=True)
	barcode_vendor = fields.Char('Barcode Vendor',compute='_barcode_vendor',  store=True)
	article = fields.Text('Article',compute='_article',  store=True)
	unit_price = fields.Monetary(default=0.0, currency_field='company_currency_id')
	company_currency_id = fields.Many2one('res.currency', string="Company Currency", related='company_id.currency_id', readonly=True,
		help='Utility field to express amount currency')
	company_id = fields.Many2one('res.company', string='Company', store=True, readonly=True)

	sub_total =  fields.Monetary(currency_field='company_currency_id',compute='_default_jmlah')
	keterangan = fields.Text('Information')
	
	no_packing = fields.Char('No Packing')
	uom_2 = fields.Many2one('uom.uom','Purchase Uom',compute='_purcahse_uom')


	@api.depends('product_id')
	def _purcahse_uom(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.uom_2 = product.uom_po_id.id


	@api.depends('product_id')
	def _article(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.article = product.article

	@api.depends('product_id')
	def _barcode_vendor(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.barcode_vendor = product.barcode_vendor

	@api.depends('product_id')
	def _in_barcode(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.in_reference = product.barcode

	# def _in_product(self):
	# 	for data in self:
	# 		product = data.product_id
	# 		if data.product_id:
	# 			data.name_product = product.name



	@api.depends('unit_price','quantity_done')
	def _default_jmlah(self):
		for line in self:
			if line.quantity_done:
				line.sub_total = line.unit_price * line.quantity_done

	@api.onchange('product_id','quantity_done')
	def onchange_article_barcode(self):
		for data in self:
			product = data.product_id
			data.barcode_vendor = product.barcode_vendor
			data.article = product.article
			data.unit_price = product.lst_price
