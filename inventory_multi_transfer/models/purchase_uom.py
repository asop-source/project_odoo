from odoo import models, fields, api, _

class node3_purchase_uom(models.Model):
	_inherit ='purchase.order.line'


	purchase_uom = fields.Many2one('uom.uom','Purchase Uom',compute='_purcahse_uom')

	@api.depends('product_id')
	def _purcahse_uom(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.purchase_uom = product.uom_po_id.id