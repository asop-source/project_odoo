
from odoo import models, fields, api, _


class node3_product_product(models.Model):
	_inherit = 'product.pricelist'

	location_id = fields.Many2one('stock.location', 'Location',index=True, required=True)




class node3_product_pricelist(models.Model):
	_inherit = 'product.pricelist.item'

	location_dest_id = fields.Many2one('stock.location','Location',related='pricelist_id.location_id', required=True)

