from odoo import models, fields, api, _
import datetime

class internal_stock_picking_batch(models.Model):
	_inherit ='stock.picking.batch'

	no_expds = fields.Char('No XPDS')
	no_seal = fields.Char('No Seal')
	no_kendaran = fields.Char('No Kendaraan')
	tgl_kirim = fields.Datetime('Tgl Kirim')
	date = fields.Datetime('Tanggal',default=fields.datetime.now())
	partner_id = fields.Many2one(string='Penerima',comodel_name='res.partner')
