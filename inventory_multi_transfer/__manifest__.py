# -*- coding: utf-8 -*-
{
    'name': "Inventory multi transfer",

    'summary': """
        - Proses internal transfer 3 gudang ke satu toko
        """,

    'description': """
        Reprt Picking List
        
    """,

    'author': "asopkarawang@gamil.com",

    'category': 'Inventory',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','purchase','node3_product_barcode'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/stock_move.xml',
        'views/purchase_uom.xml',
        'views/product_pricelist.xml',
        'views/stock_picking_batch.xml',
        'report/report_picking_list.xml',
    ],
}