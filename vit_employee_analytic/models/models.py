from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
import logging
import pdb
import babel
from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
from pytz import timezone
_logger = logging.getLogger(__name__)

class employee(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"
    work_location = fields.Many2one(
        comodel_name="account.analytic.tag", string="Work Location"
    )