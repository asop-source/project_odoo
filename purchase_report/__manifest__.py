{
    "name"          : "Purchase Report",
    "version"       : "1.0",
    "author"        : "Susi",
    "website"       : "https://odoo.com",
    "category"      : "Purchases",
    "summary"       : "Purchase Report in Pdf Format",
    "description"   : """
        
    """,
    "depends"       : [
        "base",
        "purchase",
    ],
    "data"          : [
        "views/res_users_view.xml",
        "views/purchase_order_view.xml",
        "report/purchase_order_report.xml",
        # "wizard/purchase_report_view.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}