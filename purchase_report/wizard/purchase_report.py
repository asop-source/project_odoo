import xlsxwriter
import base64
from odoo import fields, models, api
from datetime import datetime
from io import BytesIO
from collections import OrderedDict
import pytz
import string
from pytz import timezone

class PurchaseReport(models.TransientModel):
    _name = "purchase.report.xlsx"
    _description = "Purchase Report"
    
    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))
    
    state_x = fields.Selection([('choose','choose'),('get','get')], default='choose')
    data_x = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date', default=fields.Date.context_today)
    partner_ids = fields.Many2many('res.partner', 'purchase_report_xlsx_partner_rel', 'purchase_report_xlsx_id',
        'partner_id', 'Supplier')
    product_ids = fields.Many2many('product.product', 'purchase_report_xlsx_product_rel', 'purchase_report_xlsx_id',
        'product_id', 'Product')
        
    def print_excel_report(self):
        data = self.read()[0]
        partner_ids = data['partner_ids']
        product_ids = data['product_ids']
        start_date = data['start_date']
        end_date = data['end_date']
        
        query_where = ' 1=1 '
        if start_date :
            start_date = str(start_date)
            query_where +=" AND po.date_order >= '%s'"%str(start_date + ' 00:00:00')
        if end_date :
            end_date = str(end_date)
            query_where +=" AND po.date_order <= '%s'"%str(end_date + ' 23:59:59')
        if partner_ids :
            query_where +=" AND po.partner_id in %s"%str(
                tuple(partner_ids)).replace(',)', ')')
        if product_ids :
            query_where +=" AND pp.id in %s"%str(
                tuple(product_ids)).replace(',)', ')')

        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Purchase'
        filename = '%s Report %s%s'%(report_name,date_string,'.xlsx')
        
        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        
        query = """
            select po.name as po_number, po.date_order as po_date,
            rp.name as supplier, pt.name as prod_name,
            pol.product_qty as qty, pol.price_unit,
            pol.product_qty * pol.price_unit as subtotal
            from purchase_order po
            left join purchase_order_line pol on pol.order_id = po.id
            left join product_product pp on pp.id = pol.product_id
            left join product_template pt on pt.id = pp.product_tmpl_id
            left join res_partner rp on rp.id = po.partner_id
            where po.state in ('purchase','done') and %s
            order by po_number
        """%(query_where)
        
        self._cr.execute(query)
        result = self._cr.fetchall()
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 20)
        worksheet.set_column('D1:D1', 20)
        worksheet.set_column('E1:E1', 20)
        worksheet.set_column('F1:F1', 20)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', '%s Report'%report_name, wbf['title_doc'])
        worksheet.write('A3', 'Tanggal %s s/d %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        
        row=5
        
        worksheet.write('A%s' %(row), 'No', wbf['header'])
        worksheet.write('B%s' %(row), 'Purchase Number', wbf['header'])
        worksheet.write('C%s' %(row), 'Purchase Date', wbf['header'])
        worksheet.write('D%s' %(row), 'Supplier', wbf['header'])        
        worksheet.write('E%s' %(row), 'Product Name', wbf['header'])
        worksheet.write('F%s' %(row), 'Quantity', wbf['header'])
        worksheet.write('G%s' %(row), 'Price Unit', wbf['header'])
        worksheet.write('H%s' %(row), 'Subtotal', wbf['header'])
        
        row+=1
        row1=row
        no=1
        
        for res in result:
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res[0] if res[0] else '', wbf['content'])                    
            worksheet.write('C%s' %row, res[1] if res[1] else '', wbf['content'])
            worksheet.write('D%s' %row, res[2] if res[2] else '', wbf['content'])  
            worksheet.write('E%s' %row, res[3] if res[3] else '', wbf['content'])
            worksheet.write('F%s' %row, res[4] if res[4] else 0, wbf['content_number'])
            worksheet.write('G%s' %row, res[5] if res[5] else 0, wbf['content_float'])
            worksheet.write('H%s' %row, res[6] if res[6] else 0, wbf['content_float'])
            
            row+=1
            no+=1
        
        worksheet.merge_range('A%s:B%s'%(row,row), 'Total', wbf['total'])
        worksheet.write('C%s' %row, '', wbf['total_float'])
        worksheet.write('D%s' %row, '', wbf['total_float'])
        worksheet.write('E%s' %row, '', wbf['total_float'])
        worksheet.write_formula('F%s' %row, '{=subtotal(9,F%s:F%s)}'%(row1, row-1), wbf['total_number'])
        worksheet.write('G%s' %row, '', wbf['total_float'])
        worksheet.write_formula('H%s' %row, '{=subtotal(9,H%s:H%s)}'%(row1, row-1), wbf['total_float'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data_x':out, 'name':filename})
        fp.close()
        filename = '%s Report %s'%(report_name,date_string)
        filename += '%2Exlsx'
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data_x&download=true&filename="+filename
        return {
            'name': 'Purchase Report',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['orange'],'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['yellow'],'font_color': '#000000'})
        wbf['header_yellow'].set_border()
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()
        
        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right() 
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right() 
        
        wbf['content_float'] = workbook.add_format({'align': 'right','num_format': '#,##0.00'})
        wbf['content_float'].set_right() 
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right() 
        wbf['content_number'].set_left() 
        
        wbf['content_percent'] = workbook.add_format({'align': 'right','num_format': '0.00%'})
        wbf['content_percent'].set_right() 
        wbf['content_percent'].set_left() 
                
        wbf['total_float'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()            
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()         
        
        wbf['total_number'] = workbook.add_format({'align':'right','bg_color': colors['white_orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()            
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        wbf['total_number_yellow'] = workbook.add_format({'align':'right','bg_color': colors['yellow'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()            
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()
        
        wbf['total_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'center'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()            
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()         
        
        wbf['total_number_orange'] = workbook.add_format({'align':'right','bg_color': colors['orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()            
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()
        
        wbf['total_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'center'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()
        
        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()
        
        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()
        
        return wbf, workbook
