# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class InheritedHelpDesk(models.Model):
    _inherit="helpdesk.support"

    priority = fields.Selection(selection_add=[('3', 'Very High')])

    complexity = fields.Selection([
                    ('0', 'Low'),
                    ('1', 'Middle'),
                    ('2', 'High'),
                    ('3', 'Very High'),
                ], string='Complexity', required=True)
