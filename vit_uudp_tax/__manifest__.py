# -*- coding: utf-8 -*-
{
    'name': "vit tax uudp",

    'summary': """

        Inherit Field Tax

    """,

    'description': """
        Penambahan Field Tax di form UUDP
    """,

    'author': "asopkarawang@gmail.com",
    'website': "http://www.vitraining.com",


    # any module necessary for this one to work correctly
    'depends': ['base','vit_uudp'],

    # always loaded
    'data': [
        'tax.xml',
    ]
}