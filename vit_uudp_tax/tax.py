from odoo import models, fields, api, _

class vit_uudp_tax(models.Model):
	_inherit = 'uudp'

	tax_ids = fields.Many2many('account.tax', string='Taxes Applied', domain=['|', ('active', '=', False), ('active', '=', True)],)