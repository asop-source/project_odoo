# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class AssetsInherit(models.Model):
    _inherit = 'account.asset.asset'
    _rec_name = 'asset_code'

    warranty_info = fields.Char(string="Waranty Information")
    warranty_date = fields.Date(string="Warranty Date")
    warranty_service_provider_id = fields.Many2one('res.partner', string="Warranty Service Provider")

    manufacturer = fields.Char(string="Manufacturer")
    record_brand = fields.Char(string="Record Brand")
    model_number = fields.Char(string="Model Number")
    serial_number = fields.Char(string="Serial Number")
    received_date = fields.Date(string="Received Date")

    location = fields.Char(string="Information Location")
    supperior_asset = fields.Char(string="Information Superior Asset")

    pengguna_id = fields.Many2one('res.users', string="Asset User")
    source_loc = fields.Char('Source Location')

    asset_code = fields.Char('Asset Code', readonly=True, index=True, copy=False, default='New')

    @api.model
    def create(self, vals):
        vals['asset_code'] = self.env['ir.sequence'].next_by_code('account.asset.asset')
        return super(AssetsInherit, self).create(vals)

    state = fields.Selection(selection_add=[('to approve', 'To Approve'), ('validate', 'Validate'), ('open', 'Running')], copy=False)
    asset_approval = fields.Many2one(
        'res.users', store=True, readonly=True, track_visibility='onchange')
    is_approval = fields.Boolean(
        string="Approval", compute='cek_approval', store=False)
    approved_by = fields.Many2one(
        'res.users', store=True, readonly=True, track_visibility='onchange')
    level_approval = fields.Integer(string="Level Approval")

    @api.depends('is_approval')
    def cek_approval(self):
        for asset in self:
            if asset.asset_approval == self.env.user and asset.state == "to approve":
                asset.is_approval = True
            else:
                asset.is_approval = False

    @api.multi
    def button_confirm(self):
        for asset in self:
            approval = self.env["approval.asset.disposal"].search([('level', '=', 1)])
            if approval:
                asset.write({'state': 'to approve', 'asset_approval': approval[0].approval_id.id, 'level_approval': 1}) 
            else:
                raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))

    @api.multi
    def button_approve(self):
        for asset in self:
            asset.approved_by = asset.asset_approval
            next_level = asset.level_approval + 1
            apps = self.env['approval.asset.disposal'].search([('level','=', next_level)])            
            if apps:
                asset.write({'state': 'to approve', 'asset_approval': apps[0].approval_id.id, 'level_approval': next_level}) 
            else:
                asset.write({'state': 'open', 'asset_approval': False})

class AssetApproval(models.Model):
    _name = "approval.asset.disposal"
    _description = "Asset Disposal Approval"
    
    name = fields.Char(string="Description")
    level = fields.Integer(string="Level", required=True)
    minimal_amount = fields.Float(string="Min Amount")
    approval_id = fields.Many2one("res.users", string="Approval", required=True)
