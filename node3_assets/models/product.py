# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class ProductInherit(models.Model):
    _inherit = 'product.template'
    
    type = fields.Selection(selection_add=[('asset', 'Assets')])
    # asset_category_id = fields.Many2one('account.asset.category', string='Asset Type', company_dependent=True, ondelete="restrict")
    asset_req = fields.Boolean(default=False)

    @api.onchange('type')
    def set_required(self):
        if self.type == "asset":
            self.asset_req = True
        else:
            self.asset_req = False
        
    
    