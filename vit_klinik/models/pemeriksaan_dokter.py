# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class PemeriksaanDokter(models.Model):
    _name = 'pemeriksaan.dokter'


    name = fields.Many2one(comodel_name="res.partner", string="Nama Pasien / Customer")
    no_ruang = fields.Many2one(comodel_name="ruang.pemeriksaan", string="No Ruang Pemeriksaan")
    jenis_ruang = fields.Many2one(comodel_name="ruang.pemeriksaan", string="Nama Ruang Pemeriksaan")
    date_start = fields.Date(string="Waktu Pemeriksaan", default=fields.Date.context_today)
    nama_dokter = fields.Many2one(comodel_name="res.partner", string="Nama Dokter")


    # Rekam Medis
    tek_darah = fields.Char(string="Tekanan Darah")
    gol_darah = fields.Many2one(comodel_name="gol.darah", string="Gol Darah")
    tinggi_badan= fields.Char(string="Tinggi Badan")
    berat_badan= fields.Char(string="Berat Badan")
    alergi = fields.Char(string="Alergi")
    pemeriksa = fields.Many2one(comodel_name="res.partner",string ="Nama Bidan", required=True )
    diagnosa = fields.Text(string="Ket Diagnosa Sementara",required=True)

    # Photo
    before_photo = fields.Binary(string="Sebelum Pemeriksaan")
    after_photo = fields.Binary(string="Setelah Pemeriksaan")

    # Diagnosa
    tgl_diagnosa = fields.Date(string="Waktu Pemeriksaan", default=fields.Date.context_today)
    
 