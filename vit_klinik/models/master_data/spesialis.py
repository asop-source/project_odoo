# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Spesialis(models.Model):
    _name = 'spesialis'


    name = fields.Char(string="Spesialis Dokter")
    no_id = fields.Char(string="NO ID")