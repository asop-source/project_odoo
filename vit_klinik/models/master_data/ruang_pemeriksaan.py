# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Ruang(models.Model):
    _name = 'ruang.pemeriksaan'

    name = fields.Char(string="Nama Ruang Pemeriksaan", required=True)
    no_id = fields.Char(string="Nomor Kamar Pemeriksaan", required=True)