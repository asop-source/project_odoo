from odoo import models, fields, api
import datetime

class Diagnosa(models.Model):
    _name = 'diagnosa'


    dokter = fields.Many2one(comodel_name="dokter",string ="Dokter")
    date_start = fields.Date(string="Waktu Pemeriksaan", default=fields.Date.context_today)
    diagnosa = fields.Text(string="Hasil Diagnosa",required=True)