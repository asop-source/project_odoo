from odoo import models, fields, api
import datetime

class Photo(models.Model):
    _name = 'photo'


    before_photo = fields.Binary(string="Sebelum Pemeriksaan")
    after_photo = fields.Binary(string="Setelah Pemeriksaan")