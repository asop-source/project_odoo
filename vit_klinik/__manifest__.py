# -*- coding: utf-8 -*-
{
    'name': "Sistem Informasi Management Klinik",

    'summary': """

        Business Rumah Sakit
        
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com",
    'website': "http://www.vitraining.com",


    'category': 'Module Management Klinik',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale_management','account','om_account_accountant'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/customer.xml',
        'views/pemeriksaan_dokter.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}