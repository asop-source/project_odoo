# -*- coding: utf-8 -*-

from odoo import models, fields, api
import time


class node3_report_pricelist(models.Model):
	_name = 'report.node3_report_pricelist.report_product_xlsx'
	_inherit = 'report.report_xlsx.abstract'


	def generate_xlsx_report(self, workbook, data, partners):
		cell_format = {}
		cell_format['title'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'valign': 'vcenter',
			'font_size': 20,
			'font_name': 'Arial',
		})

		cell_format['header'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'border': True,
			'font_name': 'Arial',
		})
		cell_format['content'] = workbook.add_format({
			'font_size': 11,
			'border': False,
			'font_name': 'Arial',
		})

		cell_format['content_float'] = workbook.add_format({
			'font_size': 11,
			'border': True,
			'num_format': '#,##0',
			'font_name': 'Arial',
		})

		headers = [
					"No",
					"Pricelist Toko",
					"Internal Reference / Barcode",
					"Barcode Vendor",
					"Nama Product",
					"Harga Lama",
					"Harga Baru",
					"Update Price",
					"Keterangan",
					]

		worksheet = workbook.add_worksheet('Report Perubahan Product')
		worksheet.write(1, 3, "Perubahan Product", cell_format['title'])

		column = 0
		row = 4
		for col in headers:
			worksheet.write(row, column, col, cell_format['header'])
			column += 1

		########### contents
		row = 5
		final_data=[]
		no=1
		for data in partners:
			final_data.append([
				no,
				data.pricelist_id.name,
				data.reference_barcode,
				data.vendor_barcode,
				data.product_tmpl_id.name,
				data.harga_lama,
				data.harga_baru,
				data.date_start.strftime("%d-%b-%Y"),
				data.note,

			])
			no += 1

		for data in final_data:
			column = 0
			for col in data:
				worksheet.write(row, column, col, cell_format['content'] if column < 0 else cell_format['content_float'])
				column += 1
			row += 1

		workbook.close()