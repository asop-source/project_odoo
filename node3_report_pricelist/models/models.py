# # -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _



class pricelist_new(models.Model):
	_inherit = 'product.pricelist.item'
	_description = "Product New Pricelist"
	_order = "product_id"


	reference_barcode = fields.Char('Internal Reference / Barcode',compute='_new_price')
	vendor_barcode = fields.Char('Barcode Vendor',compute='_new_price')
	harga_lama = fields.Integer(string='Harga Lama',compute='_harga_lama')
	harga_baru = fields.Integer(string='Harga Baru',compute='_new_price')
	update_price = fields.Date('Update Price',default=fields.Date.today)
	note = fields.Char(string='Keterangan',compute='_note_price')



	def _note_price(self):
		for rec in self:
			if rec.harga_lama == 0:
				rec.note = "Product Baru"
			if rec.harga_lama < rec.harga_baru:
				rec.note = 'Harga Naik'
			if rec.harga_lama > rec.harga_baru and rec.harga_lama != 0:
				rec.note = 'Harga Turun'

	def _harga_lama(self):
		for rec in self:
			product_wj = self.env['inventory.new.product'].search([])
			for x in product_wj:
				naik_obj = self.search([('date_start','<',x.date),
					('product_tmpl_id','=',rec.product_tmpl_id.id),
					('pricelist_id','=',rec.pricelist_id.id)])
				for obj in naik_obj:
					rec.harga_lama = obj.fixed_price

	def _new_price(self):
		for rec in self:
			product_wj = self.env['inventory.new.product'].search([])
			for x in product_wj:
				naik_obj = self.search([('date_start','=',x.date),
					('pricelist_id','=',rec.pricelist_id.id),
					('product_tmpl_id','=',rec.product_tmpl_id.id)])
				for obj in naik_obj:
					rec.reference_barcode = obj.product_tmpl_id.barcode
					rec.vendor_barcode = obj.product_tmpl_id.barcode_vendor
					rec.harga_baru = obj.fixed_price

