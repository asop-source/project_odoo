# -*- coding: utf-8 -*-
{
    'name': "node3 New Product Pricelists",

    'summary': """
        Tambah Menu Inventory New Product PriceLists

        """,

    'author': "asopkarawang@gmail.com",


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Report',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','product', 'mail','report_xlsx'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'wizard/inventory_new_product.xml',
        'views/views.xml',
        'report/pricelist.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}