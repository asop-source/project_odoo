# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class inventory_new_product(models.TransientModel):
	_name = 'inventory.new.product'
	_description = 'Inventory New Product'


	date = fields.Date('Pricelist at Date', default=fields.Date.today)


	def open_data(self):
		res = {
				'name' : _('Product New'),
				'domain' : [('date_start','=', self.date)],
				'view_type' : 'form',
				'res_model' : 'product.pricelist.item',
				'view_id' : False,
				'view_mode' : 'tree,form',
				'context' : {'group_by': ['pricelist_id']},
				'type':'ir.actions.act_window',
				}
		return res
