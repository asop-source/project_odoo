# -*- coding: utf-8 -*-

from datetime import datetime, time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError

class PurchaseOrderInherit(models.Model):
    _inherit = "sale.order"

    state = fields.Selection(selection_add=[('to approve', 'To Approve'),('reject', 'Reject')])

    outs_approval = fields.Many2one('res.users', string="Outstanding Approval", store=True, track_visibility='onchange')
    is_approval = fields.Boolean(string="Approval", compute='cek_approval', store=False)
    level_approval = fields.Integer(string="Level Approval")

    @api.multi
    def action_confirm(self):
        if self._get_forbidden_state_confirm() & set(self.mapped('state')):
            raise UserError(_(
                'It is not allowed to confirm an order in the following states: %s'
            ) % (', '.join(self._get_forbidden_state_confirm())))

        for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
            order.message_subscribe([order.partner_id.id])

        # self.write({
        #     'state': 'sale',
        #     'confirmation_date': fields.Datetime.now()
        # })
        # self._action_confirm()
        # if self.env['ir.config_parameter'].sudo().get_param('sale.auto_done_setting'):
        #     self.action_done()

        apps = self.env['node3.sales.approval'].search([('level','=', 1)])            
        if apps:
            self.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': 1}) 
            self.sendNotif()
        else:
            raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))

        return True

    @api.multi
    def button_approve(self):
        next_level = self.level_approval + 1
        apps = self.env['node3.sales.approval'].search([('level','=', next_level), ('minimal_amount', '<=', self.amount_total)])            
        if apps:
            self.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': next_level}) 
            self.sendNotif()
        else:
            self.write({
                'state': 'sale',
                'confirmation_date': fields.Datetime.now()
            })
            self._action_confirm()
            if self.env['ir.config_parameter'].sudo().get_param('sale.auto_done_setting'):
                self.action_done()  
        self.activity_feedback(['sales_senyum.mail_act_sales_approval'], user_id=self.env.user.id)
        return True

    @api.depends('is_approval')
    def cek_approval(self):
        for pr in self:
            if pr.outs_approval == self.env.user and pr.state == "to approve": # or self.env['res.users'].has_group('base.group_erp_manager') and pr.state == "to approve":
                pr.is_approval = True
            else:
                pr.is_approval = False    

    
    def sendNotif(self):
        self.activity_schedule(
            'sales_senyum.mail_act_sales_approval',
            user_id=self.outs_approval.id,)

class ConfigApproval(models.Model):
    _name = "node3.sales.approval"
    _description = "Sales Order Approval"

    name = fields.Char(string="Description")
    level = fields.Integer(string="Level", required=True)
    minimal_amount = fields.Float(string="Min Amount")
    approval_id = fields.Many2one('res.users', string="Approval", required=True)
        

    