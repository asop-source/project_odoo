# -*- coding: utf-8 -*-

from odoo import models, fields, api

class node3_auto_adjustments(models.Model):
	_inherit = 'stock.inventory.line'

	in_reference = fields.Char('Internal Barcode',compute='_in_barcode',store=True)
	vendor_barcode = fields.Char('Barcode Vendor',compute='_barcode_vendor',store=True)
	article = fields.Char('Article',compute='_in_article',store=True)
	# uom_2 = fields.Many2one('uom.uom','Purchase uom',compute='_purcahse_uom')
	# note = fields.Char('Keterangan',compute='_note')




	# @api.depends('product_id')
	# def _note(self):
	# 	for x in self:
	# 		if x.picking_qty > x.product_qty and x.picking_qty != 0:
	# 			x.note = 'Barang Kurang'
	# 		if x.picking_qty < x.product_qty and x.picking_qty != 0:
	# 			x.note = 'Barang Lebih'


	# @api.depends('product_id')
	# def _purcahse_uom(self):
	# 	for data in self:
	# 		product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
	# 		for product in product_master:
	# 			data.uom_2 = product.uom_po_id.id


	@api.depends('product_id')
	def _in_barcode(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.in_reference = product.barcode

	@api.depends('product_id')
	def _barcode_vendor(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.vendor_barcode = product.barcode_vendor

	@api.depends('product_id')
	def _in_article(self):
		for data in self:
			product_master = self.env['product.template'].search([('name','=',data.product_id.name)])
			for product in product_master:
				data.article = product.article


