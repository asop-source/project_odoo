# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    assigned_to = fields.Many2one(
        'res.users', 'Approver', track_visibility='onchange',
    )

    pr_type = fields.Selection([
            ('asset_comp', 'Asset Computer'),
            ('asset_other', 'Asset Other'),
            ('bjm', 'Barang Jual Import'),
            ('bjl', 'Barang Jual Lokal'),
            ('service', 'Service'),
            ('consu', 'Consumable'),
            ('op', 'Orang Pribadi'),
        ], string='PR Type', default='asset_comp', readonly=True, states={'draft': [('readonly', False)]})
    department_id = fields.Many2one('hr.department', default=lambda self: self.env.user.employee_ids.department_id, string="Department", readonly=True, states={'draft': [('readonly', False)]})
    is_approval = fields.Boolean(string="Approval", compute='cek_approval', store=False)
    level_approval = fields.Integer(string="Level Approval",)

    @api.depends('is_approval')
    def cek_approval(self):
        for pr in self:
            if pr.assigned_to == self.env.user and pr.state == "to_approve": # or self.env['res.users'].has_group('base.group_erp_manager') and pr.state == "to approve":
                pr.is_approval = True
            else:
                pr.is_approval = False
    
    @api.multi
    def button_to_approve(self):
        self.to_approve_allowed_check()        
        pr = self.env['purchase.request.approval'].search([('department_id','=', self.department_id.id), ('level','=', 1)])            
        if pr:
            self.write({'state': 'to_approve', 'assigned_to': pr[0].approval_id.id, 'level_approval': 1}) 
        else:
            raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))

        return True

    @api.multi
    def button_approved(self):
        # return self.write({'state': 'approved'})
        for order in self:
            next_level = order.level_approval + 1
            apps = self.env['purchase.request.approval'].search([('department_id','=', order.department_id.id), ('level','=', next_level)])            
            if apps:
                order.write({'state': 'to_approve', 'assigned_to': apps[0].approval_id.id, 'level_approval': next_level}) 
            else:
                order.write({'state': 'approved'})         
        return True


    @api.multi
    def to_approve_allowed_check(self):
        for rec in self:
            if not rec.to_approve_allowed:
                raise UserError(
                    _("You can't request an approval for a purchase request "
                      "which is empty. (%s)") % rec.name)

class PrApproval(models.Model):
    _name = "purchase.request.approval"
    _description = "PR Approval"

    name = fields.Char(string="Description")
    department_id = fields.Many2one('hr.department', string="Department", required=True)
    level = fields.Integer(string="Level", required=True)
    approval_id = fields.Many2one('res.users', string="Approval", required=True)