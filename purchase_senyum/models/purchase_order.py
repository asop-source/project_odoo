# -*- coding: utf-8 -*-

from datetime import datetime, time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError

class PurchaseOrderInherit(models.Model):
    _inherit = "purchase.order"

    pr_id = fields.Many2one('purchase.request', string="PR Reference", readonly=True, states={'draft': [('readonly', False)]}, domain=[('state', '=', 'purchase')])
    department_id = fields.Many2one('hr.department', default=lambda self: self.env.user.employee_ids.department_id, string="Department")

    outs_approval = fields.Many2one('res.users', string="Outstanding Approval", store=True, track_visibility='onchange')
    is_approval = fields.Boolean(string="Approval", compute='cek_approval', store=False)
    level_approval = fields.Integer(string="Level Approval")
    po_type = fields.Selection(related='pr_id.pr_type', store=True)
    po_type_2 = fields.Selection([
            ('asset_comp', 'Asset Computer'),
            ('asset_other', 'Asset Other'),
            ('bjm', 'Barang Jual Import'),
            ('bjl', 'Barang Jual Lokal'),
            ('service', 'Service'),
            ('consu', 'Consumable'),
            ('op', 'Orang Pribadi'),
        ], string='PR Type', default='asset_comp', readonly=True, states={'draft': [('readonly', False)]})

    @api.onchange('pr_id')
    def get_product(self):
        obj = []
        for pr in self:
            for product in pr.pr_id.line_ids:
                values = {}
                values['product_id'] = product.product_id
                values['product_qty'] = product.product_qty
                values['name'] = product.description
                values['date_planned'] = product.schedule_date.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                values['product_uom'] = product.product_uom_id
                obj.append((0,0, values))
        self.order_line = obj
        # raise ValidationError(_(obj))

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order._add_supplier_to_product()           
            apps = self.env['node3.po.approval'].search([('department_id','=', order.department_id.id), ('level','=', 1)])            
            if apps:
                order.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': 1}) 
            else:
                raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))
        
        return True

    @api.multi
    def approval_po(self):
        for order in self:
            next_level = order.level_approval + 1
            apps = self.env['node3.po.approval'].search([('department_id','=', order.department_id.id), ('level','=', next_level), ('minimal_amount', '<=', order.amount_total)])            
            if apps:
                order.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': next_level}) 
            else:
                order.button_approve()                
        return {}

    @api.depends('is_approval')
    def cek_approval(self):
        for pr in self:
            if pr.outs_approval == self.env.user and pr.state == "to approve": # or self.env['res.users'].has_group('base.group_erp_manager') and pr.state == "to approve":
                pr.is_approval = True
            else:
                pr.is_approval = False
    
    @api.multi
    def button_approve(self, force=True):
        res = super(PurchaseOrderInherit, self).button_approve()
        for order in self: 
            if order.state == 'purchase':
                # order.name = self.env['ir.sequence'].next_by_code('purchase.order') or _('New')
                order.outs_approval = False
                # pr = self.env['node3.pr'].search([('id','=', order.pr_id.id)])
                # if pr:
                #     print(pr[0].name)
                #     pr[0].write({'state':'done'})
        return res

class ConfigApproval(models.Model):
    _name = "node3.po.approval"
    _description = "PO Approval"

    name = fields.Char(string="Description")
    department_id = fields.Many2one('hr.department', string="Department", required=True)
    level = fields.Integer(string="Level", required=True)
    minimal_amount = fields.Float(string="Min Amount")
    approval_id = fields.Many2one('res.users', string="Approval", required=True)
        

    