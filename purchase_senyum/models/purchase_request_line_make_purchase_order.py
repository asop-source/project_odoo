from odoo import models, fields, api, _
from odoo.exceptions import UserError

class InhPurchaseRequestLineMakePurchaseOrder(models.TransientModel):
    _inherit = 'purchase.request.line.make.purchase.order'

    pr_id = fields.Many2one('purchase.request', string="PR Reference", readonly=True, states={'draft': [('readonly', False)]}, domain=[('state', '=', 'purchase')])

    @api.model
    def _prepare_purchase_order(self, picking_type, group_id, company, origin):
        if not self.supplier_id:
            raise UserError(
                _('Enter a supplier.'))

        print(origin)
        raise UserError(
                _('Enter a supplier.'))
        supplier = self.supplier_id
        data = {
            'origin': origin,
            'partner_id': self.supplier_id.id,
            'fiscal_position_id': supplier.property_account_position_id and
            supplier.property_account_position_id.id or False,
            'picking_type_id': picking_type.id,
            'company_id': company.id,
            'group_id': group_id.id,
            }
        return data