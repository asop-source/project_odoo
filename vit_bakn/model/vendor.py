#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class vendor(models.Model):

    _name = "vit_bakn.vendor"
    name = fields.Char( required=True, string="Name",  help="")
    job_position = fields.Char( string="Job position",  help="")
    mobile_phone = fields.Char( string="Mobile phone",  help="")
    email = fields.Char( string="Email",  help="")


    bakn_id = fields.Many2one(comodel_name="vit_bakn.bakn",  string="Bakn",  help="")
