#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class rfq(models.Model):

    _name = "purchase.order"

    _inherit = "purchase.order"


    bakn_ids = fields.One2many(comodel_name="vit_bakn.bakn",  inverse_name="rfq_id",  string="Bakns",  help="")

    bakn_count = fields.Integer("BAKNs", compute="_get_bakn_count")

    def _get_bakn_count(self):
        for rec in self:
            rec.bakn_count = len(rec.bakn_ids)

    @api.multi
    def action_view_bakn(self):
        '''
        This function returns an action that display existing BAKN of given purchase order ids.
        When only one found, show the BAKN immediately.
        '''
        action = self.env.ref('vit_bakn.action_vit_bakn_bakn')
        result = action.read()[0]
        # create_bill = self.env.context.get('create_bill', False)
        # override the context to get rid of the default filtering
        result['context'] = {
            # 'type': 'in_invoice',
            'default_rfq_id': self.id,
            'default_currency_id': self.currency_id.id,
            'default_company_id': self.company_id.id,
            'company_id': self.company_id.id
        }
        # choose the view_mode accordingly
        if len(self.bakn_ids) > 1 :
            result['domain'] = "[('id', 'in', " + str(self.bakn_ids.ids) + ")]"
        else:
            res = self.env.ref('vit_bakn.view_vit_bakn_bakn_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            # Do not set an invoice_id if we want to create a new bill.
            # if not create_bill:
            result['res_id'] = self.bakn_ids.id or False
        return result