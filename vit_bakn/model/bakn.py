#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class bakn(models.Model):

    _name = "vit_bakn.bakn"
    name = fields.Char( required=True, string="Name",  help="")
    date = fields.Datetime( string="Date",  help="", default=fields.Datetime.now, required=True )
    place = fields.Char( string="Place",  help="")
    agrements = fields.Text( string="Agrements",  help="", required=True )


    rfq_id = fields.Many2one(comodel_name="purchase.order",  string="RFQ",  help="")
    vendor_id = fields.Many2one(comodel_name="res.partner",  string="Vendor",  help="", readonly=True, related="rfq_id.partner_id")
    template_id = fields.Many2one(comodel_name="vit_bakn.template",  string="Template",  help="")
    pemberi_kerja_ids = fields.One2many(comodel_name="vit_bakn.pemberi_kerja",  inverse_name="bakn_id",  string="Pemberi kerjas",  help="")
    vendor_ids = fields.One2many(comodel_name="vit_bakn.vendor",  inverse_name="bakn_id",  string="Vendors",  help="")

    @api.onchange('template_id')
    def onchange_template_id(self):
        self.agrements = self.template_id.agrements

    @api.model
    def create(self, vals):
        vals['name']    = self.env['ir.sequence'].next_by_code('vit.bakn')
        return super(bakn, self).create(vals)
