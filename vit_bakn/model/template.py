#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class template(models.Model):

    _name = "vit_bakn.template"
    name = fields.Char( required=True, string="Name",  help="")
    agrements = fields.Text( string="Agrements",  help="", required=True )
