#!/usr/bin/python
#-*- coding: utf-8 -*-

from odoo import models, fields, api, _

class pemberi_kerja(models.Model):

    _name = "vit_bakn.pemberi_kerja"
    name = fields.Char( required=True, string="Name",  help="", related="employee_id.job_id.name")


    bakn_id = fields.Many2one(comodel_name="vit_bakn.bakn",  string="Bakn",  help="")
    employee_id = fields.Many2one(comodel_name="hr.employee",  string="Employee", required="True", help="")
    employee_ids = fields.Many2one(comodel_name="hr.employee",  string="Employees",  help="")

    mobile_phone = fields.Char(string="Mobile Phone", required=False, readonly=True, related="employee_id.mobile_phone")