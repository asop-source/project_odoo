# -*- coding: utf-8 -*-

from odoo import models, fields, api
import time



class node3_report_product_xlsx(models.Model):
	_name = 'report.report_product_xlsx.report_product_xlsx'
	_inherit = 'report.report_xlsx.abstract'


	def generate_xlsx_report(self, workbook, data, partners):
		cell_format = {}
		cell_format['title'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'valign': 'vcenter',
			'font_size': 20,
			'font_name': 'Arial',
		})

		cell_format['header'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'border': True,
			'font_name': 'Arial',
		})
		cell_format['content'] = workbook.add_format({
			'font_size': 11,
			'border': False,
			'font_name': 'Arial',
		})

		cell_format['content_float'] = workbook.add_format({
			'font_size': 11,
			'border': True,
			'num_format': '#,##0.00',
			'font_name': 'Arial',
		})

		headers = [
					"No",
					"Nama Product",
					"Kode Barcode",
					"Harga Baru",
					"Tanggal",
					]

		worksheet = workbook.add_worksheet('Report Perubahan Product')
		worksheet.write(1, 2, "Perubahan Product", cell_format['title'])

		column = 0
		row = 4
		for col in headers:
			worksheet.write(row, column, col, cell_format['header'])
			column += 1

		########### contents
		row = 5
		final_data=[]
		no=1
		for data in partners:
			if data == False:
				data = 0
			pass
			final_data.append([
				no,
				data.name,
				data.barcode,
				data.list_price,
				data.write_date.strftime("%d-%b-%Y"),

			])
			no += 1

		for data in final_data:
			column = 0
			for col in data:
				worksheet.write(row, column, col, cell_format['content'] if column < 0 else cell_format['content_float'])
				column += 1
			row += 1

		workbook.close()