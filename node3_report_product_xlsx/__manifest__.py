# -*- coding: utf-8 -*-
{
    'name': "node3 report product xlsx",

    'summary': """
        Report Product

        """,


    'author': "asopkarawang@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Report',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','report_xlsx'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'report/product.xml',
    ],

}