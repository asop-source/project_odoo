# -*- coding: utf-8 -*-
{
    'name': "Inventory",

    'summary': """
        1.Destionation location read only false on state ready""",

    'description': """
        Long description of module's purpose
    """,

    'author': "PT. Trimitra Sistem Solusindo",
    'website': "http://www.trimitrasis.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Stock/Inventory',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}