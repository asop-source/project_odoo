# coding: utf-8
# Copyright 2017 Vauxoo (https://www.vauxoo.com) info@vauxoo.com
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

from odoo import api, fields, models


# class ProductTemplate(models.Model):

#     _inherit = "product.template"

#     product_customer_code_ids = fields.One2many(
#         'product.customer.code', 'product_id', string='Product customer codes',
#         copy=False, help="Different codes that has the product for each "
#         "partner.")

#     @api.model
#     def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
#         res = super(ProductTemplate, self)._name_search(
#             name, args=args, operator=operator, limit=limit,name_get_uid=name_get_uid)
#         if res or not self._context.get('partner_id'):
#             return res
#         partner_id = self._context['partner_id']
#         product_customer_code_obj = self.env['product.customer.code']
#         prod_code_ids = product_customer_code_obj.sudo().search(
#             [('product_code', '=', name),
#              ('partner_id', '=', partner_id)], limit=limit)
#         # TODO: Search for product customer name
#         res = prod_code_ids.mapped('product_id').name_get()
#         return res

class Productproduct(models.Model):

    _inherit = "product.product"

    product_customer_code_ids = fields.One2many(
        'product.customer.code', 'product_id', string='Product customer codes',
        copy=False, help="Different codes that has the product for each "
        "partner.")

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        res = super(Productproduct, self)._name_search(
            name, args=args, operator=operator, limit=limit,name_get_uid=name_get_uid)
        if res or not self._context.get('partner_id'):
            return res
        partner_id = self._context['partner_id']
        product_customer_code_obj = self.env['product.customer.code']
        prod_code_ids = product_customer_code_obj.sudo().search(
            [('product_code', '=', name),
             ('partner_id', '=', partner_id)], limit=limit)
        # TODO: Search for product customer name
        res = prod_code_ids.mapped('product_id').name_get()
        return res