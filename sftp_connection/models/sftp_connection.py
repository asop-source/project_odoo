from odoo import models, fields, api, _
from odoo.exceptions import UserError
from io import BytesIO
from datetime import datetime
from ftplib import FTP
import os
import socket

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. Please install paramiko on your system. (sudo pip3 install paramiko)')


class sftp_connection(models.Model):
    _name = 'sftp.connection'

    name = fields.Char(string="Name", required=True, )
    
    ip_host = fields.Char(string="IP address", help="hosting name or ip addres without http", required=True, )
    port_host = fields.Char(string="Port", required=True, )
    username_login = fields.Char(string="Username", required=True, )
    password_login = fields.Char(string="Password", required=True, )
    role_type = fields.Selection([
            ('ho', 'HO'),
            ('cabang', 'Cabang'),
        ], string='Role Type', required=True, )
    server_type = fields.Selection([
            ('DEV', 'DEV'),
            ('QAS', 'QAS'),
            ('PRD', 'PRD'),
        ], string='Server Type', required=True, )
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
        ], string='Status', default='draft')
    ftp_default_folder = fields.Char(string="SFTP default folder", required=True, default="/Home")
    os_default_folder = fields.Char(string="Local default folder", required=True, default="/Home")
    sftp_complete_path = fields.Char(string="SFTP complete path", compute='get_path',)
    os_complete_path = fields.Char(string="Local complete path", compute='get_path',)


    @api.depends('ftp_default_folder', 'os_default_folder')
    def get_path(self):
        for rec in self:
            if rec.ftp_default_folder:
                rec.sftp_complete_path = ('%s/%s/' % (rec.ftp_default_folder, rec.server_type)).replace('//', '/')
            if rec.os_default_folder:
                rec.os_complete_path = ('%s/%s/' % (rec.os_default_folder, rec.server_type)).replace('//', '/')
        return True
    

    @api.multi
    def button_confirm(self):
        for rec in self:
            try:
                # Store all values in variables
                local_dir = rec.os_complete_path #"/python/"
                sftp_dir = rec.sftp_complete_path #"/home/tms_node3/ikram/"
                ip_host = rec.ip_host #"node3.id"
                port_host = rec.port_host #"22"
                username_login = rec.username_login #"root"
                password_login = rec.password_login #"6VLlcAp"
                
                try:
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(ip_host, port_host, username_login, password_login, timeout=20)
                    sftp = s.open_sftp()

                    try:
                        sftp.chdir(sftp_dir)
                    except IOError:
                        # Create directory and subdirs if they do not exist in sftp server.
                        currentDir = ''
                        for dirElement in sftp_dir.split('/'):
                            currentDir += dirElement + '/'
                            try:
                                sftp.chdir(currentDir)
                            except:
                                # Make directory and then navigate into it
                                sftp.mkdir(currentDir, 777)
                                sftp.chdir(currentDir)
                                pass
                    sftp.chdir(sftp_dir)
                    sftp.close()

                    try:
                        os.chdir(local_dir)
                    except IOError:
                        # Create directory and subdirs if they do not exist in sftp server.
                        currentDir = ''
                        for dirElement in local_dir.split('/'):
                            currentDir += dirElement + '/'
                            try:
                                os.chdir(currentDir)
                            except:
                                # Make directory and then navigate into it
                                os.mkdir(currentDir, 777)
                                os.chdir(currentDir)
                                pass
                    os.chdir(local_dir)
                except Exception as error:
                    raise UserError(_('Error connecting to remote server! Error: ' + str(error)))            
            except Exception as error:
                raise UserError(_('Error connecting to remote server! Error: ' + str(error)))              
            
            
            rec.write({'state': 'confirmed'})
                 
        return True



class synconize_object(models.Model):
    _name = 'syncronize.object'

    name = fields.Char(string="Name", required=True, )
    ftp_connection_id = fields.Many2one('sftp.connection', string="SFTP server", required=True)
    directory = fields.Char(string="Local directory", help="Please create '/' of end string path", required=True, )
    path_to_write_to = fields.Char(string="SFTP directory", help="Please create '/' of end string path", required=True, )
    file_name = fields.Char(string="File Name", help="File name must be contain extension", required=True, )
    object_to_sync = fields.Selection([
            ('product.template', 'Products'),
            ('pos.promotion', 'Promotion/Discount'),
        ], string='Object To Sync', default='products', required=True, )
    last_sync = fields.Datetime(string="Last Sync")
    domain_unique_field = fields.Text(string="Domain", required=True, default="[]",)


 
        