from odoo import api, fields, models, _
from odoo.exceptions import UserError
from io import BytesIO
import xlrd
import xlsxwriter
import base64
from datetime import datetime
from ftplib import FTP
import os
import socket

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. Please install paramiko on your system. (sudo pip3 install paramiko)')

class EksportProduct(models.Model):
    _name = "export.product"

    name = fields.Char(string="Name", required=True, )
    kode_cabang = fields.Char(string='Kode Cabang')
    sftp_id = fields.Many2one(
        comodel_name='sftp.connection',
        string='SFTP Connection', required=True, 
        )
    domain = fields.Text(string='Domain')
    last_sync = fields.Date(string='Last Sync')
    report_ids = fields.One2many('report.export.import', 'export_product_id', string='Export Reports',)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
        ], string='Status', default='draft')    
    sftp_complete_path = fields.Char(string="SFTP complete path", compute='get_path',)
    os_complete_path = fields.Char(string="Local complete path", compute='get_path',)


    @api.depends('kode_cabang', 'sftp_id')
    def get_path(self):
        for rec in self:
            if rec.kode_cabang or rec.sftp_id:
                sftp_complete_path = ('%s/%s/%s/' % (rec.sftp_id.sftp_complete_path, 'PRODUCT', rec.kode_cabang)).replace('//', '/') #/Home/Sync/DEV/PRODUCT/SOR/
                os_complete_path = ('%s/%s/%s/' % (rec.sftp_id.os_complete_path, 'PRODUCT', rec.kode_cabang)).replace('//', '/') #/Home/Sync/DEV/PRODUCT/SOR/

                rec.sftp_complete_path = sftp_complete_path
                rec.os_complete_path = os_complete_path
        return True

    @api.multi
    def button_confirm(self):
        for rec in self:
            try:
                # Store all values in variables
                local_dir = rec.os_complete_path #"/python/"
                sftp_dir = rec.sftp_complete_path #"/home/tms_node3/ikram/"
                ip_host = rec.sftp_id.ip_host #"node3.id"
                port_host = rec.sftp_id.port_host #"22"
                username_login = rec.sftp_id.username_login #"root"
                password_login = rec.sftp_id.password_login #"6VLlcAp"
                
                try:
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(ip_host, port_host, username_login, password_login, timeout=20)
                    sftp = s.open_sftp()

                    try:
                        sftp.chdir(sftp_dir)
                    except IOError:
                        # Create directory and subdirs if they do not exist in sftp server.
                        currentDir = ''
                        for dirElement in sftp_dir.split('/'):
                            currentDir += dirElement + '/'
                            try:
                                sftp.chdir(currentDir)
                            except:
                                # Make directory and then navigate into it
                                sftp.mkdir(currentDir, 777)
                                sftp.chdir(currentDir)
                                pass
                    sftp.chdir(sftp_dir)
                    #bikin folder outs, suks, fail
                    folder_result = ['OUTS', 'SUCS', 'FAIL']
                    for folder in folder_result:
                        currentDir = sftp_dir + folder + '/'
                        try:
                             sftp.chdir(currentDir)
                        except:
                            # Make directory and then navigate into it
                            sftp.mkdir(currentDir, 777)
                            sftp.chdir(currentDir)
                            pass

                    sftp.close()

                    try:
                        os.chdir(local_dir)
                    except IOError:
                        # Create directory and subdirs if they do not exist in sftp server.
                        currentDir = ''
                        for dirElement in local_dir.split('/'):
                            currentDir += dirElement + '/'
                            try:
                                os.chdir(currentDir)
                            except:
                                # Make directory and then navigate into it
                                os.mkdir(currentDir, 777)
                                os.chdir(currentDir)
                                pass
                    os.chdir(local_dir)
                    #bikin folder outs, suks, fail
                    folder_result = ['OUTS', 'SUCS', 'FAIL']
                    for folder in folder_result:
                        currentDir = local_dir + folder + '/'
                        try:
                             os.chdir(currentDir)
                        except:
                            # Make directory and then navigate into it
                            os.mkdir(currentDir, 777)
                            os.chdir(currentDir)
                            pass
                except Exception as error:
                    raise UserError(_('Error connecting to remote server! Error: ' + str(error)))            
            except Exception as error:
                raise UserError(_('Error connecting to remote server! Error: ' + str(error)))              
            
            
            rec.write({'state': 'confirmed'})
                 
        return True

        
        
    @api.multi
    def export_to_server(self):
        folder_result = ['OUTS', 'SUCS', 'FAIL']
        for rec in self:
                    
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            dt = rec.last_sync
            domain = eval(rec.domain)
            if dt:
                domain =  domain + ['|', ('write_date', '>=', dt), ('create_date', '>=', dt)]
            
            data_product = self.env['product.template'].search(domain)

            #set last sync date
            rec.write({'last_sync' : datetime.utcnow()})
            
            if not data_product:
                raise UserError(_("Updated data not found"))
            
            worksheet = workbook.add_worksheet()
            
            row = 1
            final_data=[]
            for data in data_product :
                final_data.append([
                    data.default_code,
                    data.barcode,
                    data.name,
                    data.standard_price,
                    data.list_price,
                    data.categ_id.complete_name,
                    data.uom_id.name,
                    data.uom_po_id.name,
                    data.type,
                    data.property_stock_inventory.complete_name,
                    data.invoice_policy,
                    data.purchase_method,
                ])
                
            for data in final_data:
                column = 0
                for col in data:
                    worksheet.write(row, column, col)
                    column += 1
                row += 1
                
            workbook.close()

            current_date = str(datetime.now().strftime("%m%d%Y%H%M%S"))
            my_filename = "PRODUCT-" + rec.kode_cabang + current_date + ".xlsx"
            path = rec.os_complete_path + folder_result[0] + "/" + my_filename
            result = base64.encodestring(fp.getvalue())

            outfile = open(path, 'wb')
            binary_format = bytearray(fp.getvalue())
            outfile.write(binary_format)
            outfile.close()

            #send file to ftp
            try:
                # Store all values in variables
                dir = path  #"/python/"
                filename = my_filename #"Book1.xlsx"
                pathToWriteTo = rec.sftp_complete_path + folder_result[0] #"/home/tms_node3/ikram/"
                pathToWriteToWithFile = pathToWriteTo + "/" + my_filename
                ipHost = rec.sftp_id.ip_host #"node3.id"
                portHost = rec.sftp_id.port_host #"22"
                usernameLogin = rec.sftp_id.username_login #"root"
                passwordLogin = rec.sftp_id.password_login #"6VLlcAp"

                try:
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(ipHost, portHost, usernameLogin, passwordLogin, timeout=20)
                    sftp = s.open_sftp()
                except Exception as error:
                    raise UserError('Error connecting to remote server! Error: ' + str(error))

                try:
                    sftp.chdir(pathToWriteTo)
                except Exception as error:
                    raise UserError('Path not found ' + str(error))

                sftp.chdir(pathToWriteTo)
                # Loop over all files in the directory.
                if os.path.isfile(dir):
                    try:
                        sftp.stat(pathToWriteToWithFile)
                        UserError('File %s already exists on the remote FTP Server ------ skipped' % pathToWriteTo)
                    # This means the file does not exist (remote) yet!
                    except IOError:
                        try:
                            # sftp.put(fullpath, pathToWriteTo)
                            sftp.put(dir, pathToWriteToWithFile)
                        except Exception as err:
                            raise UserError(
                                'We couldn\'t write the file to the remote server. Error: ' + str(err))

                # Navigate in to the correct folder.
                # sftp.chdir(pathToWriteTo)
                sftp.close()
            except Exception as e:
                raise UserError('Exception! We couldn\'t back up to the FTP server..' + str(e))

        return {}

    @api.multi
    def export_pricelist_to_server(self):
        folder_result = ['OUTS', 'SUCS', 'FAIL']
        for rec in self:
                    
            fp = BytesIO()
            workbook = xlsxwriter.Workbook(fp)
            dt = rec.last_sync
            domain = eval(rec.domain)
            if dt:
                domain =  domain + ['|', ('write_date', '>=', dt), ('create_date', '>=', dt)]
            
            data_product = self.env['product.pricelist.item'].search(domain)

            #set last sync date
            rec.write({'last_sync' : datetime.utcnow()})
            
            if not data_product:
                raise UserError(_("Updated data not found"))
            
            worksheet = workbook.add_worksheet()
            
            row = 1
            final_data=[]
            for data in data_product.sorted(key=lambda l: l.pricelist_id.id):
                date_start = data.date_start.strftime("%m/%d/%Y") if data.date_start != False else False
                date_end = data.date_end.strftime("%m/%d/%Y") if data.date_end != False else False
                final_data.append([
                    #header
                    self.env["product.pricelist"].search([('id', '=', data.pricelist_id.id)]).name,
                    self.env["product.pricelist"].search([('id', '=', data.pricelist_id.id)]).sequence,
                    self.env["product.pricelist"].search([('id', '=', data.pricelist_id.id)]).currency_id.name,
                    self.env["product.pricelist"].search([('id', '=', data.pricelist_id.id)]).discount_policy,
                    #items
                    data.applied_on,
                    data.base,
                    data.compute_price,
                    data.min_quantity,
                    data.percent_price,
                    data.price_discount,
                    data.price_surcharge,
                    data.product_id.default_code,
                    data.product_tmpl_id.name,
                    data.categ_id.complete_name,
                    date_start,
                    date_end,
                    data.price_max_margin,
                    data.price_min_margin,
                    data.display_name
                ])
                
            for data in final_data:
                column = 0
                for col in data:
                    worksheet.write(row, column, col)
                    column += 1
                row += 1
                
            workbook.close()

            current_date = str(datetime.now().strftime("%m%d%Y%H%M%S"))
            my_filename = "PRICELIST-" + rec.kode_cabang + current_date + ".xlsx"
            path = rec.os_complete_path + folder_result[0] + "/" + my_filename
            result = base64.encodestring(fp.getvalue())

            outfile = open(path, 'wb')
            binary_format = bytearray(fp.getvalue())
            outfile.write(binary_format)
            outfile.close()

            #send file to ftp
            try:
                # Store all values in variables
                dir = path  #"/python/"
                filename = my_filename #"Book1.xlsx"
                pathToWriteTo = rec.sftp_complete_path + folder_result[0] #"/home/tms_node3/ikram/"
                pathToWriteToWithFile = pathToWriteTo + "/" + my_filename
                ipHost = rec.sftp_id.ip_host #"node3.id"
                portHost = rec.sftp_id.port_host #"22"
                usernameLogin = rec.sftp_id.username_login #"root"
                passwordLogin = rec.sftp_id.password_login #"6VLlcAp"

                try:
                    s = paramiko.SSHClient()
                    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    s.connect(ipHost, portHost, usernameLogin, passwordLogin, timeout=20)
                    sftp = s.open_sftp()
                except Exception as error:
                    raise UserError('Error connecting to remote server! Error: ' + str(error))

                try:
                    sftp.chdir(pathToWriteTo)
                except Exception as error:
                    raise UserError('Path not found ' + str(error))

                sftp.chdir(pathToWriteTo)
                # Loop over all files in the directory.
                if os.path.isfile(dir):
                    try:
                        sftp.stat(pathToWriteToWithFile)
                        UserError('File %s already exists on the remote FTP Server ------ skipped' % pathToWriteTo)
                    # This means the file does not exist (remote) yet!
                    except IOError:
                        try:
                            # sftp.put(fullpath, pathToWriteTo)
                            sftp.put(dir, pathToWriteToWithFile)
                        except Exception as err:
                            raise UserError(
                                'We couldn\'t write the file to the remote server. Error: ' + str(err))

                # Navigate in to the correct folder.
                # sftp.chdir(pathToWriteTo)
                sftp.close()
            except Exception as e:
                raise UserError('Exception! We couldn\'t back up to the FTP server..' + str(e))

        return {}


    

class ReportExportImport(models.Model):
    _inherit = 'report.export.import'

    export_product_id = fields.Many2one(
        comodel_name='export.product',
        string='Cabang'
        )



    