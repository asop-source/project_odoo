from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from io import BytesIO
import xlrd
import xlsxwriter
import base64
from datetime import datetime
from ftplib import FTP
import os
import socket

try:
    import paramiko
except ImportError:
    raise ImportError(
        'This module needs paramiko to automatically write backups to the FTP through SFTP. Please install paramiko on your system. (sudo pip3 install paramiko)')

 
class EksportImport(models.TransientModel):
    _name = "export.import"

    name = fields.Char(string="File Name")
    # types = fields.Selection([('eks','Export'), ('imp','Import')], string='Payment Method', default='eks')
    # tabel = fields.Many2one('ir.model', string="Object", required=True, )
    data_file = fields.Binary('File')
    syncronize_object_id = fields.Many2one('syncronize.object', string="SFTP server", required=True)
    
    @api.multi
    def manual_update(self):
        data = base64.decodestring(self.data_file)
     
        # Membaca file excel dengan library python yaitu xlrd
        excel = xlrd.open_workbook(file_contents = data)  
        
        # Menentukan sheet yang akan dibaca (sheet pertama)
        sh = excel.sheet_by_index(0)  
        
        # Mencetak nama sheet, jumlah baris, dan jumlah kolom
        # print(sh.name, sh.nrows, sh.ncols)  
        
        # Menjadikan list dari masing-masing baris excel lalu mencetaknya
        # for rx in range(sh.nrows):
        #     print([sh.cell(rx, ry).value for ry in range(sh.ncols)])
        

        # nama_po_old = ""
        # final_data = []
        # for nrow in range(sh.nrows):
        #     # final_data.append([
		# 	# 	sh.cell(rx, ry).value for ry in range(sh.ncols)
		# 	# ])
        #     nama_po = sh.cell(nrow,0).value
        #     nama_vendor = self.env["res.partner"].search([('name', '=', sh.cell(nrow,1).value)])

        #     if nrow == 0 or nama_po != nama_po_old:
        #         nama_po_old = nama_po
        #         new_id = self.env["purchase.order"].create({
        #                 'name': nama_po,
        #                 'partner_id': nama_vendor
        #             }).id


        #     nama_prod = sh.cell(nrow,2).value
        #     schedule_date = sh.cell(nrow,3).value
        #     prod_id = sh.cell(nrow,4).value
        #     prod_qty = sh.cell(nrow,5).value
        #     product_uom = sh.cell(nrow,6).value
        #     price_unit = sh.cell(nrow,7).value

        #     datetime_object = datetime.strptime(schedule_date, "%m/%d/%Y")

        #     self.env["purchase.order.line"].create({
        #             'name' : nama_prod,
        #             'product_id': prod_id,
        #             'date_planned': datetime_object,
        #             'product_qty': prod_qty,
        #             'order_id': new_id,
        #             'product_uom': product_uom,
        #             'price_unit' : price_unit
        #         })
            
        #     # for ncol in range(sh.ncols):
        #     #     nama_po = sh.cell(nrow, ncol).value

        ##########
        #PRODUXT##
        ##########

        object_to_sync = self.syncronize_object_id.object_to_sync
        domain_unique_field = self.syncronize_object_id.domain_unique_field

        final_data = []
        for nrow in range(sh.nrows):
            if nrow == 0:
                continue

            default_code = sh.cell(nrow,0).value if sh.cell(nrow,0).value != 0 else False
            barcode = sh.cell(nrow,1).value if sh.cell(nrow,1).value != 0 else False
            name = sh.cell(nrow,2).value
            standard_price = sh.cell(nrow,3).value
            list_price = sh.cell(nrow,4).value
            categ_id = self.env["product.category"].search([('complete_name', '=', sh.cell(nrow,5).value)]).id
            uom_id = self.env["uom.uom"].search([('name', '=', sh.cell(nrow,6).value)]).id
            uom_po_id = self.env["uom.uom"].search([('name', '=', sh.cell(nrow,7).value)]).id
            type = sh.cell(nrow,8).value
            property_stock_inventory = self.env["stock.location"].search([('complete_name', '=', sh.cell(nrow,9).value)]).id
            invoice_policy = sh.cell(nrow,10).value
            purchase_method = sh.cell(nrow,11).value

            cek_data_exist = self.env[object_to_sync].search([('name','=',name),('barcode','=',barcode)])
            
            if cek_data_exist:
                new_id = cek_data_exist.write({
                    'default_code' : default_code,
                    'barcode' : barcode,
                    'name' : name,
                    'standard_price' : standard_price,
                    'list_price' : list_price,
                    'categ_id' : categ_id,
                    'uom_id' : uom_id,
                    'uom_po_id' : uom_po_id,
                    'type' : type,
                    'property_stock_inventory' : property_stock_inventory,
                    'invoice_policy' : invoice_policy,
                    'purchase_method' : purchase_method,
                })
            else:
                new_id = self.env["product.template"].create({
                    'default_code' : default_code,
                    'barcode' : barcode,
                    'name' : name,
                    'standard_price' : standard_price,
                    'list_price' : list_price,
                    'categ_id' : categ_id,
                    'uom_id' : uom_id,
                    'uom_po_id' : uom_po_id,
                    'type' : type,
                    'property_stock_inventory' : property_stock_inventory,
                    'invoice_policy' : invoice_policy,
                    'purchase_method' : purchase_method,
                }).id

        # hname_old = ""
        # final_data = []
        # for nrow in range(sh.nrows):
        #     if nrow == 0:
        #         continue

        #     hname = sh.cell(nrow,0).value
        #     hsequence = sh.cell(nrow,1).value
        #     hcurrency_id = self.env["res.currency"].search([('name', '=', sh.cell(nrow,2).value)]).id
        #     hdiscount_policy = sh.cell(nrow,3).value
            
        #     hcek_data_exist = self.env['product.pricelist'].search([('name','=',hname)])

        #     if nrow == 1 or hname != hname_old:
        #         hname_old = hname
        #         if hcek_data_exist:
        #             self.env["product.pricelist"].write({
        #                 'name': hname,
        #                 'sequence': hsequence,
        #                 'currency_id': hcurrency_id,
        #                 'discount_policy': hdiscount_policy,
        #             })
        #             new_id = hcek_data_exist.id
        #             #delete all item and add it again
        #             hcek_data_exist.item_ids = [(5, 0, 0)] 
        #         else:
        #             new_id = self.env["product.pricelist"].create({
        #                 'name': hname,
        #                 'sequence': hsequence,
        #                 'currency_id': hcurrency_id,
        #                 'discount_policy': hdiscount_policy,
        #             }).id

        #     iapplied_on = sh.cell(nrow,4).value
        #     ibase = sh.cell(nrow,5).value
        #     icompute_price = sh.cell(nrow,6).value
        #     imin_quantity = sh.cell(nrow,7).value
        #     ipercent_price = sh.cell(nrow,8).value
        #     iprice_discount = sh.cell(nrow,9).value
        #     iprice_surcharge = sh.cell(nrow,10).value
        #     iproduct_id = self.env["product.product"].search([('default_code', '=', sh.cell(nrow,11).value)]).id
        #     iproduct_tmpl_id = self.env["product.template"].search([('name', '=', sh.cell(nrow,12).value)]).id
        #     icateg_id = self.env["product.category"].search([('complete_name', '=', sh.cell(nrow,13).value)]).id
        #     idate_start =  datetime.strptime(sh.cell(nrow,14).value, "%m/%d/%Y") if sh.cell(nrow,14).value != 0 else False
        #     idate_end =  datetime.strptime(sh.cell(nrow,15).value, "%m/%d/%Y") if sh.cell(nrow,15).value != 0 else False
        #     iprice_max_margin = sh.cell(nrow,16).value
        #     iprice_min_margin = sh.cell(nrow,17).value
        #     iname = sh.cell(nrow,18).value

        #     self.env["product.pricelist.item"].create({
        #         'applied_on' : iapplied_on,
        #         'base' : ibase,
        #         'min_quantity' : imin_quantity,
        #         'percent_price' : ipercent_price,
        #         'price_discount' : iprice_discount,
        #         'price_surcharge' : iprice_surcharge,
        #         'product_id' : iproduct_id,
        #         'product_tmpl_id' : iproduct_tmpl_id,
        #         'categ_id' : icateg_id,
        #         'date_start' : idate_start,
        #         'date_end' : idate_end,
        #         'price_max_margin' : iprice_max_margin,
        #         'price_min_margin' : iprice_min_margin,
        #         'pricelist_id' : new_id
        #     })


        return {}

    @api.multi
    def auto_update(self):
        self.ensure_one()

        #get file to ftp
        try:
            # Store all values in variables
            dir = self.syncronize_object_id.directory #"/python/"
            filename = self.syncronize_object_id.file_name #"Book1.xlsx"
            pathToWriteTo = self.syncronize_object_id.path_to_write_to #"/home/tms_node3/ikram/"
            ipHost = self.syncronize_object_id.ftp_connection_id.ip_host #"node3.id"
            portHost = self.syncronize_object_id.ftp_connection_id.port_host #"22"
            usernameLogin = self.syncronize_object_id.ftp_connection_id.username_login #"root"
            passwordLogin = self.syncronize_object_id.ftp_connection_id.password_login #"6VLlcAp"
            print('sftp remote path: %s' % pathToWriteTo)

            try:
                s = paramiko.SSHClient()
                s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                s.connect(ipHost, portHost, usernameLogin, passwordLogin, timeout=20)
                sftp = s.open_sftp()
            except Exception as error:
                raise ValidationError(_('Error connecting to remote server! Error: ' + str(error)))

            try:
                sftp.chdir(pathToWriteTo)
            except IOError:
                # Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in pathToWriteTo.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        sftp.chdir(currentDir)
                    except:
                        print('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        # Make directory and then navigate into it
                        sftp.mkdir(currentDir, 777)
                        sftp.chdir(currentDir)
                        pass
            sftp.chdir(pathToWriteTo)
            # Loop over all files in the directory.
            check_file_exist = False
            for f in sftp.listdir(pathToWriteTo):
                if filename == f:
                    check_file_exist = True
                    fullpath = os.path.join(pathToWriteTo, f)
                    if sftp.stat(fullpath):
                        try:
                            if os.path.isfile(os.path.join(dir, f)):
                                 print(
                                     'File %s already exists on the remote FTP Server ------ skipped' % fullpath)
                            else:
                                try:
                                    # sftp.put(fullpath, pathToWriteTo)
                                    sftp.get(os.path.join(pathToWriteTo, f), os.path.join(dir, f))
                                    print('Copying File % s------ success' % fullpath)
                                except Exception as err:
                                    print(
                                        'We couldn\'t write the file to the remote server. Error: ' + str(err))
                        # This means the file does not exist (remote) yet!
                        except Exception as err:
                            raise ValidationError(_('We couldn\'t write the file to the remote server. Error: ' + str(err)))

            #cek file di ftp server
            if not check_file_exist:
                raise ValidationError(_('File %s not exists on your FTP Server' % os.path.join(pathToWriteTo)))

            #cek file di lokal
            if not os.path.isfile(os.path.join(dir, f)):
                raise ValidationError(_('File %s not exists on your Server' % os.path.join(dir, filename)))

            # Membaca file excel dengan library python yaitu xlrd
            loc = (os.path.join(dir, filename))
            excel = xlrd.open_workbook(loc)  
            
            # Menentukan sheet yang akan dibaca (sheet pertama)
            sh = excel.sheet_by_index(0)  
            
            object_to_sync = self.syncronize_object_id.object_to_sync
            domain_unique_field = self.syncronize_object_id.domain_unique_field

            final_data = []
            for nrow in range(sh.nrows):
                if nrow == 0:
                    continue

                default_code = sh.cell(nrow,0).value if sh.cell(nrow,0).value != 0  else False
                barcode = sh.cell(nrow,1).value if sh.cell(nrow,1).value != 0 else False
                name = sh.cell(nrow,2).value
                standard_price = sh.cell(nrow,3).value
                list_price = sh.cell(nrow,4).value
                categ_id = self.env["product.category"].search([('complete_name', '=', sh.cell(nrow,5).value)]).id
                uom_id = self.env["uom.uom"].search([('name', '=', sh.cell(nrow,6).value)]).id
                uom_po_id = self.env["uom.uom"].search([('name', '=', sh.cell(nrow,7).value)]).id
                type = sh.cell(nrow,8).value
                property_stock_inventory = self.env["stock.location"].search([('complete_name', '=', sh.cell(nrow,9).value)]).id
                invoice_policy = sh.cell(nrow,10).value
                purchase_method = sh.cell(nrow,11).value

                cek_data_exist = self.env[object_to_sync].search([('name','=',name),('barcode','=',barcode)])
                
                if cek_data_exist:
                    new_id = cek_data_exist.write({
                        'default_code' : default_code,
                        'barcode' : barcode,
                        'name' : name,
                        'standard_price' : standard_price,
                        'list_price' : list_price,
                        'categ_id' : categ_id,
                        'uom_id' : uom_id,
                        'uom_po_id' : uom_po_id,
                        'type' : type,
                        'property_stock_inventory' : property_stock_inventory,
                        'invoice_policy' : invoice_policy,
                        'purchase_method' : purchase_method,
                    })
                else:
                    new_id = self.env[object_to_sync].create({
                        'default_code' : default_code,
                        'barcode' : barcode,
                        'name' : name,
                        'standard_price' : standard_price,
                        'list_price' : list_price,
                        'categ_id' : categ_id,
                        'uom_id' : uom_id,
                        'uom_po_id' : uom_po_id,
                        'type' : type,
                        'property_stock_inventory' : property_stock_inventory,
                        'invoice_policy' : invoice_policy,
                        'purchase_method' : purchase_method,
                    }).id
            
            #close excel
            excel.release_resources()
            #rename file
            current_date = str(datetime.now().strftime("%m%d%Y%H%M%S"))
            sftp.rename(fullpath, os.path.join(pathToWriteTo,  current_date+f))
            os.rename(loc, os.path.join(dir, current_date+filename))
            # Close the SFTP session.
            sftp.close()
            
        except Exception as e:
            raise ValidationError(_(str(e)))
        
        return {}

    @api.multi
    def export_to_local(self):
        print("export_to_local")
        
        
    @api.multi
    def export_to_server(self):
        self.ensure_one()
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        dt = self.syncronize_object_id.last_sync
        domain = eval(self.syncronize_object_id.domain_unique_field)
        if dt:
            domain =  domain + ['|', ('write_date', '>=', dt), ('create_date', '>=', dt)]
        
        data_product = self.env[self.syncronize_object_id.object_to_sync].search(domain)

        #set last sync date
        self.syncronize_object_id.write({'last_sync' : datetime.utcnow()})
        
        if not data_product:
            raise ValidationError(_("Updated data not found"))
        
        worksheet = workbook.add_worksheet()
        
        # row = 0
        # final_data=[]
        # for data in data_product :
        #     final_data.append([
		# 		data.order_id.name,
		# 		data.order_id.partner_id.id,
		# 		data.name,
		# 		data.date_planned.strftime("%m/%d/%Y"),
		# 		data.product_id.id,
		# 		data.product_qty,
		# 		data.product_uom.id,
		# 		data.price_unit,
		# 	])
            
        #     for data in final_data:
        #         column = 0
        #         for col in data:
        #             worksheet.write(row, column, col)
        #             column += 1
        #         row += 1
            
        # workbook.close()
        # # path = "/python/Book1.xlsx"
        
        # path = self.syncronize_object_id.directory + self.syncronize_object_id.file_name
        # result = base64.encodestring(fp.getvalue())

        # outfile = open(path, 'wb')
        # binary_format = bytearray(fp.getvalue())
        # outfile.write(binary_format)
        # outfile.close()

        ##############
        ##PRODUXT#####
        ##############

        row = 1
        final_data=[]
        for data in data_product :
            final_data.append([
				data.default_code,
                data.barcode,
                data.name,
                data.standard_price,
                data.list_price,
                data.categ_id.complete_name,
                data.uom_id.name,
                data.uom_po_id.name,
                data.type,
                data.property_stock_inventory.complete_name,
                data.invoice_policy,
                data.purchase_method,
			])
            
        for data in final_data:
            column = 0
            for col in data:
                worksheet.write(row, column, col)
                column += 1
            row += 1
            
        workbook.close()
        # path = "/python/Book1.xlsx"
        
        path = self.syncronize_object_id.directory + self.syncronize_object_id.file_name
        result = base64.encodestring(fp.getvalue())

        outfile = open(path, 'wb')
        binary_format = bytearray(fp.getvalue())
        outfile.write(binary_format)
        outfile.close()

        #send file to ftp
        try:
            # Store all values in variables
            dir = self.syncronize_object_id.directory #"/python/"
            filename = self.syncronize_object_id.file_name #"Book1.xlsx"
            pathToWriteTo = self.syncronize_object_id.path_to_write_to #"/home/tms_node3/ikram/"
            ipHost = self.syncronize_object_id.ftp_connection_id.ip_host #"node3.id"
            portHost = self.syncronize_object_id.ftp_connection_id.port_host #"22"
            usernameLogin = self.syncronize_object_id.ftp_connection_id.username_login #"root"
            passwordLogin = self.syncronize_object_id.ftp_connection_id.password_login #"6VLlcAp"
            print('sftp remote path: %s' % pathToWriteTo)

            try:
                s = paramiko.SSHClient()
                s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                s.connect(ipHost, portHost, usernameLogin, passwordLogin, timeout=20)
                sftp = s.open_sftp()
            except Exception as error:
                raise ValidationError('Error connecting to remote server! Error: ' + str(error))

            try:
                sftp.chdir(pathToWriteTo)
            except IOError:
                # Create directory and subdirs if they do not exist.
                currentDir = ''
                for dirElement in pathToWriteTo.split('/'):
                    currentDir += dirElement + '/'
                    try:
                        sftp.chdir(currentDir)
                    except:
                        print('(Part of the) path didn\'t exist. Creating it now at ' + currentDir)
                        # Make directory and then navigate into it
                        sftp.mkdir(currentDir, 777)
                        sftp.chdir(currentDir)
                        pass
            sftp.chdir(pathToWriteTo)
            # Loop over all files in the directory.
            for f in os.listdir(dir):
                if filename in f:
                    fullpath = os.path.join(dir, f)
                    if os.path.isfile(fullpath):
                        try:
                            sftp.stat(os.path.join(pathToWriteTo, f))
                            print(
                                'File %s already exists on the remote FTP Server ------ skipped' % fullpath)
                        # This means the file does not exist (remote) yet!
                        except IOError:
                            try:
                                # sftp.put(fullpath, pathToWriteTo)
                                sftp.put(fullpath, os.path.join(pathToWriteTo, f))
                                print('Copying File % s------ success' % fullpath)
                            except Exception as err:
                                raise ValidationError(
                                    'We couldn\'t write the file to the remote server. Error: ' + str(err))

            # Navigate in to the correct folder.
            sftp.chdir(pathToWriteTo)

            # Loop over all files in the directory from the back-ups.
            # We will check the creation date of every back-up.
            for file in sftp.listdir(pathToWriteTo):
                if filename in file:
                    # Get the full path
                    fullpath = os.path.join(pathToWriteTo, file)
                    # Get the timestamp from the file on the external server
                    timestamp = sftp.stat(fullpath).st_atime
                    createtime = datetime.fromtimestamp(timestamp)
                    now = datetime.now()
                    delta = now - createtime
                    # # If the file is older than the days_to_keep_sftp (the days to keep that the user filled in on the Odoo form it will be removed.
                    # if delta.days >= rec.days_to_keep_sftp:
                    #     # Only delete files, no directories!
                    #     if sftp.isfile(fullpath) and (".dump" in file or '.zip' in file):
                    #         print("Delete too old file from SFTP servers: " + file)
                    #         sftp.unlink(file)
            # Close the SFTP session.
            sftp.close()
        except Exception as e:
            raise ValidationError ('Exception! We couldn\'t back up to the FTP server..')

        return {}

        # filename = "kancut" #self.name_report + '-' + self.company_id.name + '%2Exlsx'
        # self.write({'data_file':result})
        # url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        # return {
		# 	'type': 'ir.actions.act_url',
		# 	'url': url,
		# 	'target': 'new',
		# }