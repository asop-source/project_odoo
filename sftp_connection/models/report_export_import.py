from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ReportExportImport(models.Model):
    _name = 'report.export.import'

    name = fields.Char(string='Filename')
    export_date = fields.Date(string='Export date')
    item_qty = fields.Integer(string='Item qty.')
    type = fields.Selection([("import","Import"),("export","Export")], string='type')
    data_file = fields.Binary(string='File')



    