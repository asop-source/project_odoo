# -*- coding: utf-8 -*-
{
    'name': "SFTP Connection",

    'summary': """
        SFTP Connection to sync data on PT. Senyum Pesona Timur""",

    'description': """
        SFTP Connection to sync data on PT. Senyum Pesona Timur
    """,

    'author': "PT. Trimitra Sistem Solusindo",
    'website': "http://www.trimitrasis.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/sftp_connection.xml',
        'views/export_product.xml',
        'views/menu.xml',
        'wizard/export_import.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}