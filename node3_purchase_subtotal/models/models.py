# -*- coding: utf-8 -*-

from odoo import models, fields, api

class node3_purchase_subtotal(models.Model):
	_inherit = 'purchase.request.line'


	sub_total = fields.Monetary(string='Sub Total', currency_field='currency_id', compute='_sub_total',
		help='Estimated cost of Purchase Request Line, not propagated to PO.')

	def _sub_total(self):
		for x in self:
			x.sub_total = x.product_qty * x.estimated_cost
