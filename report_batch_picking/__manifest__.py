# -*- coding: utf-8 -*-
{
    'name': "report batch picking",

    'summary': """
        Report Batch Picking

        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com",


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Report',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','inventory_multi_transfer'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'report/report_external_picking.xml',
        'report/report_internal_picking.xml',
        'report/menu_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}