# -*- coding: utf-8 -*-

from odoo import models, fields, api
from io import BytesIO
import xlsxwriter
import base64
from odoo.exceptions import Warning
from datetime import datetime

class report_batch_picking(models.Model):
	_inherit = 'stock.picking.batch'


	def cell_format(self, workbook):
		cell_format = {}
		cell_format['title'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'valign': 'vcenter',
			'font_size': 17,
			'font_name': 'Arial',
		})
		cell_format['header'] = workbook.add_format({
			'bold': True,
			'align': 'center',
			'border': True,
			'font_name': 'Arial',
		})
		cell_format['content'] = workbook.add_format({
			'font_size': 11,
			'border': False,
			'font_name': 'Arial',
		})
		cell_format['content_float'] = workbook.add_format({
			'font_size': 11,
			'border': True,
			'num_format': '#,##0.00',
			'font_name': 'Arial',
		})
		cell_format['content_number'] = workbook.add_format({
			'font_size': 11,
			'border': True,
			'num_format': '#,##0',
			'font_name': 'Arial',
		})
		cell_format['total'] = workbook.add_format({
			'bold': True,
			'num_format': '#,##0.00',
			'border': True,
			'font_name': 'Arial',
		})
		return cell_format, workbook




	data = fields.Binary('File')

	# @api.multi
	# def export_excel(self):
	# 	headers = [
	# 				"Nomor",
	# 				"Accounting Date",
	# 				"Inventory Reference",
	# 				"Inventoried Location",
	# 				"Inventories/Location",
	# 				"Inventories / Barcode",
	# 				"Inventories / Article",
	# 				"Inventories / Product",
	# 				"Inventories / Theoritical Quantity",
	# 				"Inventories / Checked Quantity",
	# 				"Inventories / Product Unit of Measure"
	# 				]
					

	# 	fp = BytesIO()
	# 	workbook = xlsxwriter.Workbook(fp)
	# 	cell_format, workbook = self.cell_format(workbook)

	# 	worksheet = workbook.add_worksheet()
	# 	worksheet.set_column('A:ZZ', 30)
	# 	column_length = len(headers)

	# 	########## parameters
	# 	worksheet.write(0, 4, "SURAT JALAN", cell_format['title'])
	# 	worksheet.write(1, 4, "Pengirim Barang", cell_format['title'])
	# 	worksheet.write(2, 0, "Tanggal", cell_format['content'])
	# 	worksheet.write(2, 1, str(self.date.strftime("%d-%b-%Y %H:%M:%S")), cell_format['content'])
	# 	worksheet.write(3, 0, "Penerima", cell_format['content'])
	# 	worksheet.write(3, 1, self.partner_id.name, cell_format['content'])
	# 	worksheet.write(4, 0, "No XPDS", cell_format['content'])
	# 	worksheet.write(4, 1, self.no_expds, cell_format['content'])
	# 	worksheet.write(5, 0, "No Seal", cell_format['content'])
	# 	worksheet.write(5, 1, self.no_seal, cell_format['content'])
	# 	worksheet.write(6, 0, "No Kendaraan", cell_format['content'])
	# 	worksheet.write(6, 1, self.no_kendaran, cell_format['content'])
	# 	worksheet.write(7, 0, "Tanggal Kirim", cell_format['content'])
	# 	worksheet.write(7, 1, str(self.tgl_kirim.strftime("%d-%b-%Y %H:%M:%S")), cell_format['content'])

	# 	########### header
	# 	column = 0
	# 	row = 9
	# 	for col in headers:
	# 		worksheet.write(row, column, col, cell_format['header'])
	# 		column += 1

	# 	########### contents
	# 	row = 10
	# 	final_data=[]
	# 	no= 1
	# 	for obj in self:
	# 		for x in self.picking_ids:
	# 			for data in x.move_ids_without_package:
	# 				final_data.append([
	# 					no,
	# 					date,
	# 					obj.name,
	# 					x.location_dest_id,
	# 					x.location_dest_id,
	# 					data.barcode_vendor,
	# 					data.article,
	# 					data.product_id.name,
	# 					data.quantity_done,
	# 					data.product_uom.name,
	# 					data.product_uom.name,

	# 				])
	# 				no += 1

	# 	for data in final_data:
	# 		column = 0
	# 		for col in data:
	# 			worksheet.write(row, column, col, cell_format['content'] if column < 0 else cell_format['content_number'])
	# 			column += 1
	# 		row += 1

	# 	workbook.close()
	# 	result = base64.encodestring(fp.getvalue())
	# 	filename = 'Report Internal Batch Picking'+ '%2Exlsx'
	# 	self.write({'data':result})
	# 	url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
	# 	return {
	# 		'type': 'ir.actions.act_url',
	# 		'url': url,
	# 		'target': 'new',
	# 	}


	@api.multi
	def export_checking(self):
		headers = [
					"Accounting Date",
					"Inventory Reference",
					"Inventoried Location",
					"Inventories/Location",
					"Inventories / Barcode",
					"Inventories / Article",
					"Inventories / Product",
					"Inventories / Theoritical Quantity",
					"Inventories / Checked Quantity",
					"Inventories / Product Unit of Measure"
					]

		fp = BytesIO()
		workbook = xlsxwriter.Workbook(fp)
		cell_format, workbook = self.cell_format(workbook)

		worksheet = workbook.add_worksheet()
		worksheet.set_column('A:ZZ', 30)
		column_length = len(headers)

		########### header
		column = 0
		row = 2
		for col in headers:
			worksheet.write(row, column, col, cell_format['header'])
			column += 1

		########### contents
		row = 3
		final_data=[]
		no= 1
		date_now = fields.Date.today()
		for obj in self:
			for x in obj.picking_ids:
				for data in x.move_ids_without_package:
					final_data.append([
						date_now.strftime("%d/%b/%y"),
						obj.name,
						x.location_dest_id.location_id.name+'/'+ x.location_dest_id.name,
						x.location_dest_id.location_id.name+'/'+ x.location_dest_id.name,
						data.barcode_vendor,
						data.article,
						data.product_id.name,
						data.quantity_done,
						" ",
						data.product_uom.name,

					])

		for data in final_data:
			column = 0
			for col in data:
				worksheet.write(row, column, col, cell_format['content'] if column < 0 else cell_format['content_number'])
				column += 1
			row += 1

		workbook.close()
		result = base64.encodestring(fp.getvalue())
		filename = 'Report Internal Batch Picking'+ '%2Exlsx'
		self.write({'data':result})
		url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
		return {
			'type': 'ir.actions.act_url',
			'url': url,
			'target': 'new',
		}
