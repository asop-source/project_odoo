# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class node3_adjustments_picking(models.Model):
	_inherit = 'stock.inventory'


	batch_picking_id = fields.Many2one('stock.picking.batch','Batch Picking')



	@api.model
	def _selection_filter(self):
		res = super(node3_adjustments_picking, self)._selection_filter()
		res_filter = [
			('none', _('All products')),
			('category', _('One product category')),
			('product', _('One product only')),
			('partial', _('Select products manually'))]

		res_filter.append(('batch', _('Batch Picking')))
		if self.user_has_groups('stock.group_tracking_owner'):
			res_filter += [('owner', _('One owner only')), ('product_owner', _('One product for a specific owner'))]
		if self.user_has_groups('stock.group_production_lot'):
			res_filter.append(('lot', _('One Lot/Serial Number')))
		if self.user_has_groups('stock.group_tracking_lot'):
			res_filter.append(('pack', _('A Pack')))
		return res_filter
		return res


	def action_start(self):
		res = super(node3_adjustments_picking, self).action_start()
		batch_oj = self.env['stock.picking.batch'].search([('name','=',self.batch_picking_id.name)]).filtered(lambda x: x.state == 'done')
		if self.filter == 'batch':
			Line_list = self.line_ids
			delete = Line_list.unlink()
			for batch in batch_oj.picking_ids:
				for picking in batch.move_ids_without_package:
					adjustment = {
						'product_id' : picking.product_id.id,
						'location_id' : batch.location_dest_id.id,
						'product_uom_id': picking.product_id.uom_id.id,
						'picking_qty' : picking.quantity_done,
						}
					self.line_ids = [(0,0,adjustment)]

		return res

	@api.multi
	def button_approve(self):
		result = super(node3_adjustments_picking, self).button_approve()
		if self.batch_picking_id:
			self.batch_picking_id.write({'state' : 'validate'})
		return result


	_sql_constraints = [
		('cek_number_picking', 'unique (batch_picking_id)','Number Batch Picking Has Process !!'),
		]

class BatchInventoryLine(models.Model):
	_inherit = "stock.inventory.line"


	picking_qty = fields.Integer('Picking Quantity')
	pricelist = fields.Integer('Pricelist',compute='_pricelist',store=True)
	pricelist_qty = fields.Integer('Pricelist Qty',compute='_pricelist_qty',store=True)

	# actual_qty = fields.Integer('Selisih Qty', compute='_actual_qty',store=True)

	# @api.depends('product_qty')
	# def _actual_qty(self):
	# 	for data in self:
	# 		if data.picking_qty == 0:
	# 			data.actual_qty = data.product_qty - data.theoretical_qty
	# 		if data.picking_qty != 0:
	# 			data.actual_qty = data.product_qty - data.picking_qty

	@api.depends('product_id')
	def _pricelist(self):
		for line in self:
			price_id = self.env['product.product'].search([('name','=',line.product_id.name)],limit=1)
			item_id = price_id.item_ids.search([('location_dest_id','=',line.location_id.id)],order='id desc',limit=1)
			for item in item_id:
				line.pricelist = item.fixed_price

	@api.depends('product_qty')
	def _pricelist_qty(self):
		for x in self:
			x.pricelist_qty = x.product_qty * x.pricelist