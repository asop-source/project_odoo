# -*- coding: utf-8 -*-
{
    'name': "node3 adjustments picking",

    'summary': """
        Adjustments Batch Picking

        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com",


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Adjustments',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','node3_product_barcode','inventory_auto_adjustments'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/adjustments.xml',
        'views/product_pricelist.xml',
        'views/batch_picking.xml',
    ],
}