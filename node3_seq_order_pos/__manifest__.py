# -*- coding: utf-8 -*-
{
    'name': "Sequence Order Pos",

    'description': """
        Perubahan Nomor Sequence Pada Pos Order
    """,

    'author': "asopkarawang@gmail.com",


    'category': 'POS',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','flexiretail_com_advance','point_of_sale'],

    # always loaded
    'data': [
        # 'data/ir_sequence.xml',
        'static/assets.xml'
    ],
    'qweb' : [
        "static/src/xml/order.xml"
    ],
}