odoo.define('node3_seq_order_pos.models', function (require) {
"use strict";

		var models = require('point_of_sale.models');
		var rpc = require('web.rpc');
		var screens = require('point_of_sale.screens');
		var OrderSuper = models.Order;
		models.Order = models.Order.extend({
		export_for_printing: function(){
		//tasos add order_ref to receipt
		var result = OrderSuper.prototype.export_for_printing.call(this);
		result.order_ref = this.order_ref;

		return result;
	},
});




models.PosModel = models.PosModel.extend({

push_order: function(order, opts) {
	opts = opts || {};
	var self = this;
	if(order){
		this.db.add_order(order.export_as_JSON());
	}
	var pushed = new $.Deferred();
	this.flush_mutex.exec(function(){
	var flushed = self._flush_orders(self.db.get_orders(), opts);
	flushed.always(function(ids){
	//tasos get order_ref from server
	if(order){
		rpc.query({
		model: 'pos.order',
		method: 'search_read',
		domain: [['pos_reference', '=', order.name]],
		fields: ['name'],
		limit: 1,
	})
	.then(function(data)
	{
		if(data.length > 0){
			order.order_ref = data[0].name;
			pushed.resolve();
			}
			});
		}
		else{
			pushed.resolve();
		}

		});
			return flushed;
				});
			return pushed;
		},
		});

	screens.PaymentScreenWidget.include({
	finalize_validation: function() {
		var self = this;
		var order = this.pos.get_order();
		if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) {
			this.pos.proxy.open_cashbox();
		}
	order.initialize_validation_date();
	order.finalized = true;
	if (order.is_to_invoice()) {
		var invoiced = this.pos.push_and_invoice_order(order);
		this.invoicing = true;
		invoiced.fail(function(error){
		self.invoicing = false;
		order.finalized = false;
	if (error.message === 'Missing Customer') {
		self.gui.show_popup('confirm',{
		'title': _t('Please select the Customer'),
		'body': _t('You need to select the customer before you can invoice an order.'),
		confirm: function(){
		self.gui.show_screen('clientlist');
	},
	});
	} else if (error.message === 'Backend Invoice') {
		self.gui.show_popup('confirm',{
		'title': _t('Please print the invoice from the backend'),
		'body': _t('The order has been synchronized earlier. Please make the invoice from the backend for the order: ') + error.data.order.name,
		confirm: function () {
		this.gui.show_screen('receipt');
	},
	cancel: function () {
		this.gui.show_screen('receipt');
	},
	});
	} else if (error.code < 0) { // XmlHttpRequest Errors
	self.gui.show_popup('error',{
		'title': _t('The order could not be sent'),
		'body': _t('Check your internet connection and try again.'),
	});
	} else if (error.code === 200) { // OpenERP Server Errors
		self.gui.show_popup('error-traceback',{
		'title': error.data.message || _t("Server Error"),
		'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
	});
	} else { // ???
		self.gui.show_popup('error',{
		'title': _t("Unknown Error"),
		'body': _t("The order could not be sent to the server due to an unknown error"),
	});
	}
	});
	invoiced.done(function(){
		self.invoicing = false;
		self.gui.show_screen('receipt');
	});
	} else {
	var self = this;
			//tasos to be synchronous
			this.pos.push_order(order).then(function() {
			self.gui.show_screen('receipt');
		});
		}
	},
	});
});
