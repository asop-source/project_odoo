# -*- coding: utf-8 -*-

from odoo import models, fields, api
from dateutil.relativedelta import relativedelta

class node3_product_beranak(models.Model):
	_inherit = 'stock.inventory.line'


	new_product = fields.Integer('Product Baru', compute='_calc_date',readonly=False)


	def _calc_date(self):
		for line in self:
			obj_new = self.search_count([('product_id','=',line.product_id.id),('theoretical_qty','!=',0)])
			if obj_new >= 1:
				line.new_product = 1
			elif line.theoretical_qty == 0:
				line.new_product = 0