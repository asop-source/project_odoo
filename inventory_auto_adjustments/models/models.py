# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError

class inventory_auto_adjustments(models.Model):
	_inherit = 'stock.inventory'

	counting_date = fields.Char('Counting Date',compute='calc_time')

	def calc_time(self):
		for rec in self:
			if rec.accounting_date:
				count_obj = datetime.strptime(str(rec.accounting_date),"%Y-%m-%d")
				date_obj = datetime.strptime(str(rec.date),"%Y-%m-%d %H:%M:%S")
				delta = date_obj - count_obj
				seconds = delta.total_seconds()
				d = divmod(seconds, 86400) #days
				if d[0] > 0:
					ret = "%s Days" %(int(d[0]))
				else:
					ret = "%s Days" %(int(d[0]))
				rec.counting_date = ret

	state = fields.Selection(selection_add=[('to approve', 'To Approve')], copy=False)
	inventory_approval = fields.Many2one(
		'res.users', store=True, readonly=True, track_visibility='onchange')
	is_approval = fields.Boolean(
		string="Approval", compute='cek_approval', store=False)
	approved_by = fields.Many2one(
		'res.users', store=True, readonly=True, track_visibility='onchange')
	level_approval = fields.Integer(string="Level Approval")

	@api.depends('is_approval')
	def cek_approval(self):
		for adj in self:
			if adj.inventory_approval == self.env.user and adj.state == "to approve":
				adj.is_approval = True
			else:
				adj.is_approval = False

	@api.multi
	def button_confirm(self):
		result = super(inventory_auto_adjustments, self).action_validate()
		for adj in self:
			approval = self.env["approval.inventory"].search([('level', '=', 1)])
			if approval:
				adj.write({'state': 'to approve', 'inventory_approval': approval[0].approval_id.id, 'level_approval': 1}) 
			else:
				raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))
		return result
		
	@api.multi
	def button_approve(self):
		for adj in self:
			adj.approved_by = adj.inventory_approval
			next_level = adj.level_approval + 1
			apps = self.env['approval.inventory'].search([('level','=', next_level)])            
			if apps:
				adj.write({'state': 'to approve', 'inventory_approval': apps[0].approval_id.id, 'level_approval': next_level}) 
			else: 
				adj.write({'state': 'done', 'inventory_approval': False})

class InventoryApproval(models.Model):
	_name = "approval.inventory"
	_description = "Inventory Adjustments Approval"
	
	name = fields.Char(string="Description")
	level = fields.Integer(string="Level", required=True)
	approval_id = fields.Many2one("res.users", string="Approval", required=True)

class inventory_line_auto_adjustments(models.Model):
	_inherit = 'stock.inventory.line'


	actual_qty = fields.Integer('Selisih Qty', compute='_actual_qty',store=True)

	@api.depends('product_qty')
	def _actual_qty(self):
		for data in self:
			data.actual_qty = data.product_qty - data.theoretical_qty


