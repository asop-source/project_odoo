# -*- coding: utf-8 -*-
{
    'name': "inventory auto adjustments",

    'summary': """
        Penambahan fields Pada inventory adjustment (Actual Qty,Harga Baru, Harga Lama dan no packing, Inventory Approval)
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com, zhafirah@trimitrasis.com",


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'inventory',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        # 'report/stock_adjustments.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}