odoo.define('node3_notic_product.models', function (require) {
"use strict";

	var models = require('point_of_sale.models');
	// var model_list = models.PosModel.prototype.models;
	models.load_fields('product.product',['harga_lama', 'is_get_colour']);

	return models;
});