from odoo import api, models, fields, _
# from datetime import datetime

class product_pos(models.Model):
	_inherit = 'product.product'


	harga_lama = fields.Integer('Harga Lama', compute='_calc_harga')
	date_now = fields.Date('Date Now',compute="_calc_date",readonly=False)
	create_now = fields.Date("Update Price",store=True)
	is_get_colour = fields.Boolean("POS Colour", compute='get_colour')

	def get_colour(self):
		for rec in self:
			d0 = rec.create_now
			if not d0:
				d0 = rec.create_date.date()
			d1 = rec.date_now
			delta = d1 - d0
			if delta.days <= 1:
				rec.is_get_colour = True
			else:
				rec.is_get_colour = False

	# def _calc_date(self):
	# 	for date in self:
	# 		date.date_now = fields.Date.today()

	# # @api.depends('item_ids.create_date')
	# def _calc_harga(self):
	# 	for data in self:
	# 		for item in data.item_ids:
	# 			date = item.search([('id','=',data.data_list())],order='id desc',limit=1)
	# 			pos_obj = self.env['pos.config'].search([('pricelist_id','=',item.pricelist_id.id),('pos_session_state','=','opened'),('current_session_state','=','opened')],order='id desc',limit=1)
	# 			pos_data = pos_obj.filtered(lambda r: r.session_ids)
	# 			for pos in pos_data:
	# 				data_item = item.search([('pricelist_id','=',pos.pricelist_id.id),('product_tmpl_id','=',item.product_tmpl_id.id),('id','<',data.data_list())],order='id desc')
	# 				for x in data_item: 
	# 					data.harga_lama = x.fixed_price
	# 					data.create_now = date.create_date.date()



	# def data_list(self):
	# 	# for line in self:
	# 	for data in self.item_ids:
	# 		pos_obj = self.env['pos.config'].search([('pricelist_id','=',data.pricelist_id.id),('pos_session_state','=','opened'),('current_session_state','=','opened')])
	# 		for pos in pos_obj:
	# 			sql = """
	# 					select id , pricelist_id from product_pricelist_item 
	# 					where pricelist_id =%s
	# 					order by id desc limit 1
	# 				"""
	# 			self._cr.execute(sql,[pos.pricelist_id.id])
	# 			data_list = self._cr.dictfetchall()
	# 			for res in data_list:
	# 				result = res['id'] if res['pricelist_id'] != None else 0
	# 				return result