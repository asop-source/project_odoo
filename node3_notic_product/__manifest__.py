{
    'name': "Notic Product POS",

    'summary': """
        Perubahan Warna Product di POS Dan Pricelist Product
        """,

    'author': "asopkarawang@gmail.com",

    'category': 'POS',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','sale','point_of_sale'],

    # always loaded
    'data': [
        'views/assets.xml',
        'views/product.xml',
    ],
    'qweb' : [
        "static/src/xml/product.xml"
    ],
    'installable' : True,

}