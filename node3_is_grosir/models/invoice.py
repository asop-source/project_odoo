from odoo import api, models, fields, _
# from datetime import datetime

class node3_account_invoice(models.Model):
	_inherit = 'account.invoice.line'


	@api.onchange('name','quantity')
	def _onchange_is_grosir(self):
		account = self.env['account.account'].search([('name','=','Penjualan Grosir')])
		for x in self:
			if self.partner_id.invoice:
				x.account_id = account.id




class node3_account_invoice_wizard(models.TransientModel):
	_inherit = "sale.advance.payment.inv"



	@api.multi
	def create_invoices(self):
		result = super(node3_account_invoice_wizard,self).create_invoices()

		account_account = self.env['account.invoice.line'].search([])
		account = self.env['account.account'].search([('name','=','Penjualan Grosir')])
		for obj in account_account:
			if obj.partner_id.invoice:
				obj.write({'account_id' : account.id })
		return result