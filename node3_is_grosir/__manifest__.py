{
    'name': "Node3 Is Grosir",

    'summary': """
            Perubahan Penjualan Retail menjadi Penjualan Grosir di invoice
        """,

    'author': "asopkarawang@gmail.com",

    'category': 'Accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','account','sale'],

    # always loaded
    'data': [
        'views/customer.xml',
    ],
    'installable' : True,

}