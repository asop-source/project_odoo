# -*- coding: utf-8 -*-
{
    'name': "Node3 Account",

    'summary': """
        Customs accounting module""",

    'description': """
        1. Add approval for payment module
        2. Add change color for invoice
        3. Custom payment receipt
        4. Received invoice date, to trigger due date
        5. Edit rule for button create invoice in sale order form
        6. Tambah note pada line item invoice
    """,

    'author': "PT. Trimitra Sistem Solusindo",
    'website': "http://www.node3.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Account',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account','purchase','hr', 'payment','sale','sale_management'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}