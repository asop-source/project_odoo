# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    state = fields.Selection(selection_add=[('to approve', 'To Approve'), ('validate', 'Validate')], track_visibility='onchange')
    approval_outstanding = fields.Many2one(
        'res.users', store=True, readonly=True, track_visibility='onchange')
    is_approval = fields.Boolean(
        string="Approval", compute='cek_approval', store=False)
    invoice_ids = fields.Many2many('account.invoice', 'account_invoice_payment_rel', 'payment_id', 'invoice_id', string="Invoices", copy=False, readonly=True, states={'draft': [('readonly', False)]},
        help="""Technical field containing the invoices for which the payment has been generated. This does not especially correspond to the invoices reconciled with the payment, as it can have been generated first, and reconciled later""")
    approved_by = fields.Many2one(
        'res.users', store=True, readonly=True, track_visibility='onchange')
    level_approval = fields.Integer(string="Level Approval")

    @api.depends('is_approval')
    def cek_approval(self):
        for pr in self:
            if pr.approval_outstanding == self.env.user and pr.state == "to approve": #or self.env['res.users'].has_group('base.group_erp_manager') and pr.state == "to approve":
                pr.is_approval = True
            else:
                pr.is_approval = False

    @api.multi
    def button_confirm(self):
        for rec in self:
            approval = self.env["node3.payment.approval"].search([('level', '=', 1)])
            if approval:
                rec.write({'state': 'to approve', 'approval_outstanding': approval[0].approval_id.id, 'level_approval': 1}) 
            else:
                raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))

    @api.multi
    def button_approve(self):
        for rec in self:
            rec.approved_by = rec.approval_outstanding
            next_level = rec.level_approval + 1
            apps = self.env['node3.payment.approval'].search([('level','=', next_level), ('minimal_amount', '<=', rec.amount)])            
            if apps:
                rec.write({'state': 'to approve', 'approval_outstanding': apps[0].approval_id.id, 'level_approval': next_level}) 
            else:
                rec.write({'state': 'validate', 'approval_outstanding': False})

    @api.multi
    def post(self):
        """ function ini di ambil dari modul account/models/account_payment.py
        """
        for rec in self:
            if rec.state != 'validate':
                raise UserError(_("Only a validate payment can be posted."))

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(
                    _("The payment cannot be processed because the invoice is not open!"))

            # keep the name in case of a payment reset to draft
            if not rec.name:
                # Use the right sequence to set the name
                if rec.payment_type == 'transfer':
                    sequence_code = 'account.payment.transfer'
                else:
                    if rec.partner_type == 'customer':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.customer.invoice'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.customer.refund'
                    if rec.partner_type == 'supplier':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.supplier.refund'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.supplier.invoice'
                rec.name = self.env['ir.sequence'].with_context(
                    ir_sequence_date=rec.payment_date).next_by_code(sequence_code)
                if not rec.name and rec.payment_type != 'transfer':
                    raise UserError(
                        _("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            amount = rec.amount * \
                (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)
            persist_move_name = move.name

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()
                persist_move_name += self._get_move_name_transfer_separator() + \
                    transfer_debit_aml.move_id.name

            rec.write({'state': 'posted', 'move_name': persist_move_name})
        return True

    @api.onchange('invoice_ids')
    def select(self):
        for rec in self:
            amount = 0
            for inv in rec.invoice_ids:
                amount = amount + inv.amount_total
            
            rec.amount = amount     
            if rec.invoice_ids:
                rec.partner_id = rec.invoice_ids[0].partner_id
    

    @api.onchange("partner_id")
    def domain_invoices(self):
        if self.partner_id:
            return {
                "domain": {
                    "invoice_ids": [
                        ("partner_id", "=", self.partner_id.id),
                        ("state", "in", ("open","in_payment")),
                    ]
                }
            }
        else:
            self.invoice_ids = False
            return {
                "domain": {
                    "invoice_ids": [
                        ("state", "in", ("open","in_payment")),
                    ]
                }
            }

class ConfigApproval(models.Model):
    _name = "node3.payment.approval"
    _description = "Payment Approval"

    name = fields.Char(string="Description")
    level = fields.Integer(string="Level", required=True)
    minimal_amount = fields.Float(string="Min Amount")
    approval_id = fields.Many2one('res.users', string="Approval", required=True)