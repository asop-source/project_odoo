from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import timedelta, datetime

from . import terbilang


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    due_date_count = fields.Integer(compute='calculate_date', store=False)
    date_received_inv = fields.Date(string='Received Invoice Date',
        readonly=False, states={'paid': [('readonly', True)]}, index=True,
        help="Invoice received by customer, you still can change this field after you confirm", copy=False)
    text_amount = fields.Text(string="Text Amount", store=False, compute='get_terbilang',)    
    text_amount_lang = fields.Selection([
            ('id', 'Indonesia'),
            ('en', 'English'),
        ], string='Text Amount Languange', default='id', help="Use this to change languange on text amount")

    def calculate_date(self):
        for r in self:
            today = datetime.today().strftime('%Y-%m-%d')
            if r.date_due:
                r.due_date_count = self.weekday_count(today, r.date_due)

    def weekday_count(self, start, end):
        start = datetime.strptime(str(start), "%Y-%m-%d")
        end = datetime.strptime(str(end), "%Y-%m-%d")
        daydiff = end.weekday() - start.weekday()
        days = ((end - start).days - daydiff) / 7 * 5 + min(
            daydiff, 5) - (max(end.weekday() - 4, 0) % 5)
        days = days + 1
        return days

    @api.onchange('payment_term_id', 'date_invoice', 'date_received_inv')
    def _onchange_payment_term_date_invoice(self):
        date_invoice = self.date_invoice
        date_received_inv = self.date_received_inv
        if not date_invoice:
            date_invoice = fields.Date.context_today(self)
            self.date_invoice = date_invoice
        if not date_received_inv:
            date_received_inv = fields.Date.context_today(self)
            self.date_received_inv = date_received_inv
        if self.payment_term_id:
            pterm = self.payment_term_id
            pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(value=1, date_ref=date_received_inv)[0]
            self.date_due = max(line[0] for line in pterm_list)
        elif self.date_due and (date_received_inv > self.date_due):
            self.date_due = date_received_inv

    def get_terbilang(self):
        for rec in self:
            amount = rec.amount_total
            currency = rec.currency_id.name
            lang = rec.text_amount_lang
            rec.text_amount = terbilang.terbilang(amount, currency, lang)


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    note = fields.Text(string="Note")

