# -*- coding: utf-8 -*-

from odoo import models, fields, api

class node3_kode_area(models.Model):
	_inherit = 'product.pricelist'

	kode_area = fields.Char('Kode Area')




class article_product(models.Model):
	_inherit = 'product.template'

	barcode_vendor = fields.Char('Barcode Vendor')
	article = fields.Text('Article')
