from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api, _
from odoo.exceptions import Warning, UserError
import time, xlsxwriter, base64, pytz
from io import StringIO, BytesIO
from pytz import timezone
import pdb
from odoo.exceptions import UserError, ValidationError


class VitKanban(models.Model):
    _name           = 'vit.kanban'
    _inherit        = ['mail.thread']
    _order          = "name desc, id desc"
    _description    = 'Kanban Transfer'


    name = fields.Char('Number',required=True, default='/')
    date_start = fields.Date('Date Start',
        default=lambda self: fields.Date.to_string(date.today().replace(day=1)))
    date_end = fields.Date('Date End',
        default=lambda self: fields.Date.to_string((datetime.now() + relativedelta(months=+1, day=1, days=-1)).date()))
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user)
    partner_id = fields.Many2one('res.partner','Partner', track_visibility='onchange')
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id, track_visibility='onchange')
    detail_ids = fields.Many2many('stock.move',string='Stock Move')
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),('cancel','Cancel'),('done','Done')], string='State',default='draft', track_visibility='onchange')
    reference = fields.Char('Reference', track_visibility='onchange')
    notes = fields.Text('Notes', track_visibility='onchange')
    picking_code = fields.Selection([('outgoing','Outgoing'),('incoming','Incoming')],string='Picking Code', track_visibility='onchange')
    date = fields.Date('Effective Date', default=fields.Date.context_today, track_visibility='on_change')
    # bea cukai
    nomor_aju_id = fields.Many2one('vit.nomor.aju', string='No Aju', ondelete='restrict', track_visibility='on_change')
    nama_pengangkut = fields.Char(string='Nama Pengangkut', track_visibility='on_change')
    tgl_ttd = fields.Date('Tanggal TTD', track_visibility='on_change')
    no_polisi = fields.Char('Nomor Polisi', track_visibility='on_change')
    seri_dokumen = fields.Char('Seri Dokumen', track_visibility='on_change')
    tgl_dokumen = fields.Date('Tanggal Dokumen', track_visibility='on_change')
    no_sj_pengirim = fields.Char('No SJ Pengirim', track_visibility='on_change')
    no_pabean = fields.Char('Nomor Pabean', size=100, track_visibility='on_change')
    tgl_pabean = fields.Date('Tanggal Pabean', track_visibility='on_change')
    status_aju = fields.Char('Status Dokumen', track_visibility='on_change')
    kode_cara_angkut = fields.Char('Kode Cara Angkut', default='3', track_visibility='on_change')
    kontainer_ids = fields.One2many('vit.kontainer', 'kanban_id', string='Kontainer', track_visibility='on_change')
    kemasan_ids = fields.One2many('vit.kemasan', 'kanban_id', string='Kemasan', ondelete='cascade')
    dokumen_ids = fields.One2many('vit.dokumen', 'kanban_id', string='Dokumen', ondelete='cascade')
    package_ids = fields.One2many('vit.kanban.package', 'kanban_id', string='Package Kecil', ondelete='cascade')
    package_big_ids = fields.One2many('vit.kanban.package.big', 'kanban_id', string='Package Besar', ondelete='cascade')
    #master_pendukung_id = fields.Many2one('vit.master.pendukung', string='Master Pendukung')
    kode_negara_pemasok_id = fields.Many2one('vit.kode.negara.pemasok', 'Kode Negara Pemasok', track_visibility='on_change')
    pelabuhan_bongkar_id = fields.Many2one('vit.pelabuhan.bongkar', 'Pelabuhan Bongkar', track_visibility='on_change')
    pelabuhan_transit_id = fields.Many2one('vit.pelabuhan.transit', 'Pelabuhan Transit', track_visibility='on_change')
    pelabuhan_muat_id = fields.Many2one('vit.pelabuhan.muat', 'Pelabuhan Muat', track_visibility='on_change')
    freight = fields.Float('Freight', track_visibility='on_change')
    ndpbm_id = fields.Many2one('vit.kurs.pajak', 'Kurs Pajak/NDPBM', track_visibility='on_change')
    tempat_penimbun_id = fields.Many2one('vit.tempat.penimbun', 'Tempat Penimbun', track_visibility='on_change')
    kode_harga_id = fields.Many2one('vit.kode.harga', 'Kode Harga', track_visibility='on_change')
    jenis_angkut_id = fields.Many2one('vit.jenis.angkut', string='Jenis Angkut', track_visibility='on_change')
    invoice_id = fields.Many2one("account.invoice","Invoice")
    is_bea_cukai = fields.Boolean('Is Bea Cukai', track_visibility='on_change', default=False)
    bc_type = fields.Selection([('BC 2.3','BC 2.3'),('BC 2.6.1','BC 2.6.1'),('BC 2.6.2','BC 4.6.2'),('BC 2.5','BC 2.5'),('BC 2.7','BC 2.7'),('BC 4.0','BC 4.0'),('BC 4.1','BC 4.1')],string='Jenis BC', track_visibility='on_change')

    @api.onchange('no_sj_pengirim')
    def onchange_no_sj_pengirim(self):
        if self.picking_code == 'incoming' :
            if not self.dokumen_ids :
                data = []
                dokumen_sj = self.env['vit.dokumen.master'].sudo().search([('code','=','640')],limit=1)
                if dokumen_sj :
                    data.append((0,0, {'dokumen_master_id':dokumen_sj.id,'nomor':self.no_sj_pengirim}))
                dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
                if dokumen_inv and self.invoice_id:
                    ref = self.invoice_id.reference
                    if not ref :
                        ref = self.invoice_id.name
                    data.append((0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':ref}))
                self.dokumen_ids = data
        elif self.picking_code == 'outgoing' :
            if not self.dokumen_ids :
                data = []
                dokumen_sj = self.env['vit.dokumen.master'].sudo().search([('code','=','640')],limit=1)
                if dokumen_sj :
                    data.append((0,0, {'dokumen_master_id':dokumen_sj.id,'nomor':self.name}))
                dokumen_inv = self.env['vit.dokumen.master'].sudo().search([('code','=','380')],limit=1)
                if dokumen_inv and self.invoice_id:
                    data.append((0,0, {'dokumen_master_id':dokumen_inv.id,'nomor':self.invoice_id.number}))
                self.dokumen_ids = data

    @api.model
    def create(self,vals):
        if vals['picking_code'] == 'outgoing' :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.kanban.out')
        elif vals['picking_code'] == 'incoming' :
            vals['name'] = self.env['ir.sequence'].next_by_code('vit.kanban.in')
        return super(VitKanban, self).create(vals)

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft':
                raise UserError(_('Data yang bisa dihapus hanya yang berstatus draft !'))
        return super(VitKanban, self).unlink()


    @api.multi
    def write(self, vals):
        for kbn in self :
            if 'detail_ids' in vals :
                jml_datails = len(kbn.detail_ids)
                if kbn.state == 'confirm' :
                    if jml_datails != len(vals['detail_ids'][0][2]):
                        raise UserError(_('Jika ingin hapus atau tambah lines detail harus pada status draft!'))
        return super(VitKanban, self).write(vals)

    @api.multi
    def sync_big_package(self):
        pack_k = self.env['vit.kanban.package']
        for big in self :
            for s in big.package_big_ids :
                for det in s.package_ids.filtered(lambda i:i.id not in big.package_ids.mapped('package_id.id')) :
                    lot = det.last_lot_id
                    if not lot:
                        last_trans = det.quant_ids.sorted('in_date')[0]
                        if last_trans.lot_id:
                            lot = last_trans.lot_id
                    pack_k.create({'kanban_id'      : big.id,
                                    'package_id'    : det.id,
                                    'product_id'    : det.last_product_id.id or s.product_id.id or False,
                                    'lot_id'        : lot.id or False,
                                    'quantity'      : det.last_quantity})

        return True


    @api.multi
    def sync_package(self):
        self.ensure_one()
        #import pdb;pdb.set_trace()
        self.sync_big_package()
        move_line = self.env['stock.move.line']
        for kbn in self.package_ids :
            # quant = kbn.package_id.quant_ids.sorted('in_date')[0]
            # quant.sudo().reserved_quantity = 0.0
            # quant.env.cr.commit()
            mapping_product = self.detail_ids.filtered(lambda x:x.product_id.id == kbn.product_id.id and x.quantity_done < x.new_demand)
            t_qty_done = 0
            for mapp in mapping_product :
                if (t_qty_done+kbn.quantity) > mapp.reserved_availability :
                    continue
                if kbn.move_id :
                    break
                if mapp.state != 'assigned' :
                    continue
                if mapp.new_demand > 0.0 :
                    qty = mapp.new_demand
                elif mapp.reserved_availability > 0.0 :
                    qty = mapp.reserved_availability
                else :
                    qty = mapp.product_uom_qty

                if kbn.quantity > qty :
                    continue
                if not mapp.move_line_ids :
                    move_line.create({'move_id': mapp.id,
                                        'product_id': mapp.product_id.id,
                                        'lot_id':kbn.lot_id.id,
                                        'package_id':kbn.package_id.id,
                                        'qty_done':kbn.quantity,
                                        'product_uom_id':mapp.product_id.uom_id.id,
                                        'location_id':mapp.location_id.id,
                                        'location_dest_id':mapp.location_dest_id.id})
                    t_qty_done += kbn.quantity
                    if move_line.product_uom_qty < move_line.qty_done :
                        move_line.sudo().unlink()
                    else :
                        kbn.move_id = mapp.id
                else :
                    # if mapp.new_demand > 0.0 :
                    #     if mapp.new_demand <= sum(mapp.move_line_ids.mapped('qty_done')):
                    #         continue
                    # elif mapp.reserved_availability > 0.0 :
                    #     if mapp.reserved_availability <= sum(mapp.move_line_ids.mapped('qty_done')):
                    #         continue
                    # elif mapp.product_uom_qty > 0.0 :
                    #     if mapp.product_uom_qty <= sum(mapp.move_line_ids.mapped('qty_done')):
                    #         continue              
                    lanjut = False
                    for line in mapp.move_line_ids :
                        if not line.kanban_line_id and not line.package_id and not line.lot_id :
                            sql = "update stock_move_line set result_package_id=null, package_id=%s, lot_id=%s, qty_done=%s where id=%s" % ( kbn.package_id.id,kbn.lot_id.id,kbn.quantity,line.id)
                            line.env.cr.execute(sql)
                            line.kanban_line_id = kbn.id
                            #############################
                            # line.package_id = kbn.package_id.id
                            # line.lot_id = kbn.lot_id.id
                            # line.qty_done = kbn.quantity
                            kbn.move_id = mapp.id
                            lanjut = True
                            t_qty_done += kbn.quantity
                            break
                    if lanjut :
                        continue
                    if qty > sum(mapp.move_line_ids.mapped('qty_done')) :
                        move_line.create({'move_id': mapp.id,
                                        'product_id': mapp.product_id.id,
                                        'lot_id':kbn.lot_id.id,
                                        'package_id':kbn.package_id.id,
                                        'qty_done':kbn.quantity,
                                        'product_uom_id':mapp.product_id.uom_id.id,
                                        'location_id':mapp.location_id.id,
                                        'location_dest_id':mapp.location_dest_id.id})
                        t_qty_done += kbn.quantity
                        if move_line.product_uom_qty < move_line.qty_done :
                            move_line.sudo().unlink()
                        else :
                            kbn.move_id = mapp.id
        
        #cek ulang jika ada yg double stock.move.lines

        # for line in self.detail_ids.filtered(lambda i:i.reserved_availability < i.quantity_done and i.state != 'confirmed'):
        #     for li in line.move_line_ids.filtered(lambda a:a.product_uom_qty == 0.0) :
        #         li.unlink()

        return True

    @api.multi
    def action_assign(self):
        moves = self.mapped('detail_ids').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        if not moves:
            raise UserError(_('Nothing to check the availability for.'))
        # If a package level is done when confirmed its location can be different than where it will be reserved.
        # So we remove the move lines created when confirmed to set quantity done to the new reserved ones.
        #import pdb;pdb.set_trace()
        for mv in moves.filtered(lambda m:m.location_id.usage not in ('supplier','customer')) :
            old_demand = mv.product_uom_qty
            mv.product_uom_qty = mv.new_demand
            mv._action_assign()
            mv_state = mv.state
            #mv.product_uom_qty = old_demand
            sql = "update stock_move set product_uom_qty=%s,product_qty=%s, state='draft' where id = %s " % ( old_demand,old_demand,mv.id) 
            self.env.cr.execute(sql)
            self.env.cr.commit()
            sql2 = "update stock_move set state='%s' where id = %s " % ( mv_state,mv.id)
            self.env.cr.execute(sql2)
            self.env.cr.commit()
            # reset package and lot
            #move_ids = str(tuple(moves.ids)).replace(',)', ')')
            sql = "update stock_move_line set package_id=null, result_package_id=null, lot_id=null, qty_done=0.0 where move_id = %s " % ( mv.id) 
            self.env.cr.execute(sql)
        # cek jika masih waiting
        auto_cancel =  moves.filtered(lambda m:m.location_dest_id.usage in ('supplier','customer') and m.state == 'confirmed')
        if auto_cancel :
            for x in auto_cancel :
                x._do_unreserve()
                quant = self.cek_quant(x.product_id,x.location_id)
                if quant :
                    if x.new_demand > 0.0 :
                        qty = x.new_demand
                    else:
                        qty = x.product_uom_qty
                    quant.write({'reserved_quantity': quant.reserved_quantity-qty})
        move_force =  moves.filtered(lambda m:m.location_id.usage in ('supplier','customer'))
        if move_force :
            move_force._action_assign()
        return True

    @api.multi
    def do_unreserve(self):
        for kanban in self:
            for pack in self.package_ids.filtered(lambda m:m.package_id.location_id.usage not in ('supplier','customer'))  :
                if pack.package_id.quant_ids :
                    p = pack.package_id.quant_ids.sorted('in_date')[0]
                    #p.sudo().reserved_quantity = 0.0
                    pack.move_id = False
            kbn = kanban.detail_ids.filtered(lambda m:m.location_id.usage not in ('supplier','customer'))
            if kbn :
                detail_ids = str(tuple(kbn.ids)).replace(',)', ')')
                #sql = "update stock_move_line set package_id=null, result_package_id=null, lot_id=null, qty_done=0.0, product_uom_qty=0.0, product_qty=0.0 where move_id in %s " % ( detail_ids)
                sql = "delete from stock_move_line where move_id in %s" % ( detail_ids,)
                kanban.env.cr.execute(sql)
                kanban.env.cr.commit()

    @api.onchange('detail_ids','date_start','date_end','partner_id','company_id','picking_code')
    def onchange_domain(self):
        domain = []
        #import pdb;pdb.set_trace()
        if self.picking_code and self.picking_code == 'outgoing' :
            obj_move = self.env['stock.move'].search([('picking_id.partner_id','=',self.partner_id.id),
                ('state','not in',('draft','done','cancel')),('company_id','=',self.company_id.id),
                ('picking_code','=','outgoing'),('date','>=',self.date_start),('date','<=',self.date_end),
                ('kanban_id','=',False)])
            domain = {'domain': {'detail_ids': [('id', 'in', obj_move.ids)],'partner_id':[('customer','=',True)]}}
        elif self.picking_code and self.picking_code == 'incoming' :
            #if self.partner_id.transaction_type != 'Import' :
            obj_move = self.env['stock.move'].search([('picking_id.partner_id','=',self.partner_id.id),
                ('state','in',('partially_available','assigned','waiting','confirmed')),('company_id','=',self.company_id.id),
                ('picking_code','=','incoming'),('date','>=',self.date_start),('date','<=',self.date_end),
                ('kanban_id','=',False)])
            domain = {'domain': {'detail_ids': [('id', 'in', obj_move.ids)],'partner_id':[('supplier','=',True)]}}
            # else :
            #     obj_move = self.env['stock.move'].search([('picking_id.group_id.partner_id','=',self.partner_id.id),
            #         ('state','in',('partially_available','assigned')),('company_id','=',self.company_id.id),
            #         ('picking_code','=','internal'),('date','>=',self.date_start),('date','<=',self.date_end),
            #         ('kanban_id','=',False),('picking_id.picking_type_id.warehouse_id.name','ilike','Pelabuhan')])
            #     domain = {'domain': {'detail_ids': [('id', 'in', obj_move.ids)]}}
        if domain :
            domain = domain
        else :
            domain = {'domain': {'detail_ids': [('id', '=', 0)]}}
        return domain

    @api.multi
    def action_confirm(self):
        if not self.detail_ids:
            raise UserError(_('Tab details tidak boleh kosong'))
        #for move in self.detail_ids.filtered(lambda m:m.location_id.usage not in ('supplier','customer')) :
        for move in self.detail_ids:
            move.kanban_id = self.id
            if move.new_demand == 0.0 :
                move.new_demand = move.product_uom_qty
            if not self.kemasan_ids :
                self.env['vit.kemasan'].create({'kanban_id' : self.id, 'product_id' : move.product_id.id})
            elif self.kemasan_ids.filtered(lambda p:p.product_id.id != move.product_id.id) :
                self.env['vit.kemasan'].create({'kanban_id' : self.id, 'product_id' : move.product_id.id})
        self.state = 'confirm' 
        self.action_assign()

    @api.multi
    def cek_quant(self, product_id, location_id) :
        quants = self.env['stock.quant'].sudo().search([('product_id','=',product_id.id),
                                                        ('location_id','=',location_id.id),
                                                        ('reserved_quantity','>',0.0)], order='in_date desc',limit=1)
        return quants

    @api.multi
    def action_cancel(self):
        #import pdb;pdb.set_trace()
        self.kemasan_ids.unlink()
        self.do_unreserve()       
        #self.detail_ids._action_cancel()
        for x in self.detail_ids.filtered(lambda i:i.state != 'done') :
            x._do_unreserve()
            quant = self.cek_quant(x.product_id,x.location_id)
            if quant :
                if x.new_demand > 0.0 :
                    qty = x.new_demand
                else:
                    qty = x.product_uom_qty
                quant.write({'reserved_quantity': quant.reserved_quantity-qty})
        # self.detail_ids.action_set_to_draft()
        # self.detail_ids._action_confirm()
        self.state = 'cancel'
        for move in self.detail_ids.filtered(lambda i:i.state != 'done')  :
            move.kanban_id = False
            move.state = 'confirmed'
        

    @api.multi
    def action_reset_to_draft(self):
        for x in self.detail_ids :
            x.kanban_id = False
        self.state = 'draft'

    @api.multi
    def re_update_bc_data(self):
        self.update_bc_data()
        return {
                'warning': {
                    'title': _('Update'),
                    'message': _("Data Bea Cukai sukses diperbaharui..")
                }
            }

    @api.multi
    def update_bc_data(self):
        picking_ids = self.detail_ids.mapped('picking_id')
        pick_ids = []
        for pick in picking_ids :
            if pick.id not in pick_ids :
                pick_ids.append(pick.id)
                pick.nomor_aju_id = self.nomor_aju_id.id
                pick.tgl_ttd = self.tgl_ttd
                pick.tgl_dokumen = self.tgl_dokumen
                pick.nama_pengangkut = self.nama_pengangkut
                pick.no_polisi = self.no_polisi
                pick.no_sj_pengirim = self.no_sj_pengirim
                pick.kode_cara_angkut = self.kode_cara_angkut
                #pick.master_pendukung_id = self.master_pendukung_id.id
                pick.kode_negara_pemasok_id = self.kode_negara_pemasok_id.id
                pick.pelabuhan_bongkar_id = self.pelabuhan_bongkar_id.id
                pick.pelabuhan_transit_id = self.pelabuhan_transit_id.id
                pick.pelabuhan_muat_id = self.pelabuhan_muat_id.id
                pick.freight = self.freight
                pick.ndpbm_id = self.ndpbm_id.id
                pick.tempat_penimbun_id = self.tempat_penimbun_id.id
                pick.kode_harga_id = self.kode_harga_id.id
                pick.jenis_angkut_id = self.jenis_angkut_id.id
                pick.bc_type = self.bc_type
                if self.kemasan_ids and not pick.kemasan_ids:
                    kemasan = self.env['vit.kemasan']
                    for km in self.kemasan_ids :
                        kemasan.create({'picking_id': pick.id,
                                        'product_id': km.product_id.id,
                                        'package_type_id': km.package_type_id.id,
                                        'qty' : km.qty,
                                        'seri' : km.seri})
                if self.dokumen_ids and not pick.dokumen_ids:
                    dokumen = self.env['vit.dokumen']
                    for doku in self.dokumen_ids :
                        dokumen.create({'picking_id': pick.id,
                                        'seri': doku.seri,
                                        'dokumen_master_id': doku.dokumen_master_id.id,
                                        'nomor': doku.nomor,
                                        'tanggal': doku.tanggal})
                if self.kontainer_ids and not pick.kontainer_ids:
                    kontainer = self.env['vit.kontainer']
                    for kon in self.kontainer_ids :
                        kontainer.create({'picking_id': pick.id,
                                            'name': kon.name,
                                            'seri_kontainer': kon.seri_kontainer,
                                            'kesesuaian_dokumen': kon.kesesuaian_dokumen,
                                            'keterangan': kon.keterangan,
                                            'kode_stuffing': kon.kode_stuffing,
                                            'kode_tipe_kontainer': kon.kode_tipe_kontainer,
                                            'kode_ukuran_kontainer': kon.kode_ukuran_kontainer,
                                            'flag_gate_in': kon.flag_gate_in,
                                            'flag_gate_out': kon.flag_gate_out,
                                            'nomor_polisi': kon.nomor_polisi,
                                            'nomor_segel': kon.nomor_segel,
                                            'waktu_gate_in': kon.waktu_gate_in,
                                            'waktu_gate_out': kon.waktu_gate_out})
                if not pick.no_pabean :
                    pick.no_pabean = self.no_pabean
                if not pick.tgl_pabean :
                    pick.tgl_pabean = self.tgl_pabean
                if not pick.status_aju :
                    pick.status_aju = self.status_aju

        return True

    def create_aju_id(self):
        origin = self.detail_ids[0].origin
        date_now = str(datetime.now().date())
        dt = datetime.now().date()
        dt_y = dt.year
        dt_m = '{:02d}'.format(dt.month)
        dt_d = '{:02d}'.format(dt.day)
        date = str(dt_y) + str(dt_m) + str(dt_d)
        po = self.env['purchase.order']
        so = self.env['sale.order']
        if self.picking_code == 'incoming' :
            # search PO
            po_exist = po.sudo().search([('name','=',origin)],limit=1)
            if po_exist :
                obj = False
                bc_type = False
                if po_exist.purchase_type == 'Local' :
                    obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.0')],limit=1)
                    bc_type = 'BC 4.0'
                elif po_exist.purchase_type == 'Import':
                    obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.3')],limit=1)
                    bc_type = 'BC 2.3'
                elif po_exist.purchase_type == 'Kawasan Berikat':
                    obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                    bc_type = 'BC 2.7'
                elif po_exist.purchase_type == 'Subcon':
                    obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2')],limit=1)
                    bc_type = 'BC 2.6.2'
                if not obj :
                    raise ValidationError(_('Purchase type %s belum di set atau master aju %s belum dibuat !') % (po_exist.name,po_exist.purchase_type))
                if not bc_type :
                    raise UserError(_('Transaksi selain lokal (BC 4.0), berikat (BC 2.7), import (BC 2.3), subcon (BC 2.6.2) belum bisa diproses!'))
                if obj.kode_dokumen_bc:
                    kppbc = str(obj.kode_dokumen_bc)
                else :
                    kppbc = str(obj.kppbc)
                id_modul = str(obj.id_modul)
            else :
                # search SO, jika retur PO
                so_exist = so.sudo().search([('name','=',origin)],limit=1)
                bc_type = False
                if so_exist.sale_type == 'Local':
                    # arahkan ke 40 karena SO retur (barang masuk)
                    bc_type = 'BC 4.0'
                elif so_exist.sale_type == 'Kawasan Berikat':
                    bc_type = 'BC 2.7'
                elif so_exist.sale_type == 'Export':
                    bc_type = 'BC 2.0'
                elif so_exist.sale_type == 'Subcon':
                    bc_type = 'BC 2.6.1'
                if not bc_type :
                    raise UserError(_('No dokumen retur %s tidak ditemukan di SO maupun PO')%(origin))
                obj = self.env['vit.export.import.bc'].search([('name','=',bc_type)],limit=1)
                        #raise ValidationError(_('Purchase type %s belum di set atau master aju %s belum dibuat !') % (po_exist.name,po_exist.purchase_type))
                #if not bc_type :
                    #raise UserError(_('Transaksi selain lokal (BC 4.0), berikat (BC 2.7), import (BC 2.3), subcon (BC 2.6.2) belum bisa diproses!'))
                if obj.kode_dokumen_bc:
                    kppbc = str(obj.kode_dokumen_bc)
                else :
                    kppbc = str(obj.kppbc)
                id_modul = str(obj.id_modul)
                #raise ValidationError(_('Purchase Order %s not found !') % rest.name)
        else :
            # search SO
            so_exist = so.sudo().search([('name','=',origin)],limit=1)
            bc_type = False
            if so_exist.sale_type == 'Local':
                bc_type = 'BC 4.1'
            elif so_exist.sale_type == 'Kawasan Berikat':
                bc_type = 'BC 2.7'
            elif so_exist.sale_type == 'Export':
                bc_type = 'BC 2.0'
            elif so_exist.sale_type == 'Subcon':
                bc_type = 'BC 2.6.1'
            if not bc_type :
                # jika tidak ada SO cari PO nya, biasanya retur
                po_exist = po.sudo().search([('name','=',origin)],limit=1)
                if po_exist :
                    obj = False
                    bc_type = False
                    if po_exist.purchase_type == 'Local' :
                        # arahkan ke 41 lagi karena PO retur (barang keluar)
                        obj = self.env['vit.export.import.bc'].search([('name','=','BC 4.1')],limit=1)
                        bc_type = 'BC 4.1'
                    elif po_exist.purchase_type == 'Import':
                        obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.3')],limit=1)
                        bc_type = 'BC 2.3'
                    elif po_exist.purchase_type == 'Kawasan Berikat':
                        obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.7')],limit=1)
                        bc_type = 'BC 2.7'
                    elif po_exist.purchase_type == 'Subcon':
                        obj = self.env['vit.export.import.bc'].search([('name','=','BC 2.6.2')],limit=1)
                        bc_type = 'BC 2.6.2'
                    if not obj :
                        raise ValidationError(_('Purchase type %s belum di set atau master aju %s belum dibuat !') % (po_exist.name,po_exist.purchase_type))
                    if not bc_type :
                        raise UserError(_('Transaksi selain lokal (BC 4.0), berikat (BC 2.7), import (BC 2.3), subcon (BC 2.6.2) belum bisa diproses!'))
                else :
                    #raise UserError(_('Transaksi selain lokal (BC 4.1), berikat (BC 2.7), export (BC 2.0), subcon (BC 2.6.1) belum bisa diproses!'))
                    raise UserError(_('No dokumen %s tidak ditemukan di PO maupun SO')%(origin))
            aju_obj = self.env['vit.export.import.bc'].search([('name','=',bc_type)],limit=1)
            if aju_obj.kode_dokumen_bc:
                kppbc = str(aju_obj.kode_dokumen_bc)
            else :
                kppbc = str(aju_obj.kppbc)
            id_modul = str(aju_obj.id_modul)
            acc_id = False
            journal = self.env['account.journal'].sudo().search([('company_id','=',self.company_id.id),('type','=','sale')], limit=1)
            if self.partner_id.property_account_receivable_id:
                acc_id = self.partner_id.property_account_receivable_id.id
            if not acc_id:
                raise ValidationError(_('account pada customer ( %s ) belum di set!!') % self.partner_id.name)
        if not self.nomor_aju_id:
            obj_aju = self.env['vit.nomor.aju'].search([('company_id','=',self.company_id.id),('name','=',bc_type)],order="name desc", limit=1)
            count = len(obj_aju.picking_ids)
            if count < 5:
                types = []
                partners = []
                for picking in obj_aju.picking_ids:
                    if picking.partner_id and picking.partner_id.id != picking.partner_id.id :
                        partners.append(z.partner_id.id)
                    if picking.picking_type_id.code not in types:
                        types.append(picking.picking_type_id.code)
                    # jika picking type sama dan partner nya sama semua maka bisa pake nomor aju ini
                    if len(types) == 1 and len(partners) < 1:
                        self.nomor_aju_id = obj_aju.id
            if not self.nomor_aju_id :
                inc_aju = str(self.env['ir.sequence'].next_by_code('increment_no_aju'))
                id_aju = self.env['vit.nomor.aju'].create({'name': kppbc + '0' + id_modul + date + inc_aju})
                self.nomor_aju_id = id_aju
        if not self.tgl_ttd :
            self.tgl_ttd = date_now
        if not self.tgl_dokumen :
            self.tgl_dokumen = date_now
        self.update_bc_data()

        return True

    @api.multi
    def action_validate(self):
        self.ensure_one()
        if self.detail_ids.filtered(lambda x:x.reserved_availability == 0.0 ):
            raise ValidationError(_('Reserved qty ada yang masih nol !'))
        if self.detail_ids.filtered(lambda x:x.state != 'assigned'):
            raise ValidationError(_('Semua data harus available !'))
        jml_picking = self.detail_ids.mapped('picking_id')
        for pic in jml_picking :
            jml_kanban = pic.move_ids_without_package.mapped('kanban_id.name')
            if len(jml_kanban) > 1 :
                raise ValidationError(_('SO/PO [%s] ada di lebih dari satu kanban %s') % (pic.origin, str(jml_kanban)))
        if self.picking_code == 'incoming':
            me = self.action_validate_incoming()
        else :
            avails = self.detail_ids#.filtered(lambda x:x.state == 'assigned' )
            for i in avails :
                i.kanban_id = self.id
                if i.state == 'assigned':
                    if i.move_line_nosuggest_ids :
                        if i.move_line_nosuggest_ids.filtered(lambda x: not x.lot_id ):
                            raise ValidationError(_('Lot harus diisi !'))
                    else :
                        if i.group_id and i.move_line_ids.filtered(lambda x: not x.package_id or not x.lot_id ):
                            raise ValidationError(_('Lot / package harus diisi !'))
                        elif not i.group_id and i.move_line_ids.filtered(lambda x: not x.lot_id ):
                            raise ValidationError(_('Lot harus diisi !'))
            me = self.action_validate_outgoing()
        for det in self.detail_ids :
            det.new_demand = det.quantity_done
        return me

    @api.multi
    def action_validate_outgoing(self):
        # pdb.set_trace()
        for rest in self:
            qty_done = rest.detail_ids.filtered(lambda x:x.quantity_done == 0.0)
            if qty_done :
                raise ValidationError(_('Qty done product %s masih bernilai nol !')%(qty_done[0].product_id.name))
            journal = self.env['account.journal'].sudo().search([('company_id','=',rest.company_id.id),('type','=','sale')], limit=1)
            if rest.partner_id.property_account_receivable_id:
                acc_id = rest.partner_id.property_account_receivable_id.id
            else:
                raise ValidationError(_('account pada customer ( %s ) belum di set!!') % rest.partner_id.name)
            self.create_aju_id()
            ref = []
            move_lines = []
            for x in rest.detail_ids:
                acc_line_id = False
                tax = []
                sale = []
                list_price = 0
                if x.product_id.categ_id.property_account_income_categ_id:
                    acc_line_id = x.product_id.categ_id.property_account_income_categ_id.id
                if not acc_line_id:
                    raise ValidationError(_('account receivable pada product ( %s ) belum di set!!') % x.product_id.name)
                obj_so = self.env['sale.order'].search([('name','=',x.origin)],limit=1)
                if not obj_so:
                    me = rest.action_validate_incoming()
                    if me :
                        return True
                for hg in obj_so.order_line.filtered(lambda hg: hg.product_id.id == x.product_id.id):
                    list_price = hg.price_unit
                    sale = [(4, hg.id)]
                    if hg.tax_id :
                        if len(hg.tax_id) == 1 :
                            taxe = hg.tax_id.id
                        else :
                            taxe = hg.tax_id.ids
                        tax = [(4,taxe)]
                # if not x.invoiced :
                #     move_lines.append((0,0,{
                #             'product_id'        : x.product_id.id,
                #             'price_unit'        : list_price,
                #             'quantity'          : x.quantity_done,
                #             'name'              : x.product_id.name,
                #             'account_id'        : acc_line_id,
                #             'invoice_line_tax_ids': tax,
                #             'sale_line_ids': sale
                #             }))
                #     x.invoiced = True
                move_lines.append((0,0,{
                        'product_id'        : x.product_id.id,
                        'price_unit'        : list_price,
                        'quantity'          : x.quantity_done,
                        'name'              : x.product_id.name,
                        'account_id'        : acc_line_id,
                        'invoice_line_tax_ids': tax,
                        'sale_line_ids': sale
                        }))
                if x.origin not in ref:
                    ref.append(x.origin)
                # if x.picking_id :
                #     x.picking_id.action_done()
                # else :
                # x._action_done()
            rest.detail_ids.mapped('picking_id').action_done()
            ref_char = ""
            ref_char = ', '.join(ref)

            type_ln = 'out_invoice'
            if rest.picking_code == 'incoming' :
                # refund
                type_ln = 'out_refund'
            
            data = {
                    'partner_id': rest.partner_id.id,
                    'journal_id': journal.id,
                    'account_id': acc_id,
                    'invoice_line_ids': move_lines,
                    'type': type_ln,
                    'origin': rest.name,
                    'user_id': rest.env.user.id,
                    'company_id': rest.company_id.id,
                    'reference': ref_char,
                    'currency_id': obj_so.currency_id.id
                    }
            inv = self.env['account.invoice'].sudo().create(data)
            inv.action_invoice_open()
            for z in rest.detail_ids:
                if z.picking_id :
                    z.picking_id.invoice_id = inv.id
                elif z.kanban_id and not z.picking_id :
                    z.kanban_id.invoice_id = inv.id
            rest.invoice_id = inv.id
            rest.state = 'done'
        return True

    @api.multi
    def action_validate_incoming(self):
        for rest in self:
            qty_done = rest.detail_ids.filtered(lambda x:x.quantity_done == 0.0)
            if qty_done :
                raise ValidationError(_('Qty done product %s masih bernilai nol !')%(qty_done[0].product_id.name))
            journal = self.env['account.journal'].sudo().search([('company_id','=',rest.company_id.id),('type','=','purchase')], limit=1)
            if rest.partner_id.property_account_payable_id:
                acc_id = rest.partner_id.property_account_payable_id.id
            else:
                raise ValidationError(_('account payable pada vendor ( %s ) belum di set!!') % rest.partner_id.name)
            self.create_aju_id()
            ref = []
            move_lines = []
            for x in rest.detail_ids:
                acc_line_id = False
                tax = []

                list_price = 0
                if x.product_id.categ_id.property_account_expense_categ_id:
                    acc_line_id = x.product_id.categ_id.property_account_expense_categ_id.id
                if not acc_line_id:
                    raise ValidationError(_('account expense pada product ( %s ) belum di set!!') % x.product_id.name)
                purchase = self.env['purchase.order'].search([('name','=',x.origin)],limit=1)
                if not purchase :
                    me = rest.action_validate_outgoing()
                    if me :
                        return True
                for hg in purchase.order_line.filtered(lambda hg: hg.product_id.id == x.product_id.id):
                    list_price = hg.price_unit
                    if hg.taxes_id:
                        if len(hg.taxes_id) == 1 :
                            taxe = hg.taxes_id.id
                        else :
                            taxe = hg.taxes_id.ids
                        tax = [(4,taxe)]
                    move_lines.append((0,0,{
                            'product_id'        : x.product_id.id,
                            'price_unit'        : list_price,
                            'quantity'          : x.quantity_done,
                            'name'              : x.product_id.name,
                            'account_id'        : acc_line_id,
                            'invoice_line_tax_ids': tax,
                            'purchase_line_id': hg.id
                            }))
                if x.origin not in ref:
                    ref.append(x.origin)
                # if x.picking_id :
                #     x.picking_id.action_done()
                # else :
                    #x._action_done()
            rest.detail_ids.mapped('picking_id').action_done()
            ref_char = ""
            ref_char = ', '.join(ref)
            kanban_name = rest.name
            if rest.reference :
                kanban_name = rest.name+' : '+rest.reference
            type_ln = 'in_invoice'
            if rest.picking_code == 'outgoing' :
                type_ln = 'in_refund'
            data = {
                    'partner_id': rest.partner_id.id,
                    'journal_id': journal.id,
                    'account_id': acc_id,
                    'invoice_line_ids': move_lines,
                    'type': type_ln,
                    'origin': kanban_name,
                    'user_id': rest.env.user.id,
                    'company_id': rest.company_id.id,
                    'name': ref_char,
                    'currency_id': purchase.currency_id.id
                    }
           # import pdb;pdb.set_trace()
            inv = self.env['account.invoice'].sudo().create(data)
            #import pdb;pdb.set_trace()
            #inv.action_invoice_open()
            for z in rest.detail_ids:
                if z.picking_id :
                    z.picking_id.invoice_id = inv.id
                elif z.kanban_id and not z.picking_id :
                    z.kanban_id.invoice_id = inv.id
            rest.invoice_id = inv.id
            rest.state = 'done'
        return True


VitKanban()


class VitKontainer(models.Model):
    _inherit = "vit.kontainer"

    kanban_id = fields.Many2one('vit.kanban', string='Kanban')

VitKontainer()


class VitKemasan(models.Model):
    _inherit = "vit.kemasan"

    kanban_id = fields.Many2one('vit.kanban', string='Kanban')

VitKemasan()


class VitDokumen(models.Model):
    _inherit = "vit.dokumen"

    kanban_id = fields.Many2one('vit.kanban', string='Kanban')

    @api.multi
    @api.onchange('dokumen_master_id')
    def onchange_dokumen_master_id(self):
        res = super(VitDokumen, self).onchange_dokumen_master_id()
        #import pdb;pdb.set_trace()
        picking = False
        if self.dokumen_master_id and self.dokumen_master_id.code == '380':
            if 'params' in self._context:
                if self._context['params'] and self._context['params']['model'] and self._context['params']['model'] == 'vit.kanban':
                    kanban_id = self._context['params']['id']
                    kanban = self.env['vit.kanban'].browse(kanban_id)  
            else : 
                kanban = self.kanban_id
            if kanban and kanban.invoice_id:    
                if kanban.picking_code == 'incoming':
                    ref = kanban.invoice_id.reference
                    if not ref :
                        ref = kanban.invoice_id.name
                    self.nomor = ref
                    if kanban.invoice_id.date_invoice :
                        self.tanggal = kanban.invoice_id.date_invoice
                elif kanban.picking_code == 'outgoing':
                    ref = kanban.invoice_id.number
                    if not ref :
                        ref = kanban.invoice_id.name
                    self.nomor = ref
                    if kanban.invoice_id.date_invoice :
                        self.tanggal = kanban.invoice_id.date_invoice
        return res

VitDokumen()

class VitKanbanPackage(models.Model):
    _name = "vit.kanban.package"
    _description    = 'Mapping Package Kanban'

    @api.onchange('product_id')
    def onchange_domain_product(self):
        domain = []
        for x in self :
            if x.kanban_id :
                ids = x.kanban_id.detail_ids.mapped('product_id')
                domain = {'domain': {'product_id': [('id', 'in', ids.ids)]}}
        return domain

    @api.onchange('package_id','quantity')
    def package_id_change(self):
        #import pdb;pdb.set_trace()
        # active_form = self._context.get('params', False)
        active_form = self._context.get('default_kanban_id', False)
        if active_form :
            # if 'id' in active_form :
                # kanban = self.env['vit.kanban'].search([('id','=',active_form['id'])])
            kanban = self.env['vit.kanban'].search([('id','=',active_form)])
            if kanban:
                products = kanban.detail_ids.mapped('product_id.id')
                if self.package_id.quant_ids :
                    quant = self.package_id.quant_ids.sorted('in_date')[0]
                    if quant.product_id.id in products :
                        self.product_id = quant.product_id.id
                        self.lot_id = quant.lot_id.id
                        # self.quantity = quant.quantity-quant.reserved_quantity
                        self.quantity = quant.quantity
                    else :
                        self.package_id = False
                        self.product_id = False
                        self.lot_id = False
                        self.quantity = 0.0
                        self.env.cr.commit()
                        raise ValidationError(_('Product package not found !'))
                elif self.package_id :
                    self.package_id = False
                    self.product_id = False
                    self.lot_id = False
                    self.quantity = 0.0
                    self.env.cr.commit()
                    raise ValidationError(_('Package content not found !'))


    kanban_id = fields.Many2one('vit.kanban', string='Kanban')
    package_id = fields.Many2one('stock.quant.package', string='Source Package')
    product_id = fields.Many2one('product.product', string='Product')
    lot_id = fields.Many2one('stock.production.lot', string='Lot / Serial Number')
    quantity = fields.Float('Quantity')
    move_id = fields.Many2one('stock.move', string='Move')
    product_customer = fields.Char('Code', compute='_compute_default_customer_code',store=True)

    @api.depends('product_id')
    def _compute_default_customer_code(self):
        for line in self:
            if line.product_id and line.product_id.product_customer_code_ids and line.kanban_id.partner_id :
                code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.kanban_id.partner_id.id)
                if code_exist :
                    line.product_customer = code_exist[0].product_code

VitKanbanPackage()


class VitKanbanPackageBig(models.Model):
    _name = "vit.kanban.package.big"
    _description    = 'Mapping Package Kanban Besar'

    @api.onchange('product_id','package_id')
    def onchange_domain_pack_besar(self):
        domain = []
        for x in self :
            if x.kanban_id :
                ids = x.kanban_id.detail_ids.mapped('product_id')
                domain = {'domain': {'package_id': [('package_ids', '!=', False)],'product_id': [('id', 'in', ids.ids)]}}
        return domain

    @api.onchange('package_id','quantity')
    def package_id_change(self):
        active_form = self._context.get('default_kanban_id', False)
        if active_form :
            kanban = self.env['vit.kanban'].search([('id','=',active_form)])
            if kanban:
                #import pdb;pdb.set_trace()
                if self.package_id.package_ids :
                    product = self.package_id.package_ids[0].sorted('id')[0]
                    if product and product.last_product_id :
                        self.product_id = product.last_product_id.id
                        #self.quantity = sum(product.package_ids.mapped('last_quantity'))
                    elif not product.last_product_id :
                        quant = product.quant_ids.sorted('in_date')[0]
                        self.product_id = quant.product_id.id
                        #self.quantity = sum(product.package_ids.mapped('last_quantity'))
                    #else :
                        # self.package_id = False
                        # self.product_id = False
                        # self.quantity = 0.0
                        # self.env.cr.commit()
                        # raise ValidationError(_('Package kecil tidak ditemukan !'))

    kanban_id = fields.Many2one('vit.kanban', string='Kanban')
    package_id = fields.Many2one('stock.quant.package', string='Source Package')
    package_ids = fields.One2many(related='package_id.package_ids', string="Childs")
    product_id = fields.Many2one('product.product', string='Product')
    quantity = fields.Float('Quantity')

VitKanbanPackageBig()