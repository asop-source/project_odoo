# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models

# class ProcurementGroup(models.Model):
#     _inherit = 'procurement.group'

#     @api.model
#     def run(self, product_id, product_qty, product_uom, location_id, name, origin, values):
#         import pdb;pdb.set_trace()
#         res = super(ProcurementGroup, self).run(product_id, product_qty, product_uom, location_id, name, origin, values)
#         if origin :
#             picking = self.env['stock.picking'].sudo().search([('origin','=',origin),('state','!=','cancel')])
#             if picking and len(picking) == 1 :
#                 picking.state = 'draft'
#                 picking.printed = True
#         return res

# ProcurementGroup()


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.multi
    def _action_launch_stock_rule(self):
        res = super(SaleOrderLine, self)._action_launch_stock_rule()
        orders = list(set(x.order_id for x in self))
        for order in orders:
            # reassign = order.picking_ids.filtered(lambda x: x.state=='confirmed' or (x.state in ['waiting', 'assigned'] and not x.printed))
            # if reassign:
            #     reassign.action_assign()
            unreserve = order.picking_ids.filtered(lambda x: x.state=='assigned' and x.picking_type_id.code == 'outgoing')
            if unreserve:
                unreserve.do_unreserve()
        return res

SaleOrderLine()