from odoo.addons import decimal_precision as dp
from psycopg2 import OperationalError, Error
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.osv import expression
from odoo.tools.float_utils import float_compare, float_is_zero

class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    group_id = fields.Many2one('procurement.group', related="move_id.group_id",string='Transaction', store=True)
    client_order_ref = fields.Char(related="move_id.sale_line_id.order_id.client_order_ref",string='PO Customer', store=True)
    kanban_id = fields.Many2one('vit.kanban', related="move_id.kanban_id",string='Kanban', store=True)
    kanban_line_id = fields.Many2one("vit.kanban.package","Kanban Line ID")

StockMoveLine()


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.multi
    @api.depends('move_line_ids.product_qty')
    def _compute_reserved_availability(self):
        """ Fill the `availability` field on a stock move, which is the actual reserved quantity
        and is represented by the aggregated `product_qty` on the linked move lines. If the move
        is force assigned, the value will be 0.
        """
        s_ids = self.ids
        if not s_ids :
            try:
                s_ids = self._origin.ids
            except:
                pass
        result = {data['move_id'][0]: data['product_qty'] for data in 
            self.env['stock.move.line'].read_group([('move_id', 'in', s_ids)], ['move_id','product_qty'], ['move_id'])}
        for rec in self:
            new_rec = rec.id
            if not new_rec :
                try :
                    new_rec = self._origin.id
                except :
                    pass
            quantity = rec.product_id.uom_id._compute_quantity(result.get(new_rec, 0.0), rec.product_uom, rounding_method='HALF-UP')
            if quantity == 0.0 and rec.state == 'assigned':
                # if rec.kanban_id.name[:3] == 'RTR' :
                #     quantity = rec.new_demand
                if rec.kanban_id :
                    quantity = rec.new_demand
                else :
                    quantity = rec.product_uom_qty
            rec.reserved_availability = quantity

    @api.multi
    def action_cancel_move_kanban(self):
        #import pdb;pdb.set_trace()
        self.ensure_one()
        self._do_unreserve()
        quant = self.env['vit.kanban'].cek_quant(self.product_id,self.location_id)
        if quant :
            if self.new_demand > 0.0 :
                qty = self.new_demand
            else:
                qty = self.product_uom_qty
            quant.write({'reserved_quantity': quant.reserved_quantity-qty})
        self.kanban_id = False
        self.state = 'confirmed'

    @api.multi
    def action_confirm_move_kanban(self):
        self.ensure_one()
        old_demand = self.product_uom_qty
        self.product_uom_qty = self.new_demand
        self._action_assign()
        mv_state = self.state
        sql = "update stock_move set product_uom_qty=%s,product_qty=%s, state='draft' where id = %s " % ( old_demand,old_demand,self.id) 
        self.env.cr.execute(sql)
        self.env.cr.commit()
        sql2 = "update stock_move set state='%s' where id = %s " % ( mv_state,self.id)
        self.env.cr.execute(sql2)
        self.env.cr.commit()
        # reset package and lot
        sql = "update stock_move_line set package_id=null, result_package_id=null, lot_id=null, qty_done=0.0 where move_id = %s " % ( self.id) 
        self.env.cr.execute(sql)

    @api.multi
    def action_set_to_draft(self):
        if self.filtered(lambda m: m.state != 'cancel'):
            raise UserError(_("You can set to draft cancelled moves only !"))
        self.write({'state': 'draft'})

    kanban_id = fields.Many2one('vit.kanban', string='Kanban', copy=False)
    new_demand = fields.Float('New Demand',digits=dp.get_precision('Product Unit of Measure'))
    client_order_ref = fields.Char(related="sale_line_id.order_id.client_order_ref",string='PO Customer', store=True)
    product_customer = fields.Char('Customer Product Code', compute='_compute_default_customer_code',store=True)

    @api.depends('product_id','picking_id','state')
    def _compute_default_customer_code(self):
        for line in self:
            if line.product_id and line.product_id.product_customer_code_ids and line.picking_id :
                code_exist = False 
                if line.kanban_id :
                    code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.kanban_id.partner_id.id)
                elif line.picking_id :
                    code_exist = line.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == line.picking_id.partner_id.id)
                
                if code_exist :
                    line.product_customer = code_exist[0].product_code

    # def _action_assign(self):
    #     cr = self.env.cr
    #     for move in self :
    #         if move.picking_code == 'outgoing' and move.new_demand > 0.0:
    #             old_demand = move.product_uom_qty
    #             move.product_uom_qty = move.new_demand
    #             res = super(StockMove, move)._action_assign()
    #             #move.product_uom_qty = old_demand
    #             cr.execute("update stock_move set product_uom_qty=%s where id = %s",( old_demand,move.id,))
    #         else :
    #             res = super(StockMove, move)._action_assign()
    #     return

StockMove()


class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.depends('write_date','state','move_ids_without_package.kanban_id')
    def _search_kanban(self):
        for pick in self:
            kanban_id = pick.move_ids_without_package.filtered(lambda p:p.kanban_id)
            if kanban_id :
            	pick.kanban_id = kanban_id[0].kanban_id.id
            else :
            	pick.kanban_id = False

    kanban_id = fields.Many2one('vit.kanban', string='Kanban', compute="_search_kanban",store=True, copy=False)
    asal_bb = fields.Char('Asal Bahan Baku', compute='_compute_asal_bahan_baku')


    def _compute_asal_bahan_baku(self):
        for picking in self:
            if picking.origin :
                mrp_id = self.env['mrp.production'].sudo().search([('name','=',picking.origin)],limit=1)
                if mrp_id :
                    wire = mrp_id.move_raw_ids.filtered(lambda x:x.product_tmpl_id.categ_id.name == 'WIRE' or x.product_tmpl_id.categ_id.name == 'Wire')
                    if wire :
                        lot_id = wire[0].move_line_ids.filtered(lambda l:l.lot_id)
                        if lot_id and lot_id[0].purchase_order_ids:
                            vendor_id = lot_id[0].purchase_order_ids[0].partner_id
                            if vendor and vendor.transaction_type :
                                picking.asal_bb = vendor.transaction_type 


StockPicking()


class StockQuantPackage(models.Model):
    _inherit = "stock.quant.package"

    @api.multi
    @api.depends('name', 'last_quantity')
    def name_get(self):
        result = []
        #import pdb;pdb.set_trace()
        for res in self:
            name = res.name
            if res.last_quantity > 0.0 :
                qty_awal = int(res.last_quantity)
                qty = '{:,}'.format(qty_awal)
                name = name + ' --> '+str(qty)
            result.append((res.id, name))
        return result

    last_partner_id = fields.Many2one('res.partner','Last Partner', compute='_compute_default_customer_code',store=True)
    last_product_id = fields.Many2one('product.product','Last Product', compute='_compute_default_customer_code',store=True)
    last_lot_id = fields.Many2one('stock.production.lot','Last Lot', compute='_compute_default_customer_code',store=True)
    last_quantity = fields.Float('Last Quantity',digits=(9,0), compute='_compute_default_customer_code',store=True)
    product_customer = fields.Char('Customer Code', compute='_compute_default_customer_code',store=True)
    transaction_type = fields.Char('Sumber Bahan Baku', compute='_compute_source_rawmate',store=True)


    
    package_ids = fields.One2many('stock.quant.package', 'package_baru_id', string='Child Package',)
    package_baru_id = fields.Many2one(string='Package Baru', comodel_name='stock.quant.package', store=True)
    active = fields.Boolean('Active', default=True)
    jenis_package = fields.Selection([
        ('besar', 'Package Besar'),
        ('kecil','Package Kecil')],
        compute ='_onchange_package', store=True)

    keterangan = fields.Text(string='Keterangan')

    @api.depends('location_id','quant_ids','name','write_date')
    def _compute_default_customer_code(self):
        for line in self:
            if line.quant_ids :
                last_quant = line.quant_ids.sorted('in_date')[0]
                line.last_quantity = last_quant.quantity
            domain = ['|', ('result_package_id', 'in', line.ids), ('package_id', 'in', line.ids),('move_id.picking_id.partner_id','!=',False)]
            last_move = self.env['stock.move.line'].search(domain,order='date desc',limit=1)
            if last_move :
                line.last_partner_id = last_move.picking_id.partner_id.id
            if last_move and last_move.product_id and last_move.product_id.product_customer_code_ids and last_move.picking_id.partner_id :
                if last_move.product_id:
                    line.last_product_id = last_move.product_id.id
                code_exist = last_move.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == last_move.picking_id.partner_id.id)
                if code_exist :
                    line.last_partner_id = last_move.picking_id.partner_id.id
                    line.product_customer = code_exist[0].product_code
                    line.last_lot_id = last_move.lot_id.id or False

    @api.depends('name')
    def transaction_type(self):
        for line in self:
            if line.quant_ids :
                old_quant = line.quant_ids.sorted('in_date',reverse=True)[0]
                if old_quant.lot_id :
                    # search MO
                    mo_exist = self.env['mrp.production'].sudo().search([('name','ilike', old_quant.lot_id.name)])
            domain = ['|', ('result_package_id', 'in', line.ids), ('package_id', 'in', line.ids)]
            last_move = self.env['stock.move.line'].search(domain,order='date desc',limit=1)
            if last_move and last_move.product_id and last_move.product_id.product_customer_code_ids and last_move.picking_id.partner_id :
                if last_move.product_id:
                    line.last_product_id = last_move.product_id.id
                code_exist = last_move.product_id.product_customer_code_ids.filtered(lambda code:code.partner_id.id == last_move.picking_id.partner_id.id)
                if code_exist :
                    line.last_partner_id = last_move.picking_id.partner_id.id
                    line.product_customer = code_exist[0].product_code
                    line.last_lot_id = last_move.lot_id.id or False

StockQuantPackage()


class StockQuant(models.Model):
    _inherit = "stock.quant"

    @api.model
    def _update_reserved_quantity(self, product_id, location_id, quantity, lot_id=None, package_id=None, owner_id=None, strict=False):
        """ Increase the reserved quantity, i.e. increase `reserved_quantity` for the set of quants
        sharing the combination of `product_id, location_id` if `strict` is set to False or sharing
        the *exact same characteristics* otherwise. Typically, this method is called when reserving
        a move or updating a reserved move line. When reserving a chained move, the strict flag
        should be enabled (to reserve exactly what was brought). When the move is MTS,it could take
        anything from the stock, so we disable the flag. When editing a move line, we naturally
        enable the flag, to reflect the reservation according to the edition.

        :return: a list of tuples (quant, quantity_reserved) showing on which quant the reservation
            was done and how much the system was able to reserve on it
        """
        self = self.sudo()
        rounding = product_id.uom_id.rounding
        quants = self._gather(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
        reserved_quants = []
        
        if float_compare(quantity, 0, precision_rounding=rounding) > 0:
            # if we want to reserve
            available_quantity = self._get_available_quantity(product_id, location_id, lot_id=lot_id, package_id=package_id, owner_id=owner_id, strict=strict)
            # if float_compare(quantity, available_quantity, precision_rounding=rounding) > 0:
            #     raise UserError(_('It is not possible to reserve more products of %s than you have in stock.') % product_id.display_name)
        elif float_compare(quantity, 0, precision_rounding=rounding) < 0:
            # if we want to unreserve
            available_quantity = sum(quants.mapped('reserved_quantity'))
            # if float_compare(abs(quantity), available_quantity, precision_rounding=rounding) > 0:               
            #     raise UserError(_('It is not possible to unreserve more products of %s than you have in stock.') % product_id.display_name)
        else:
            return reserved_quants
        #import pdb;pdb.set_trace()
        for quant in quants:
            if float_compare(quantity, 0, precision_rounding=rounding) > 0:
                max_quantity_on_quant = quant.quantity - quant.reserved_quantity
                if float_compare(max_quantity_on_quant, 0, precision_rounding=rounding) <= 0:
                    continue
                max_quantity_on_quant = min(max_quantity_on_quant, quantity)
                quant.reserved_quantity += max_quantity_on_quant
                reserved_quants.append((quant, max_quantity_on_quant))
                quantity -= max_quantity_on_quant
                available_quantity -= max_quantity_on_quant
            else:
                max_quantity_on_quant = min(quant.reserved_quantity, abs(quantity))
                quant.reserved_quantity -= max_quantity_on_quant
                reserved_quants.append((quant, -max_quantity_on_quant))
                quantity += max_quantity_on_quant
                available_quantity += max_quantity_on_quant

            if float_is_zero(quantity, precision_rounding=rounding) or float_is_zero(available_quantity, precision_rounding=rounding):
                break
        return reserved_quants

StockQuant()