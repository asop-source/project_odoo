from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from datetime import date, datetime, time

class VitKanbanWizard(models.TransientModel):
    _name = "vit.kanban.wizard"

    @api.model
    def default_get(self, fields):
        res = super(VitKanbanWizard, self).default_get(fields)
        if 'kanban_id' in fields and not res.get('kanban_id') and self._context.get('active_model') == 'vit.kanban' and self._context.get('active_id'):
            res['kanban_id'] = self._context['active_id']
        if 'detail_return_ids' in fields and not res.get('detail_return_ids') and res.get('kanban_id'):
            kanban = self.env['vit.kanban'].browse(res.get('kanban_id'))
            detail_return_ids = []
            for kbn in kanban.detail_ids.filtered(lambda x:x.state=='done') :
                detail_return_ids.append((0, 0, {'product_id': kbn.product_id.id ,
                                                'quantity':kbn.quantity_done,
                                                'reference' : kbn.reference,
                                                'uom_id' : kbn.product_uom.id}))
            res['detail_return_ids'] = detail_return_ids
        return res

            
    kanban_id = fields.Many2one('vit.kanban', string="Parent")
    detail_return_ids = fields.One2many('vit.kanban.wizard.details','request_id','Kanban Return')

    @api.multi
    def create_return(self):
        self.ensure_one()
        move_obj = self.env['stock.move']
        kanban_obj = self.env['vit.kanban']
        date = str(datetime.now().date())
        for wizard in self :
            kanban = wizard.kanban_id
            picking_code = kanban.picking_code
            if picking_code == 'incoming' :
                new_picking_code = 'outgoing'
            else :
                new_picking_code = 'incoming'
            
            return_kanban = kanban_obj.create({'picking_code' : new_picking_code,
                                                'partner_id' : kanban.partner_id.id,
                                                'company_id' : kanban.company_id.id,
                                                'kanban_id' : kanban.id,
                                                # 'date' : date,
                                                # 'date_start' : date,
                                                # 'date_end' : date,
                                                'reference' : kanban.name
                                                })
            datas = []
            picking = False
            for det in wizard.detail_return_ids.filtered(lambda i:i.quantity > 0.0 ) :
                for mv in kanban.detail_ids.filtered(lambda m:m.product_id.id == det.product_id.id) :
                    if mv.location_dest_id.usage in ('transit','git') :
                        raise UserError(_('Lokasi destinasi transit tidak bisa di retur !'))
                    elif mv.location_id.usage in ('transit','git')  :
                        raise UserError(_('Lokasi asal transit tidak bisa di retur !'))
                    if not picking :
                        picking = self.env['stock.picking'].create({'picking_type_id': mv.picking_type_id.return_picking_type_id.id,
                                                                'date': date,
                                                                'partner_id' : kanban.partner_id.id,
                                                                'location_id':mv.location_dest_id.id,
                                                                'location_dest_id':mv.location_id.id,
                                                                'origin': kanban.name +' : '+ return_kanban.name})
                    move = move_obj.create({'kanban_id' : return_kanban.id,
                                            'name' : det.product_id.name,
                                            'product_id' : det.product_id.id,
                                            'product_uom_qty' : det.quantity,
                                            'new_demand' : det.quantity,
                                            'product_uom' : det.uom_id.id,
                                            'origin' : mv.origin,
                                            'price_unit' : mv.price_unit,
                                            'reference' : kanban.name,
                                            'state':'confirmed',
                                            'picking_type_id':mv.picking_type_id.return_picking_type_id.id,
                                            'location_id': mv.location_dest_id.id,
                                            'location_dest_id': mv.location_id.id,
                                            'company_id': kanban.company_id.id,
                                            'date_expected':date,
                                            'picking_id':picking.id,
                                            'partner_id' : kanban.partner_id.id,
                                            })
                    #move._action_confirm()
                    self.env.cr.commit()
                    datas.append(move.id)
                    break
            return_kanban.name = 'RTR/'+return_kanban.name
            return_kanban.write({'detail_ids' : [(6,0,datas)]})
            return {
            'name': _('Returned Transfer'),
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'vit.kanban',
            'res_id': return_kanban.id,
            'type': 'ir.actions.act_window',
            'context': dict(self.env.context),
        }

VitKanbanWizard()


class VitKanbanWizardDetails(models.TransientModel):
    _name = "vit.kanban.wizard.details"

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id :
            self.uom_id = self.product_id.uom_id.id

    request_id = fields.Many2one('vit.kanban.wizard','Request ID')
    product_id =  fields.Many2one('product.product', string="Product", required=True)
    quantity = fields.Float(string='Quantity')
    reference = fields.Char(string='Reference', required=True)
    uom_id = fields.Many2one('uom.uom','UoM',required=True)

VitKanbanWizardDetails()