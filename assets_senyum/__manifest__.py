# -*- coding: utf-8 -*-
{
    'name': "Node3 Asset PT. Senyum",

    'summary': """
        Node3 Assets""",

    'description': """
        Informasi pengguna aset
        Approval
        Sequence No. Asset
    """,

    'author': "PT. Trimitra Sistem Solusindo",
    'website': "http://trimitrasis.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Assets Management',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','om_account_asset','purchase','account','hr'],

    # always loaded
    'data': [
	'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/account_asset_asset.xml',
        'views/product.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}