


from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
import pdb
import logging
_logger = logging.getLogger(__name__)



class TemplateSaleOrder(models.Model):
    _inherit = 'sale.order.template.line'

    rab_n_1 = fields.Integer(string="RAB N -1")
    rab_n_2 = fields.Float(string="RAB Real")
    rab_n_3 = fields.Many2one(comodel_name="account.analytic.tag", string="Account Analytic Tag")


