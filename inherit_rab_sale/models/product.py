
from odoo import api, fields, models, _



class TemplateSaleOrder(models.Model):
	_inherit = 'product.product'


	rab_n_1 = fields.Float(string="RAB N -1")
	rab_n_2 = fields.Many2one(comodel_name="account.analytic.account",
							string="Analytic Account")

	# rab_id = fields.Many2one(comodel_name="sale.order.line", string="Sale Order line")