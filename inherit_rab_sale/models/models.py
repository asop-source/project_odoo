# -*- coding: utf-8 -*-

from odoo import models, fields, api
import time
import pdb
import logging
_logger = logging.getLogger(__name__)
from odoo.addons import decimal_precision as dp


class SalesOrderForm(models.Model):
	_inherit = 'sale.order'


	@api.depends('order_line.rab_n_4')
	def _rab4_total(self):
		for rec in self:
			for line in rec.order_line:
				if line.rab_n_4 > 0:
					rec.rab_n_3 += line.rab_n_4
				else:
					rec.rab_n_3 = 0.0
				
	# @api.multi
	# def action_generate_report(self):
	# 	res = self.env['sale.order.line'].search([('rab_n_6.name','=','RAB-JAN-20'),('product_id','=','price_unit')])
	# 	for line in res:
	# 		print(line)
	# 		print('OOOOOOOOOOOOOO')
	# 		if line.rab_n_6.name:
	# 			print(line.rab_n_6.name)
	# 			print('AAAAAAAAAAAAAA')
	# 			line.rab_n_4 = line.price_unit
	# 			line.rab_n_4
	# 		else :
	# 			line.rab_n_4 = 0.0
		

	rab_n_1 = fields.Many2one(comodel_name="account.analytic.tag", string="RAB N -1")
	rab_n_2 = fields.Many2one(comodel_name="account.analytic.tag", string="RAB N +1")
	rab_n_3 = fields.Float(string="Total RAB N-1", readonly=True, compute='_rab4_total')




class SalesOrderLine(models.Model):
	_inherit = 'sale.order.line'



	# @api.depends('rab_n_6','price_subtotal')
	# def _rab_nilai(self):
	# 	for rec in self:
	# 		if rec.rab_n_6:
	# 			rec.rab_n_4 = 1
	# 		else:
	# 			rec.rab_n_4 = rec.price_subtotal




	@api.depends('rab_n_4','rab_n_5')
	def _rab_saldo(self):
		for rec in self:
			if rec.rab_n_5 > 0:
				rec.rab_n_7 = rec.rab_n_4 - rec.rab_n_5
			else:
				rec.rab_n_7 = 0

	# @api.depends('product_id','rab_n_6')
	# def _jumlah_rab(self):
	# 	print('ZZZZZZZZZZZZZZZ')
	# 	for data in self:
	# 		data = self.env['sale.order.line'].search([('rab_n_6.name','=','RAB-JAN-20'),('product_id.id','=','rab_n_6.name')])
	# 		print('AAAAAAAAAAAAAAAAAAAAA')
	# 		print(data.price_unit)

		
	# 	for line in data:
	# 		print(line.price_unit)
	# 		print('ssssssssssssssssss')
	# 		if line.product_id.name == line.rab_n_6.name :
	# 			line.rab_n_4 = line.price_unit
	# 		else :
	# 			line.rab_n_4 = 0.0



	price_unit = fields.Float('Unit Price', required=True, digits=dp.get_precision('Product Price'), default=0.0)
	rab_n_4 = fields.Float(string="RAB N -1")
	rab_n_5 = fields.Integer(string="RAB Real")
	rab_n_6 = fields.Many2one(comodel_name="account.analytic.tag",
							string="Analytic Tags",
							related="order_id.rab_n_2", )
	rab_n_7 = fields.Integer(string="RAB Saldo", compute="_rab_saldo")