from odoo import fields, api, models, _
from odoo.addons import decimal_precision as dp

class VitKodeNegaraPemasok(models.Model):
    _name = "vit.kode.negara.pemasok"
    _description = "Kode Negara Pemasok"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitPelabuhanBongkar(models.Model):
    _name = "vit.pelabuhan.bongkar"
    _description = "Pelabuhan Bongkar"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)
    kantor_bongkar = fields.Char('Kantor Bongkar', required=True)

class VitPelabuhanTransit(models.Model):
    _name = "vit.pelabuhan.transit"
    _description = "Pelabuhan Transit"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitPelabuhanMuat(models.Model):
    _name = "vit.pelabuhan.muat"
    _description = "Pelabuhan Muat"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitTempatPenimbun(models.Model):
    _name = "vit.tempat.penimbun"
    _description = "Tempat Penimbun"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitKodeHarga(models.Model):
    _name = "vit.kode.harga"
    _description = "Kode Harga"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitJenisAngkut(models.Model):
    _name = "vit.jenis.angkut"
    _description = "Jenis Angkut"

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitKodeDokumen(models.Model):
    _name = "vit.kode.dokumen"
    _description = "Kode Dokumen"

    @api.multi
    def name_get(self):
        result = []
        for res in self:
            name = res.name
            if res.code :
                name = res.code+' - '+name
            result.append((res.id, name))
        return result

    code = fields.Char('Kode', required=True)
    name = fields.Char('Keterangan', required=True)

class VitKursPajak(models.Model):
    _name = "vit.kurs.pajak"
    _description = "Kurs Pajak / NDPBM"
    _rec_name = 'currency_id'

    currency_id = fields.Many2one('res.currency', required=True, string='Currency Code')
    description = fields.Char(related='currency_id.currency_unit_label', string='Description')
    rate_ids = fields.One2many('vit.kurs.pajak.rate', 'kurs_pajak_id', string='Rate Kurs Pajak')

    @api.model
    def create(self, vals):
        res = super(VitKursPajak, self).create(vals)
        if 'description' in vals and vals['description'] != res.currency_id.currency_unit_label :
            res.currency_id.write({'currency_unit_label':vals['description']})
        return res

    @api.multi
    def write(self, vals):
        res = super(VitKursPajak, self).write(vals)
        if 'description' in vals :
            for kurs in self :
                if vals['description'] != kurs.currency_id.currency_unit_label :
                    kurs.currency_id.write({'currency_unit_label':vals['description']})
        return res

    def get_rate(self, date):
        date = date[:10]
        rate = '0.00'
        rate_id = self.env['vit.kurs.pajak.rate'].search([
            ('kurs_pajak_id','=',self.id),
            ('date','<=',date),
        ], order='date desc', limit=1)
        if rate_id :
            rate = rate_id.rate
        return rate

class VitKursPajakRate(models.Model):
    _name = "vit.kurs.pajak.rate"
    _description = "Rate Kurs Pajak"
    _rec_name = 'date'
    _order = 'date'

    date = fields.Date('Date', required=True)
    rate = fields.Float(string='Exchange Rate', digits=dp.get_precision('Product Price'), required=True)
    kurs_pajak_id = fields.Many2one('vit.kurs.pajak', string='Kurs Pajak', ondelete='cascade')
