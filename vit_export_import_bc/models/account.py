from odoo import fields, api, models, _
from odoo.exceptions import Warning

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    seri_dokumen = fields.Char('Seri Dokumen')
    
class AccountTax(models.Model):
    _inherit = 'account.tax'

    kode_tarif = fields.Char('Kode Tarif')
    