from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp


class ResPartner(models.Model):
    _inherit = 'res.partner'

    transaction_type = fields.Selection([
        ('Local','Local'),
        ('Export','Export'),
        ('Import','Import'),
        ('Kawasan Berikat','Kawasan Berikat'),
        ('Subcon','Subcon'),
    ], string="Default Transaction Type")

ResPartner()