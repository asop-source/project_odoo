from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class ResCompany(models.Model):
    _inherit = 'res.company'

    bea_cukai = fields.Boolean('Bea Cukai')

ResCompany()