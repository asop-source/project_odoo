from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    sale_type = fields.Selection([
        ('Local','Local'),
        ('Export','Export'),
        ('Kawasan Berikat','Kawasan Berikat'),
        ('Subcon','Subcon'),
    ], string="Sale Type", related='partner_id.transaction_type',store=True)

    # @api.onchange('partner_id')
    # def onchange_partner(self):
    #     for x in self:
    #         if x.partner_id and x.partner_id.transaction_type and x.partner_id.transaction_type != 'Import':
    #             x.sale_type = x.partner_id.transaction_type

SaleOrder()


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    purchase_type = fields.Selection([
        ('Local','Local'),
        ('Import','Import'),
        ('Kawasan Berikat','Kawasan Berikat'),
        ('Subcon','Subcon'),
    ], string="Purchase Type", related='partner_id.transaction_type',store=True)

    @api.multi
    def _create_picking(self):
        res = super(PurchaseOrder, self)._create_picking()
        pickings = self.picking_ids.filtered(lambda pi:pi.state == 'assigned')
        for pick in pickings :
            if self.company_id.bea_cukai :
                pick.is_bea_cukai = True
            product_ids = pick.move_ids_without_package.mapped('product_id')
            for prod in product_ids :
                if not pick.kemasan_ids :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
                elif pick.kemasan_ids.filtered(lambda p:p.product_id.id != prod.id) :
                    self.env['vit.kemasan'].create({'picking_id' : pick.id, 'product_id' : prod.id})
        return res

    @api.onchange('partner_id')
    def onchange_partner(self):
        for x in self:
            if x.partner_id and x.partner_id.transaction_type and x.partner_id.transaction_type == 'Import':
                wh = self.env['stock.warehouse'].sudo().search([('name','ilike','Pelabuhan'),('company_id','=',x.company_id.id)],limit=1)
                if wh : 
                    x.picking_type_id = wh.in_type_id.id
            else :
                default_wh = self.env['stock.warehouse'].sudo().search([('name','=',x.company_id.name),('company_id','=',x.company_id.id)],limit=1)
                if default_wh :
                    x.picking_type_id = default_wh.in_type_id.id
PurchaseOrder()

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    seri = fields.Integer('Seri', size=3)
    
    