from odoo import fields, api, models, _
from odoo.exceptions import Warning

class ProductCategory(models.Model):
    _inherit = 'product.category'

    pos_tarif = fields.Char('POS Tarif')
    