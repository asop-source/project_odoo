{
    "name"          : "Export Import BC",
    "version"       : "1.1",
    "author"        : "vITraining",
    "website"       : "https://vitraining.com",
    "category"      : "Bea Cukai",
    "license"       : "LGPL-3",
    "contributors"  : """
        - Miftahussalam <https://blog.miftahussalam.com>
    """,
    "summary"       : "Data integration with Bea Cukai",
    "description"   : """

    """,
    "depends"       : [
        "base",
        "account",
        "stock",
        "sale",
        "purchase",
        "vit_nitto_product",
    ],
    "data"          : [
        "security/res_groups.xml",
        "views/vit_export_import_bc_view.xml",
        "views/vit_nomor_aju_view.xml",
        "views/stock_view.xml",
        "views/product_view.xml",
        "views/company_view.xml",
        "views/orders_view.xml",
        "views/account_view.xml",
        "views/partner_view.xml",
        "views/vit_master_pendukung_view.xml",
        "wizard/vit_export_import_bc_wizard.xml",
        "security/ir.model.access.csv",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
    "external_dependencies": {
        "python": [
            "xlsxwriter",
        ]
    }
}