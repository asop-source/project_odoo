# -*- coding: utf-8 -*-

from odoo import models, fields, api

class node3_notification_mail(models.Model):
	_inherit = 'product.template'



	@api.model
	def create(self, vals):
		self.add_followers(vals)
		new_id = super(node3_notification_mail,self).create(vals)
		new_id.message_subscribe([x.partner_id.id for x in new_id.message_follower_ids])

		body_write = "Product Baru Telah di Tambah Please Check"
		new_id.send_followers(body_write)
		return new_id

	def add_followers(self, vals):
		uid = self.env.uid
		group_ids = self.find_notife_user()
		partner_ids = []
		for group in group_ids:
			for user in group.users:
				if user.id != uid:
					partner_ids.append(user.partner_id.id)
				if partner_ids:
					vals ['message_follower_ids'] = [(0,0,{
						'res_model' : 'product.template',
						'partner_id' : pid
						})for pid in partner_ids]

	def find_notife_user(self):
		group_obj = self.env['res.groups']
		group_ids = group_obj.sudo().search([('name','=','Notification Product')])
		return group_ids

	def send_followers(self, body_write):
		followers = [x.partner_id.id for x in self.message_follower_ids]
		self.message_post(body_write=body_write, type="notification", subtype="mt_comment",partner_ids=followers,)
		return