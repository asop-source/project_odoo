# -*- coding: utf-8 -*-
{
    'name': "node3 notification mail",

    'summary': """
        Notification Message Mail on Change or Update Product
        """,


    'author': "asopkarawang@gmail.com",


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Mail',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','sale','point_of_sale','account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/group.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}