# -*- coding: utf-8 -*-
{
    'name': "XMLRPC DB-Sync",

    'summary': """
        Integrasi XMLRPC Odoo

        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Api Odoo',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/sync_fields.xml',
        'views/sync_models.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}