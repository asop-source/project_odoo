# -*- coding: utf-8 -*-

from odoo import models, fields, api

class sync_models(models.Model):
	_name = 'sync.models'
	_rec_name = 'name'

	name = fields.Char('Name Models')
	model_id = fields.Many2one('ir.model', 'Models to Synchronize', required=True)
	model_ids = fields.Many2many('Model Line',comodel_name='ir.model')


	ip_1 = fields.Char('Internal IP',default='http://')
	port_1 = fields.Char('Internal Port', default='8069')
	db_local_1 = fields.Char('Internal Database')
	internal_username_1 = fields.Char('Internal UserName')
	internal_password_1 = fields.Char('Internal UserName')

	url_db1 = self.ip_1 + ":" + self.port_1
	db_1 = self.db_local
	username_1 = self.internal_username_1
	password_1 = self.internal_password_1
	common_1 = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url_db1))
	models_1 = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url_db1))

	ip_2 = fields.Char('External IP',default='http://')
	port_2 = fields.Char('External Port', default='8069')
	db_local_2 = fields.Char('External Database')
	external_username_2 = fields.Char('External UserName')
	external_password_2 = fields.Char('External UserName')

	url_db2 = self.ip_2 + ":" + self.port_2
	db_2 = self.db_local_2
	username_2 = self.external_username_2
	password_2 = self.external_password_2
	common_2 = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url_db2))
	models_2 = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url_db2))


	uid_db1 = common_1.authenticate(db_1, username_1, password_1, {})
	uid_db2 = common_2.authenticate(db_2, username_2, password_2, {})




