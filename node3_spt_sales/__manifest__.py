{
    "name"          : "Sales Report",
    "version"       : "1.0",
    "author"        : "Susi",
    "website"       : "https://node3.id",
    "category"      : "Sales",
    "summary"       : "Sales Report in Pdf Format",
    "description"   : """
        
    """,
    "depends"       : [
        "base",
        "sale",
    ],
    "data"          : [
        "security/ir.model.access.csv",
        "report/sales_quotation_report.xml",
        "report/sales_quotation_report_cust.xml",
        "views/sale_order.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}