from odoo import models, fields, api, _
from odoo.exceptions import UserError

class SaleOrderLineCetak(models.Model):
    _name = 'sale.order.line.cetak'    
    _inherit = ['sale.order.line']

    order_id = fields.Many2one('sale.order', string='Order Reference', required=True, ondelete='cascade', index=True, copy=False)

    # description 
    # product_uom_qty
    # product_uom
    # price_unit
    # tax_id
    # discount
    # price_subtotal
    # price_total

    
    # name = fields.Text(string='Description', required=True)
    # price_unit = fields.Float('Unit Price', required=True, digits=dp.get_precision('Product Price'), default=0.0)
    # product_uom_qty = fields.Float(string='Ordered Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1.0)
    # price_subtotal = fields.Monetary(compute='_compute_amount', string='Subtotal', readonly=True, store=True)
    # price_tax = fields.Float(compute='_compute_amount', string='Total Tax', readonly=True, store=True)
    # price_total = fields.Monetary(compute='_compute_amount', string='Total', readonly=True, store=True)

    # tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])

    # discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)
    # currency_id = fields.Many2one(related='order_id.currency_id', depends=['order_id'], store=True, string='Currency', readonly=True)

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    _sql_constraints = [
        ('accountable_required_fields',
            "CHECK(1=1)",
            "Missing required fields on accountable sale order line."),
        ('non_accountable_null_fields',
            "CHECK(1=1)",
            "Forbidden values on non-accountable sale order line"),
    ]


    