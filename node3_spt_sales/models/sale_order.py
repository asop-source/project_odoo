from odoo import models, fields, api, _
from odoo.exceptions import UserError

class InheritSaleOrder(models.Model):
    _inherit = 'sale.order'
    _order = 'origin'

    order_line_cetak = fields.One2many('sale.order.line.cetak', 'order_id', string='Order Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True, auto_join=True)

    amount_untaxed_cetak = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, compute='_amount_all_cetak', track_visibility='onchange', track_sequence=5)
    amount_tax_cetak = fields.Monetary(string='Taxes', store=True, readonly=True, compute='_amount_all_cetak')
    amount_total_cetak = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all_cetak', track_visibility='always', track_sequence=6)
    total_qty = fields.Float(compute='_amount_total', string="Total Qty", store=False,)

    # note_cetak = fields.Text('Terms and conditions')

    @api.depends('order_line_cetak.price_total')
    def _amount_all_cetak(self):
        # """
        # Compute the total amounts of the SO.
        # """
        for order in self:
            amount_untaxed_cetak = amount_tax_cetak = 0.0
            for line in order.order_line_cetak:
                amount_untaxed_cetak += line.price_subtotal
                amount_tax_cetak += line.price_tax
            order.update({
                'amount_untaxed_cetak': amount_untaxed_cetak,
                'amount_tax_cetak': amount_tax_cetak,
                'amount_total_cetak': amount_untaxed_cetak + amount_tax_cetak,
            })

    @api.depends('total_qty')
    def _amount_total(self):
        for purchase in self:
            total_qty = 0.0
            for line in purchase.order_line:
                total_qty += line.product_uom_qty
        
        purchase.total_qty = total_qty

class SalesModel(models.Model):
    _inherit = "sale.order.line"
    barcode_vendor = fields.Char('Barcode Vendor')
    article = fields.Text('Article')
    description = fields.Text('Information')


    @api.onchange('product_id')
    def validasi_form(self):
        ## Validasi Karakter pada field 'name'
        product = self.product_id
        self.barcode_vendor = product.barcode_vendor
        self.article = product.article
        self.description = product.description

    