# -*- coding: utf-8 -*-
{
    'name': "Node3 Pos Custom",

    'summary': """
        Custom Receipt POS(Print Out Pos)
        """,


    'author': "asopkarawang@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Pos',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','flexiretail_com_advance','point_of_sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/assets.xml',
    ],

    'qweb' : [
        "static/src/xml/receipt.xml"
    ],

}