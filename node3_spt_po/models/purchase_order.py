from odoo import fields, models, api, _
from datetime import datetime, timedelta

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    _order = 'origin'

    def _default_acknowledge(self):
        return self.env.context.get('default_acknowledge_uid') or self.env['res.users'].search(
            [('name', 'ilike', 'Bernard')], limit=1)

    def _default_acknowledge2(self):
        data= self.env.context.get('default_acknowledge2_uid') or self.env['res.users'].search(
            [('name', 'ilike', 'Albert')], limit=1)
        print (data)
        return data.id

    def _default_approve(self):
        data= self.env.context.get('default_approve_uid') or self.env['res.users'].search(
            [('name', 'ilike', 'Dwi Handri')], limit=1)
        print (data)
        return data.id

    acknowledge_uid = fields.Many2one(comodel_name='res.users', string='Acknowledge',default=_default_acknowledge)
    acknowledge2_uid = fields.Many2one(comodel_name='res.users', string='Acknowledge 2',default=_default_acknowledge2)
    approve_uid = fields.Many2one(comodel_name='res.users', string='Approved By',default=_default_approve)

    def get_date(self, date=''):
        if date :
            date = (date + timedelta(hours=7)).strftime('%Y-%m-%d')
        return date

    total_qty = fields.Float(compute='_amount_total', string="Total Qty", store=False,)

    @api.depends('total_qty')
    def _amount_total(self):
        for purchase in self:
            total_qty = 0.0
            for line in purchase.order_line:
                total_qty += line.product_qty
        
        purchase.total_qty = total_qty

class PurchaseModel(models.Model):
    _inherit = "purchase.order.line"
    article = fields.Text('Article')
    inform = fields.Text('Information')


    @api.onchange('product_id')
    def validasi_form(self):
        ## Validasi Karakter pada field 'name'
        product = self.product_id
        self.article = product.article