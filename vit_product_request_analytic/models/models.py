from odoo import api, fields, models, _
import time
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class product_request(models.Model):
    _name = 'vit.product.request'
    _inherit = 'vit.product.request'


    location_id = fields.Many2one(comodel_name="account.analytic.tag", string="Location", required=False, )
    business_id = fields.Many2one(comodel_name="account.analytic.tag", string="Business", required=False, )


class product_request_line(models.Model):
    _name = 'vit.product.request.line'
    _inherit = 'vit.product.request.line'


    location_id = fields.Many2one(comodel_name="account.analytic.tag",
                                  string="Location",
                                  required=False,
                                  )
    business_id = fields.Many2one(comodel_name="account.analytic.tag",
                                  string="Business",
                                  required=False,
                                  )




