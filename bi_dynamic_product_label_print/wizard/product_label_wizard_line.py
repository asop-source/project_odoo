# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
##############################################################################
from odoo import api, fields, models, _

class product_label_wizard_line(models.TransientModel):
    _name = "product.label.wizard.line"
    
    
    def _default_sequence(self):
        label_line = self.search([], limit=1, order="sequence DESC")
        return label_line.sequence or 0
    
    font_size =  fields.Char("Font Size (In px)")
    sequence = fields.Integer()
    color = fields.Char("Font Color")
    field_id = fields.Many2one('ir.model.fields', string="Field Name",domain=[('model_id.model','=','product.product')])
    wizard_id = fields.Many2one('bi.product.label.wizard',string="Wizard")
    
    
    _order = "sequence"


class template_label_wizard_line(models.TransientModel):
    _name = "template.label.wizard.line"

    def _default_sequence(self):
        label_line = self.search([], limit=1, order="sequence DESC")
        return label_line.sequence or 0

    font_size = fields.Char("Font Size (In px)")
    sequence = fields.Integer()
    color = fields.Char("Font Color")
    field_id = fields.Many2one('ir.model.fields', string="Field Name",
                               domain=[('model_id.model', '=', 'product.template')])
    wizard_id = fields.Many2one('bi.template.label.wizard', string="Wizard")

    _order = "sequence"