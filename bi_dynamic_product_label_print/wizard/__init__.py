# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from . import bi_product_label_wizard
from . import product_label_wizard_line
from . import bi_template_label_wizard
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
