# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
##############################################################################
from odoo import api, fields, models, _
from reportlab.graphics import barcode
from base64 import b64encode


class bi_template_label_wizard(models.TransientModel):
    _name = "bi.template.label.wizard"

    template_id = fields.Many2one('dynamic.product.label.setting', string="Template",
                                  domain=[('template_type', '=', 'template')])
    label_hight = fields.Float("Label Height")
    label_width = fields.Float("Label Width")

    barcode_hight = fields.Float("Barcode Height")
    barcode_width = fields.Float("Barcode Width")
    product_template_ids = fields.Many2many('product.template', string="Products", required="1")
    currency_id = fields.Many2one('res.currency', required=True)
    currency_position = fields.Selection([
        ('after', _('After')),
        ('before', _('Before')),
    ], 'Position', required=True, translate=True)
    line_ids = fields.One2many('template.label.wizard.line', 'wizard_id', string="Lines")

    qty = fields.Integer("Quantity")
    logo = fields.Binary("Logo")
    barcode_type = fields.Selection([
        ('Code39', 'Code39'),
        ('Code128', 'Code128'),
        ('EAN', 'EAN'),
        ('EAN13', 'EAN-13'),
        ('EAN-8', 'EAN-8'),
        ('ISBN', 'ISBN'),
        ('ISBN-10', 'ISBN-10'),
        ('ISBN-13', 'ISBN-13'),
        ('ISSN', 'ISSN'),
        ('JAN', 'JAN'),
        ('PZN', 'PZN'),
        ('UPC', 'UPC'),
        ('UPC-A', 'UPC-A'),

    ], 'Barcode Type', required=True, )

    @api.model
    def default_get(self, fields):
        rec = super(bi_template_label_wizard, self).default_get(fields)
        paper_formate = self.env['ir.model.data'].xmlid_to_object(
            'bi_dynamic_product_label_print.paper_format_barcode_label')
        rec['label_hight'] = paper_formate.page_height or 100
        rec['label_width'] = paper_formate.page_width or 70
        rec['barcode_type'] = 'EAN13'
        rec['qty'] = 1
        rec['currency_position'] = 'after'
        rec['currency_id'] = self.env.user.company_id.currency_id.id
        rec['product_template_ids'] = self.env.context.get('active_ids', False)

        template = self.env['dynamic.product.label.setting'].search(
            [('if_default_template', '=', True), ('template_type', '=', 'template')], limit=1)
        rec['template_id'] = template.id or False
        return rec

    @api.multi
    def generate_label(self):
        for record in self.product_template_ids:
            if record.barcode:
                jpgdata = barcode.createBarcodeDrawing('EAN13', value=record.barcode, format='png',
                                                       width=self.barcode_width or 100, height=self.barcode_hight or 70,
                                                       humanReadable=False)
                imgdata = b64encode(jpgdata.asString('png'))
                record.barcode_img = imgdata

        paper_formate = self.env['ir.model.data'].xmlid_to_object(
            'bi_dynamic_product_label_print.paper_format_barcode_label')
        paper_formate.page_height = self.label_hight or 100
        paper_formate.page_width = self.label_width or 70

        return self.env.ref('bi_dynamic_product_label_print.action_bi_print_barcode_template').report_action(self, {})

    @api.onchange('template_id')
    def onchange_template_id(self):
        res = {}
        template = self.template_id or False
        if template:
            values = {}
            vals_list = []
            values = {'label_hight': template.label_hight, 'label_width': template.label_width,
                      'barcode_type': template.barcode_type, 'qty': template.qty, 'currency_id': template.currency_id,
                      'logo': template.logo, 'currency_position': template.currency_position,
                      'barcode_hight': template.barcode_hight,
                      'barcode_width': template.barcode_width, }
            for line in template.template_setting_line_ids:
                vals = {
                    'font_size': line.font_size,
                    'sequence': line.sequence,
                    'color': line.color,
                    'field_id': line.field_id.id,
                }
                vals_list.append((0, 0, vals))
            values['line_ids'] = vals_list

            self.update(values)

