# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api,_

class product_product(models.Model):
	_inherit = 'product.product'
	
	barcode_img = fields.Binary(string='Barcode Image')
	
	@api.multi
	def get_field_value(self,field_id):
		result = ""
		if field_id and field_id.model_id.model == 'product.product':
			field_name = field_id.name
			if field_id.ttype in ['many2many','one2many']:
				result = ""
			elif field_id.ttype in ['many2one']:
				result = self[field_name].display_name
			else:
				result = self[field_name]
		
		return result


class product_product(models.Model):
	_inherit = 'product.template'

	barcode_img = fields.Binary(string='Barcode Image')

	@api.multi
	def get_field_value(self, field_id):
		result = ""
		if field_id and field_id.model_id.model == 'product.template':
			field_name = field_id.name
			if field_id.ttype in ['many2many', 'one2many']:
				result = ""
			elif field_id.ttype in ['many2one']:
				result = self[field_name].display_name
			else:
				result = self[field_name]

		return result
