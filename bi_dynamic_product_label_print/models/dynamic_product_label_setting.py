# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api,_

class dynamic_product_label_setting(models.Model):

    _name = 'dynamic.product.label.setting'
    
    name = fields.Char("Name")
    if_default_template = fields.Boolean("Default Template")
    setting_line_ids = fields.One2many('dynamic.product.label.setting.line','setting_id',string="Lines")

    template_type = fields.Selection([
        ('template', 'Product Template'),
        ('product', 'Product Variant'),
    ], 'Template Type', required=True, )
    label_hight = fields.Float("Label Height")
    label_width = fields.Float("Label Width")

    barcode_hight = fields.Float("Barcode Height")
    barcode_width = fields.Float("Barcode Width")
    currency_id = fields.Many2one('res.currency', required=True)
    currency_position = fields.Selection([
        ('after', _('After')),
        ('before', _('Before')),
    ], string='Position', required=True, translate=True)
    line_ids = fields.One2many('product.label.wizard.line', 'wizard_id', string="Lines")

    qty = fields.Integer("Quantity")
    logo = fields.Binary("Logo")
    barcode_type = fields.Selection([
        ('Code39', 'Code39'),
        ('Code128', 'Code128'),
        ('EAN', 'EAN'),
        ('EAN13', 'EAN-13'),
        ('EAN-8', 'EAN-8'),
        ('ISBN', 'ISBN'),
        ('ISBN-10', 'ISBN-10'),
        ('ISBN-13', 'ISBN-13'),
        ('ISSN', 'ISSN'),
        ('JAN', 'JAN'),
        ('PZN', 'PZN'),
        ('UPC', 'UPC'),
        ('UPC-A', 'UPC-A'),

    ], 'Barcode Type', default='EAN13', required=True, )
    template_setting_line_ids = fields.One2many('dynamic.template.label.setting.line', 'setting_id', string="Lines")


class dynamic_product_label_setting_line(models.Model):
    _name = 'dynamic.product.label.setting.line'
    
    def _default_sequence(self):
        label_line = self.search([], limit=1, order="sequence DESC")
        return label_line.sequence or 0
    
    setting_id = fields.Many2one('dynamic.product.label.setting', "Setting")
    font_size =  fields.Char("Font Size (In px)")
    sequence = fields.Integer()
    color = fields.Char("Color")
    field_id = fields.Many2one('ir.model.fields', string="Fields",domain=[('model_id.model','=','product.product')])
    
    _order = "sequence"


class dynamic_template_label_setting_line(models.Model):
    _name = 'dynamic.template.label.setting.line'

    def _default_sequence(self):
        label_line = self.search([], limit=1, order="sequence DESC")
        return label_line.sequence or 0

    setting_id = fields.Many2one('dynamic.product.label.setting', "Setting")
    font_size = fields.Char("Font Size (In px)")
    sequence = fields.Integer()
    color = fields.Char("Color")
    field_id = fields.Many2one('ir.model.fields', string="Fields", domain=[('model_id.model', '=', 'product.template')])

    _order = "sequence"
