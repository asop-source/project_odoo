# -*- coding: utf-8 -*-

from datetime import datetime, time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError

class PurchasePromoInherit(models.Model):
    _name = "pos.promotion"
    _inherit = ['pos.promotion', 'mail.thread','mail.activity.mixin']

    department_id = fields.Many2one('hr.department', default=lambda self: self.env.user.employee_ids.department_id, string="Department")

    outs_approval = fields.Many2one('res.users', string="Outstanding Approval", store=True, track_visibility='onchange')
    is_approval = fields.Boolean(string="Approval", compute='cek_approval', store=False)
    level_approval = fields.Integer(string="Level Approval")
    state = fields.Selection([
            ('draft', 'Draft'),
            ('to approve', 'To Approve'),
            ('approved', 'Approved'),
            ('reject', 'Reject')
        ], string='Status', default='draft', track_visibility='onchange')
   

    @api.multi
    def button_confirm(self):
        for promo in self:   
            apps = self.env['node3.promo.approval'].search([('department_id','=', promo.department_id.id), ('level','=', 1)])            
            if apps:
                promo.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': 1}) 
                self.sendNotif()
            else:
                raise ValidationError(_("Routing approval not found, please create approval routing or contact admin"))        
        return True

    @api.multi
    def button_approve(self):
        for promo in self:
            next_level = promo.level_approval + 1
            apps = self.env['node3.promo.approval'].search([('department_id','=', promo.department_id.id), ('level','=', next_level),])            
            if apps:
                promo.write({'state': 'to approve', 'outs_approval': apps[0].approval_id.id, 'level_approval': next_level}) 
                self.sendNotif()
            else:
                promo.write({'state': 'approved', 'outs_approval': False})           
            
            promo.activity_feedback(['pos_senyum.mail_act_pos_promo'], user_id=self.env.user.id)
        return True

    @api.depends('is_approval')
    def cek_approval(self):
        for pr in self:
            if pr.outs_approval == self.env.user and pr.state == "to approve": # or self.env['res.users'].has_group('base.group_erp_manager') and pr.state == "to approve":
                pr.is_approval = True
            else:
                pr.is_approval = False
    
    @api.multi
    def button_reject(self, force=True):
        self.write({'state': 'reject', 'outs_approval': False,}) 
        self.activity_feedback(['pos_senyum.mail_act_pos_promo'], user_id=self.env.user.id)
        return True

    def sendNotif(self):
        for rec in self:
        # self.message_post(type="notification", 
        #         subject=_("Approval "),
        #         body=_('Need Approval'), 
        #         subtype='mail.mt_comment', 
        #         author_id=self.env.create_uid.partner_id.id,
        #         needaction_partner_ids= [(4, self.outs_approval.partner_id.id)],
        #         model= self._name,
        #         res_id=self.id,)
            # activty_type = self.env['mail.activity.type'].search([('id', '=', '4')]) # get To Do

            # activity_id = self.env['mail.activity'].create({
            #     'summary': 'Approval Promotion',
            #     'activity_type_id': activty_type.id,
            #     'res_model_id': self.env['ir.model'].search([('model', '=', 'pos.promotion')], limit=1).id,
            #     'res_id': rec.id,
            #     'user_id': rec.outs_approval.id,
            #     'date_deadline': rec['write_date'],
            # })

            self.activity_schedule(
                'pos_senyum.mail_act_pos_promo',
                user_id=rec.outs_approval.id,)

class ConfigApproval(models.Model):
    _name = "node3.promo.approval"
    _description = "Promotios Approval"

    name = fields.Char(string="Description")
    department_id = fields.Many2one('hr.department', string="Department", required=True)
    level = fields.Integer(string="Level", required=True)
    approval_id = fields.Many2one('res.users', string="Approval", required=True)
        

    