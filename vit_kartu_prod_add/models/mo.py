from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
import pdb

class production(models.Model):
    _inherit = 'mrp.production'

    customer_id = fields.Many2one('res.partner', 'Customer', help='Customer MO ini')
    code_cust_id = fields.Many2one('product.customer.code', 'Code Product', help='kode product versi customer')
    warna           = fields.Char("Warna",related="product_id.warna", store=True)

    @api.onchange('customer_id')
    def onchange_template_id(self):
        prod_code_ids = self.env['product.customer.code'].sudo().search([('product_id', '=', self.product_id.id),('partner_id', '=', self.customer_id.id)], limit=1).id
        if prod_code_ids:
            self.code_cust_id = prod_code_ids