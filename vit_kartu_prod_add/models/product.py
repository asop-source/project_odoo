from odoo import models, fields, api
from openerp import api, exceptions, fields, models, _
import base64
import pytz 
import xlwt
from io import BytesIO, StringIO
from xlrd import open_workbook
from datetime import datetime,timedelta
import pdb

class product(models.Model):
    _inherit = 'product.product'

    plt             = fields.Char("PLT", track_visibility='on_change')
    lt              = fields.Char("LT", track_visibility='on_change')
    no_poly         = fields.Char("No. Poly Box", track_visibility='on_change')
    kg_pal          = fields.Integer("Berat/KG (PAL)", track_visibility='on_change')
    heading         = fields.Integer("Heading (LOT)", track_visibility='on_change')
    NoSpec          = fields.Char("NoSpec", track_visibility='on_change')
    Diameter        = fields.Float("Diameter", track_visibility='on_change')
    Ket_Furnice     = fields.Char("Keterangan Furnice ", track_visibility='on_change')
    warna           = fields.Char("Warna", track_visibility='on_change')
    No_Registrasi   = fields.Char("No Registrasi ", track_visibility='on_change')
    Std_PolyBox     = fields.Char("Standart Poly Box", track_visibility='on_change')

    @api.onchange('warna')
    def onchange_warna(self):
        if self.warna :
            self.warna = self.warna.upper()


product()