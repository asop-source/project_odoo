
from odoo import models, fields, api
from odoo import api, fields, models, _
#from datetime import datetime, date
from datetime import datetime, timedelta
from odoo.exceptions import Warning,UserError

class MedicalAppointment(models.Model):
	_inherit = 'medical.appointment'


	state = fields.Selection([('draft', 'Draft'),
							('confirm', 'Confirm'),
							('prosses', 'Prosses'),
							('done', 'Done')],
							 string='Status',readonly=True, default='draft')



	def create_physican(self):
		for rec in self:
			rec.state = 'prosses'
			

		physican_requisition = {
						'patient_id' : self.patient_id.id,
						'doctor_id'  : self.doctor_id.id,
				}
		self.env['physicians.action'].create(physican_requisition)


	def create_done(self):
		for rec in self:
			rec.state = 'done'






