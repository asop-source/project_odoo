# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import time

class PhysiciansAction(models.Model):
	_name = 'physicians.action'
	_inherit = 'mail.thread'



	@api.multi
	def open_data_lab(self):
		return {
			'name' : _('Request Laboratium'),
			'domain' : [('patient_id.name', '=', self.patient_id.name)],
			'view_type' : 'form',
			'res_model' : 'medical.patient.lab.test',
			'view_id' : False,
			'view_mode' : 'tree,form',
			'type':'ir.actions.act_window',
		}

	@api.multi
	def action_hasil_lab(self):
		return {
			'name' : _('Hasil Laboratium'),
			'domain' : [('patient_id.name', '=', self.patient_id.name)],
			'view_type' : 'form',
			'res_model' : 'medical.lab',
			'view_id' : False,
			'view_mode': 'tree,form',
			'type': 'ir.actions.act_window',
		}

	@api.multi
	def open_data_obat(self):
		return {
			'name' : _('Rescription'),
			'domain' : [('patient_id.name', '=', self.patient_id.name)],
			'view_type' : 'form',
			'res_model' : 'medical.prescription.order',
			'view_id' : False,
			'view_mode': 'tree,form',
			'type': 'ir.actions.act_window',
		}

	
	def lab_get_count(self):
		count_labs = self.env['medical.patient.lab.test'].search_count([('patient_id.name', '=', self.patient_id.name)])
		self.lab_test = count_labs

	def obat_get_count(self):
		count_obat = self.env['medical.prescription.order'].search_count([('patient_id.name', '=', self.patient_id.name)])
		self.obat_count = count_obat

	def hasil_lab_count(self):
		count_hasil = self.env['medical.lab'].search_count([('patient_id.name', '=', self.patient_id.name)])
		self.hasil_lab_test = count_hasil



	name = fields.Char(string="Nomor Pemeriksaan Dokter", readonly=True ,copy=True)
	patient_id = fields.Many2one('medical.patient','Nama Pasien',required=True)
	doctor_id = fields.Many2one('medical.physician','Nama Dokter Pemeriksa',required=True)
	gejala_penyakit = fields.Text(string="Penjelasan Gejala Penyakit")
	diagnosa_utama = fields.Many2one(comodel_name='medical.pathology',string="Diagnosa Utama")
	paktor_diagnosa = fields.Many2one(comodel_name='medical.pathology.category', string="Faktor Diagnosa")
	tanggal_diagnosa = fields.Date(string="Tanggal Diagnosa",default=lambda self:time.strftime("%Y-%m-%d"))
	laboratium = fields.Boolean( string="Rujukan Laboratorium/Radiolog")
	tujuan_doctor = fields.Many2one('medical.physician','Nama Dokter')
	radiologi = fields.Many2one(comodel_name='medical.test_type', string="Tes Laboratorium/Radiolog")
	diagnosa_komplikasi = fields.Many2one(comodel_name='medical.pathology.category', string="Diagnosa Komplikasi")
	lab_test = fields.Integer(string="Total Request Lab Test", compute="lab_get_count")
	hasil_lab_test = fields.Integer(string="Total Request Lab Test", compute="hasil_lab_count")
	obat_count = fields.Integer(string="Total Request Obat", compute="obat_get_count")
	state = fields.Selection([('draft', 'Validate'),
							('confirm', 'Confirm'),
							('tested', 'Tested'),
							('done', 'Done')],
							 string='Status',readonly=True, default='draft')

	@api.multi
	def action_validate(self):
		for ex in self:
			ex.state = "confirm"


	@api.multi
	def create_requests(self):
		for ex in self:
			ex.state = "tested"


		req_lab = {
					'medical_test_type_id'  : self.radiologi.id,
					'patient_id'			: self.patient_id.id,
					'doctor_id'  			: self.doctor_id.id,

				}
		self.env['medical.patient.lab.test'].create(req_lab)


	@api.multi
	def create_prescription(self):
		for ex in self:
			ex.state = "done"

		prescription = {
						'patient_id' : self.patient_id.id,
						'doctor_id'  : self.doctor_id.id,
						'prescription_date' : self.tanggal_diagnosa
				}
		self.env['medical.prescription.order'].create(prescription)




	@api.model
	def create(self, vals):
		vals['name'] = self.env['ir.sequence'].next_by_code('physicians.action') or 'PAC-'
		msg_body = 'Physicians Action created'
		self.message_post(body=msg_body)
		result = super(PhysiciansAction, self).create(vals)
		return result
