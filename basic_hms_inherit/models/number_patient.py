from odoo import models, fields, api

class MedicalAppointment(models.Model):
	_inherit = 'medical.appointment'


	nomor = fields.Char(string='Nomor Antrian',readonly=True ,copy=True)



	@api.model
	def create(self, vals):
		vals['nomor'] = self.env['ir.sequence'].next_by_code('nomor.antrian')
		msg_body = 'Masukan Data Nomor Antrian'
		self.message_post(body=msg_body)
		result = super(MedicalAppointment, self).create(vals)
		return result

