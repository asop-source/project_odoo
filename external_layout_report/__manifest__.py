# Copyright 2016 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "External Layout Report",
    "summary": "Custome Report",
    "description": """
        Report external_layout_standard
    """,
    "version": "AP.0.01",
    "category": "Report",
    "website": "https://github.com/asop-source",
    "author": "asopkarawang@gmail.com",
    "installable": True,
    "depends": [
        "web","sale",'account','stock'
    ],
    "data": [
        "report/external_layout_standar.xml",
        # "report/assets.xml",
    ],
    "development_status": "Mature",
    "maintainers": ["lmignon"],
}